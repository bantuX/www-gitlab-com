---
layout: markdown_page
title: "Acquisition Process"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Acquisition process
The process is comprised of three key stages:
1. Exploratory
1. Business case
1. Diligence

### Exploratory stage
1. Intro call: we'll reach out to schedule a 50 minute introductory call. The purpose of the call is to:
    1. Current state of your company including team, products, financials and more
    1. Review the expectations and process noted on this page
    1. Start discussing which features could be built into gitlab
    1. Discuss which GitLab product category the team could join as a whole
    1. Answer questions your team may have
Details from this call should be collected following the [Initial Acquisition Review Template](https://docs.google.com/document/d/1RekjfQj89pIV7DZzeNZ00HYOwqlqlDFm7Gy4yZET9rc/edit?usp=sharing)(a GitLab internal document).
TARGET TEAM: Ahead of the product call please review our [roadmap](https://about.gitlab.com/direction) and outline which of your current and future product features can be implemented into GitLab's product categories. Outline a simple integration timeline for those features, considering an MVC release on the first month after joining GitLab and monthly releases following with quick iterations.
1. Mutual NDA: Sign a mutual NDA as linked on our [legal handbook page](https://about.gitlab.com/handbook/legal)
1. Create a new, private Slack channel for the acquisition team discussions. Format: `#acq-company_name`. Add VP Product Strategy and where relevant, the relevant product category director/s to newly created Slack channel.
1. Add template WIP Business Case to the top of the acquisition gdoc and start filling the details
1. Product call: start product diligence and deep dive into the discussion of which features could be built into GitLab and into which GitLab product stage
1. Internal review: validate potential fit for the team within GitLab and the integration options into GitLab

### Business case stage
1. [Form the acquisition team](/acquisition-process/#forming_an_acquisition_team)
1. Product integration strategy: the lead PM will formalize the integration strategy with a focus on what we keep as-is, what we re-implement in GitLab, what we discard, and how we migrate existing users.
1. Revenue projection: evaluate projected revenue from customer transitions
and added functionality as outlined in the integration strategy.
1. Preliminary financial & legal diligence - list of preliminary documents:
   1. Financials
   1. Tax returns
   1. Employee roster with salary information
   1. Employee agreements and PIAA
   1. Customer list with name, monthly revenue, contract termination date and any other fields if relevant.
   1. Vendor list with monthly spend
   1. Asset list
   1. Any assets that are needed for the business and will be part of the acquisition
   1. Assets excluded from the acquisition
1. Present business case for internal review of the acquisition proposal.
1. LOI: assuming all terms have been covered and reviewed, the alliances team will share a LOI within 7 days.

### Diligence stage
1. Technical diligence: your team will provide access to key engineering contacts at GitLab to your code repository and facilitate access to the products to conduct our technical diligence.
1. Financial & legal diligence
1. CEO call: Once integration of the product and team have been scoped out the acquisition lead will reach out to the CEO PA to schedule a 50 minute call. The purpose of this call is to:
    1. Discuss the wind down and onboarding process details including roles for the founders and the placement of the team within GitLab.
    1. Review the employee onboarding goals


## Forming an acquisition team
An acquisition team will consist of the following GitLab functions:
1. Acquisition lead, currently Eliran Mesika on all Acquisitions
1. Product Manager
1. Engineer
1. Finance/accounting team member, currently Paul Machle
1. Legal team member, currently Jamie Hurewitz

To assign the product manager, after the product call or as soon as it's clear which product category the features will be implemented into, contact the category product director for the assignment.

To assign the engineer team member, contact the engineering manager of the relevant category for assignment.

### Acquisition team responsibilities

| Member | Role | Deliverables |
| -- | -- | -- |
| Acquisition lead | 1. Main POC for acquired team 1. Identify potential areas for integration 1. Create case for acquisition and customer transition story | 1. Business case with deal structure 1. Total potential revenue gain (customers & product) |
| Product Manager | 1. Outline current product features to be implemented into GitLab 1. Outline potential future functionalities to be built into GitLab after the integration period | 1. Integration strategy 1. Potential revenue gain of acquired functionality|
| Engineer | 1. Lead technical diligence | 1. Code quality review 1. Integration strategy validation - feasibility and timeline |
| Finance | 1. Lead financial diligence 1. Validate business case and deal structure | |
| Legal | 1. Review entity, assets and existing agreements 1. Evaluate sunset and customer transition path | 1. LOI 1. Acquisition agreement|

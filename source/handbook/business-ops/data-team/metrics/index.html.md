---
layout: markdown_page
title: "KPI Index"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Maintenance

This page is meant to help map a [KPI](/handbook/ceo/kpis/) to where it is defined in the handbook. The Data Team is responsible for maintaining this page.

If a KPI is not defined, create an MR to propose a definition. Ask the appropriate business stakeholder to review and merge.

## Work Prioritization
In connection with our [Q2 OKRs](/company/okrs/fy20-q2/). We have made the following classifications on which metrics will be visualized in Periscope:

- P0 - Most important and expected to be completed in Q2
- P1 - High priority with expectation to be completed in Q2
- P2 - The data currently resides in a separate data system that provides for adequate visualization of the metric. We have de-prioritized these given that the metric is already being tracked effectively. The work to be performed in Q2 will be to ensure the definitions are clear and that we are reporting the actual KPI against our plan and target.
- Out of scope for data team - These KPIs are contained in systems that are either non-existent or are not planned to be supported by the data team in Q2.
- Regardless of classification all KPIs on this page are expected to have linked definitions with known quantities for Plan and Target values.

## GitLab KPIs 

GitLab KPIs are duplicates of goals of the reports further down this page.
GitLab KPIs are the most important indicators of company performance.
The [GitLab KPIs Dashboard](https://app.periscopedata.com/app/gitlab/434327/) whihch is also listed in the [Periscope directory](/handbook/business-ops/data-team/periscope-directory/).

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1 - P2
1. TCV - OpEx vs. plan > 1 - P1
1. [Sales efficiency ratio](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1 - P1
1. Pipe generated vs. plan > 1 - P0
1. Wider community contributions per release (dependent on GitLab.com data) - P1
1. [LTV / CAC](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) ratio > 4 - P1
1. Average NPS (out of scope for Q2)
1. Hires vs. plan > 0.9  (out of scope for Q2)
1. [Monthly employee turnover](/handbook/people-operations/people-operations-metrics/#turnover) - P0
1. New hire average score (out of scope for Q2)
1. Merge Requests per release per developer - P1
1. Uptime GitLab.com (out of scope for Q2 as dependent on PD)
1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown (out of scope for Q2)
1. [Support CSAT](/support/#customer-satisfaction) - P1
1. Runway > 12 months - P1
1. [MAUI](http://www.meltano.com/docs/roadmap.html#maui) (Meltano so not part of the GitLab Executive Team KPIs) > 10% WoW

## Sales KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1 - P0
1. [Field efficiency ratio](/handbook/finance/operating-metrics/#field-efficiency-ratio) > 2 - P0
1. [TCV](/handbook/finance/operating-metrics/#total-contract-value-tcv) vs. plan > 1 - P0
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) YoY > 190% - P0
1. Win rate > 30% - P1
1. % of ramped reps at or above quota > 0.7 - P1
1. [Net Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 2 - P0
1. [Gross Retention](/handbook/customer-success/vision/#measurement-and-kpis) > 0.9 - P0
1. Rep IACV per comp > 5 - P1
1. [ProServe](/handbook/finance/operating-metrics/#pcv) revenue vs. cost > 1.1 - P1
1. Services attach rate for strategic > 0.8 - P1
1. Self-serve sales ratio > 0.3 - P1
1. Licensed users - P1
1. [ARPU](/handbook/finance/operating-metrics/#arpu) (P0)
1. New strategic accounts - P1
1. [IACV per Rep](/handbook/finance/operating-metrics/#iacv-rep) > $1.0M - P0
1. [New hire location factor](/handbook/people-operations/global-compensation/#location-factor) < 0.72 - P0

## Marketing KPIs

1. Pipe generated vs. plan > 1 - P0
1. Pipe-to-spend > 5 - P0
1. [Marketing efficiency ratio](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) > 2 - P0
1. [Sales Accepted Opportunity (SAO)](/handbook/business-ops/#criteria-for-sales-accepted-opportunity-sao) - P0
1. [LTV / CAC ratio](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) > 4 - P1
1. Twitter mentions (out of scope for Q2) 
1. Sessions on our marketing site (out of scope for Q2)
1. New users (P1)
1. Product Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates (out of scope for Q2)
1. Social response time (out of scope for Q2)
1. Meetup Participants with GitLab presentation (out of scope for Q2)
1. GitLab presentations given - P1
1. Wider community contributions per release - P1
1. Monthly Active Contributors from the wider community - P1
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.72 - PO
1. Pipeline coverage: 2X for current quarter, 1X for next quarter, and .5 for 2 QTRs out. - P1

## People Operations KPIs

1. Hires vs. plan > 0.9 (Bamboo & Sheets) -P0
1. Apply to hire days < 30 (Greenhouse) -P0
1. No offer [NPS](/handbook/finance/operating-metrics/#nps) > [4.1](https://stripe.com/atlas/guides/scaling-eng) (out of scope for Q2) 
1. Offer acceptance rate > 0.9 (Greenhouse/Bamboo) -P1
1. Average [NPS](/handbook/finance/operating-metrics/#nps) (out of scope for Q2)
1. [Average location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) (Bamboo) -P0
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.72 (Bamboo) -P0
1. [12 month employee turnover](/handbook/people-operations/people-operations-metrics/#turnover) < 16% (Bamboo) -P0
1. [Voluntary employee turnover](/handbook/people-operations/people-operations-metrics/#turnover) < 10% (Bamboo) -P0
1. Candidates per vacancy (Greenhouse) -P1
1. Percentage of vacancies with active sourcing (Greenhouse) -P1
1. New hire average score (Out of scope for Q2)
1. Onboarding [NPS](/handbook/finance/operating-metrics/#nps) (out of scope for Q2)
1. Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention (Greenhouse) -P1
1. PeopleOps cost per employee (Netsuite over Bamboo) -P1
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1 (Bamboo)-P1

## Finance KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2 -P0
1. [Sales efficiency](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1.0 -P0
1. [Magic number](/handbook/finance/operating-metrics/#magic-number) > 1.1 -P0
1. [Gross margin](/handbook/finance/operating-metrics/#gross-margin) > 0.85 -P0
1. [Average days of sales outstanding](/handbook/finance/operating-metrics/#days-sales-outstanding-dso) < 45 -P0
1. [Average days to close](/handbook/finance/accounting/#month-end-review--close) (out of scope for Q2)
1. [Runway > 12 months](/handbook/finance/operating-metrics/#cash-burn-average-cash-burn-and-runway) -P1
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.73  -P0
1. [ARR by annual cohort](/handbook/finance/operating-metrics/#arr-cohort) - P0
1. [Reasons for churn](/handbook/customer-success/vision/#measurement-and-kpis) -P0
1. [Reasons for net expansion](/handbook/customer-success/vision/#measurement-and-kpis) -P0
1. [Refunds processed as % of orders](/handbook/support/workflows/services/gitlab_com/verify_subscription_plan.html#refunds-processed-as--of-orders) -P1


## Product KPIs

1. [Ambition Percentage](/direction/#how-we-plan-releases) (Merged Issues/Planned Issues per Release) ~ 70% (GitLab.com data) - P2
1. SMAU (Pings)
1. [MAU](/handbook/finance/operating-metrics/#monthly-active-user-mau) (GitLab.com + Pings) - P1
1. Stage Monthly Active Instances (SMAI) (Pings) - P2
1. Monthly Active Instances (MAI) (Pings) - P2
1. [Sessions on release post](/handbook/finance/operating-metrics/#sessions-release-post)  (Google Analytics) - P2
1. Installation churn (Pings) - P2
1. User churn (Pings) - P2
1. Lost Instances (Pings) - P2
1. [Acquisition](/direction/growth/#acquistion) (out of scope for Q2 - Snowplow)
1. [Adoption](/direction/growth/#adoption) (out of scope for Q2 - Snowplow)
1. [Upsell](/direction/growth/#upsell) (out of scope for Q2 - Snowplow)
1. [Retention](/direction/growth/#retention) (out of scope for Q2 - Snowplow)
1. Net Promotor Score / Customer Satisfaction with the product (unclear definition)
1. [New hire location factor](/handbook/people-operations/people-operations-metrics/#low-location-factor-reporting) < 0.72 - P1
1. Signups (GitLab.com) - P2
1. Onboarding completion rate (Snowplow) - P2
1. Comments (GitLab.com) - P2
1. Accepting Merge Requests issue growth (GitLab.com) - P2

## Engineering KPIs

1. Merge Requests per release per engineer in product development > 10 (GitLab.com) - P2
1. Uptime GitLab.com > 99.95% (out of scope for Q2 - Pingdom?)
1. Performance GitLab.com (out of scope for Q2 - Pingdom? Pagerduty? Not sure)
1. [Priority Support Service Level Agreement (SLA)](/handbook/support/#weekly-metrics-in-brief) - P2
1. [Support CSAT](/support/#customer-satisfaction) - P2
1. Support cost vs. recurring revenue (Netsuite over Zuora) - P0
1. Days to fix S1 security issues (GitLab.com) - P1
1. Days to fix S2 security issues (GitLab.com) - P1
1. Days to fix S3 security issues (GitLab.com) - P1
1. GitLab.com infrastructure cost per MAU (Netsuite over GitLab.com) - P0
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) per support rep > $1.175M (Zuora over Bamboo) - P0
1. [New hire location factor < 0.58](/handbook/hiring/charts/engineering-function/) - P1
1. Public Cloud Spend (Netsuite?) - P0

## Alliances KPIs

1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Red Hat, Digital Ocean, etc (out of scope for Q2)
1. Active installations per hosting platform: Total, AWS, Azure, GCP, Red Hat, Digital Ocean, Unknown (out of scope for Q2)
1. Product Downloads: Updates & Initial per distribution method: Omnibus, Cloud native helm chart, Source, etc (out of scope for Q2)
1. Acquisition velocity: [Acquire 3 teams per quarter](/handbook/alliances/acquisition-offer/) for less than $2m in total. (out of scope for Q2)
1. Acquisition success: 70% of acquisitions ship the majority of their old product functionality as part of GitLab within 3 months after acquisition. (out of scope for Q2)
1. [New hire location factor](/handbook/people-operations/global-compensation/#location-factor) < 0.82 -P0

## GitLab Metrics

We share a spreadsheet with investors called "GitLab Metrics", these can be found in the [dashboard with the same name](https://app.periscopedata.com/app/gitlab/409920/WIP:-GitLab-Metrics).

## All other metrics

Many metrics are important for tracking progress, but are not the top KPIs for the organization.
If a metric is listed above, it should not be listed in the below list. We do not need to maintain things in multiple places.

* [Average Sales Price (ASP)](link to location in handbook)
* [Capital Consumption](link to location in handbook)
* [Cash Burn, Average Cash Burn and Runway](link to location in handbook)
* [Contract Value](link to location in handbook)
   * [Annual Contract Value (ACV)](link to location in handbook)
   * [Incremental Annual Contract Value (IACV)](link to location in handbook)
   * [Gross Incremental Annual Contract Value (Gross IACV)](link to location in handbook)
   * [Growth Incremental Annual Contract Value (Growth IACV)](link to location in handbook)
   * [New Incremental Annual Contract Value (New IACV)](link to location in handbook)
   * [ProServe Contract Value (PCV)](link to location in handbook)
   * [Total Contract Value (TCV)](link to location in handbook)
* [Cost per MQL](link to location in handbook)
* [Credit](link to location in handbook)
* [Customers](link to location in handbook)
   * [Customer Segmentation](link to location in handbook)
   * [Customer Counts](link to location in handbook)
* [Customer Acquisition Cost (CAC)](link to location in handbook)
* [Customer Acquisition Cost (CAC) Ratio](link to location in handbook)
* [Days Sales Outstanding (DSO)](link to location in handbook)
* [Downgrade](link to location in handbook)
* [Field efficiency ratio](link to location in handbook)
* [Free Cash Flow (FCF)](link to location in handbook)
* [GitLab.com User and Group Churn](link to location in handbook)
* [Gross Burn Rate](link to location in handbook)
* [Gross Margin](link to location in handbook)
* [Licensed Users](link to location in handbook)
* [Life-Time Value (LTV)](link to location in handbook)
* [Life-Time Value to Customer Acquisition Cost Ratio (LTV:CAC)](link to location in handbook)
* [Lost instances](link to location in handbook)
* [Lost Renewal](link to location in handbook)
* [Magic Number](link to location in handbook)
* [Marketing efficiency ratio](link to location in handbook)
* [Marketo Qualified Lead (MQL)][Customer lifecycle](/handbook/business-ops/#customer-lifecycle)
* [Monthly Active Group (MAG)](link to location in handbook)
* [Monthly Active User (MAU)](link to location in handbook)
* [New ACV / New Customers](link to location in handbook)
* [New ACV / New Customers by Sales Assisted](link to location in handbook)
* [Non GAAP Revenue (Ratable Recognition)](link to location in handbook)
* [Reasons for Churn / Expansion, Dollar Weighted](link to location in handbook)
* [Retention, Gross & Net (Dollar Weighted)](/handbook/customer-success/vision/#measurement-and-kpis)
* [Revenue](link to location in handbook)
   * [Annual Recurring Revenue (ARR)](link to location in handbook)
   * [ARR by Annual Cohort](link to location in handbook)
   * [Monthly Recurring Revenue (MRR)](link to location in handbook)
* [Revenue per Licensed User (also known as ARPU)](link to location in handbook)
* [Sales Efficiency Ratio](link to location in handbook)
* [Sales Qualified Lead (SQL)](link to location in handbook)
* [Rep Productivity](link to location in handbook)
* [Social Response Time](link to location in handbook)
* [Team Members](/handbook/people-operations/people-operations-metrics/)

---
layout: markdown_page
title: Product
---

Are you not on the product team, but looking for information on how to get in
contact with or provide information/ask questions to product managers? Please
see our [How to Engage](/handbook/product/how-to-engage) guide.

The document below talks about _how_ we do product at GitLab, not about _what_.
For the _what_, see [Product Direction](/direction/).

If you are a product manager looking to learn our process, this document is
broken into the following sections:

- [Product Principles](/handbook/product/index.html#product-principles) covers our guiding ideas that help us in all aspects of our job.
- [Product Process](/handbook/product/index.html#product-process) is about the regular product delivery loop we go through that delivers features to our users.
- [How to Work as a PM](/handbook/product/index.html#how-to-work-as-a-pm) offers guidance on how to excel as a PM here.
- [Product Leadership](/handbook/product/leadership/) covers how to be an effective leader in the product management organization.
- [GitLab as a Product](/handbook/product/index.html#gitlab-as-a-product) contains information about GitLab as a product and how we manage it.

## On this page
{:.no_toc}

- TOC
{:toc}

### Other pages related to Product
{:.no_toc}

* [Product Categories](/handbook/product/categories/)
* [Release posts](/handbook/marketing/blog/release-posts/)
* [Making Gifs](/handbook/product/making-gifs/)
* [Data analysis](/handbook/business-ops/data-team/#-data-analysis-process)
* [Technical Writing](/handbook/product/technical-writing/)
* [Markdown Guide](/handbook/product/technical-writing/markdown-guide/)
* [Demo](/handbook/marketing/product-marketing/demo/)

## Product Principles

GitLab is designed and developed in a unique way.

The direction for the GitLab product is spelled out on the
[Direction page](/direction). This document provides lessons and heuristics on
how to design changes and new features. Our iterative process is demonstrated in
a [blog post](/2017/01/04/behind-the-scenes-how-we-built-review-apps/).

Much of our product philosophies are inspired by the [Ruby on Rails doctrine](http://rubyonrails.org/doctrine/) of ['Rails is omakase'](http://david.heinemeierhansson.com/2012/rails-is-omakase.html). I highly suggest reading these.

### Enabling Collaboration

From development teams to marketing organizations, everyone needs to collaborate
on digital content. Content should be open to suggestions by a wide number of
potential contributors. Open contribution can be achieved by using a mergeable
file format and distributed version control. The [mission of GitLab](/company/strategy/#mission)
is to **allow everyone to collaborate on all digital content** so people can
cooperate effectively and achieve better results, faster.

### Bringing Ideas to Reality

Ideas flow though many stages before they are realized. An idea originates in a
chat discussion, an issue is created, it is planned in a sprint, coded in an
IDE, committed to version control, tested by CI, code reviewed, deployed,
monitored, and documented. Stitching all these stages of the DevOps lifecycle
together can be done in many different ways. You can have a marketplace of
proprietary apps from different suppliers or use a suite of products developed
in isolation.

### The Minimally Viable Change (MVC)

Reduce every change proposal to its absolutely minimally viable form.
This allows us to ship almost anything within a single release,
get immediate feedback and avoid deep investments in ideas that might
not work. Other advantages:

- This helps avoid the sunk-cost fallacy in situations where we might
decide to drop a change, as we almost never spend more than a few weeks working
on something.
- It prevents over-engineering.
- It forces everyone involved to look for the simplest solution to a problem,
which is often the best solution.
- It forces working towards an 80/20 solution; while competing products might cater
to the last 20% of the market, a minimally viable solution is _good enough_ for 80%.
- A bigger change is harder to review.
- A bigger change is harder to roll back.
- The bigger the change, the larger the risk of a conflict with someone else's contribution
- The bigger the change, the longer it takes before everyone can benefit from the change.
- Further changes or enhancements to the change are driven by feedback from
actual users. This is a much more informative mechanism than the intuition
of a product person (though note that this doesn't mean we should just build
whatever feedback tells us).

The minimally viable change [should not be a broken feature](http://blog.crisp.se/2016/01/25/henrikkniberg/making-sense-of-mvp).

#### Content of an MVP

We only ship in a Minimally Viable Product style. Here are some guidelines about
it:

* The issue should be the smallest iteration we can do to address the problem.
* The issue that describes the change should be as concise a possible, while
  providing enough information to understand:
    * the context
    * what we want to achieve
    * who it is for
    * what the use cases are
* The MVP should be achievable in one iteration.
* If relevant, check if we can measure the usage of the feature by modifying the
  usage ping.
* If the issue generates a lot of discussion, make sure that the issue
  description always reflects the latest decisions at the current point in time.
* Feel free to edit the issue description without keeping a history of the
  previous content, as long as it reflects the latest decisions. If you really
  want to, you can copy old content into the discussion thread for posterity.
* It's not shipped until it's documented, no matter what the change is.

![mvc.png](/handbook/product/mvc.png)

Virtually every new feature must be maintained forever (the standard for sunsetting a
feature is very high). Creating a new feature means that GitLab has to continually manage the technical
costs of maintenance of that feature. Making small changes with quick feedback loops reduces the risk
of introducing a new feature where the value doesn't justify the long-term costs.

A new feature also adds a tax on the user experience due to additional complexity in the product.
Make sure that the benefits gained by users from the new feature more than cover the tax incurred,
accounting for how frequently the new feature is used by users. In particular, the new feature
should satisfy this inequality:

`(benefits * frequency) / (tax * (1-frequency)) > 1`

Despite its minimal form, the change
* always requires documentation
* has to have its usage tracked from day 1 if the feature has a meaningful impact.

[![xkcd.com](https://imgs.xkcd.com/comics/optimization.png)](https://xkcd.com/1691/)

#### Converting ideas into MVCs quickly

There is a certain failure case for MVC issues that product managers need to
be aware of and actively work to prevent; because we don't have infinite capacity
to work on everything, and good ideas are always coming in, it's possible (if
the PM is not watching closely) for issues with good ideas to slowly but inevitably
decay into something that is difficult to understand and move forward with. As
more and more comments are added and the issue slowly morphs from idea to idea
without any direction from a product manager, it will become difficult to ever
make complete sense of it again without losing some important context. It will
also be nearly impossible for someone who wants to make a community contribution
to understand what the direction is. It becomes an undefined blob of ideas.

To prevent this, it is the responsibility of the product manager to quickly turn
open-ended idea issues into something actionable, deliverable, and truly MVC as
quickly as possible.

The guidance for product managers here is to never let an idea issue persist
longer than three months without having been converted into a clear MVC.
If this has not happened, you should ask yourself why this is the case: is the
idea overly broad? Is there no clear immediate deliverable? Is it just not
important enough to spend time on? These are all signals that should be acted
upon without delay, before the issue degrades to the point that it becomes
unusable without significant rework.

In practice, if the original issue was really broad, and still contains great
ideas, then simply converting it into an MVC that gets closed when it ships
could be a disservice as valuable context is lost when it comes time to improve
upon the MVC. In this case, consider creating a new issue for the MVC so that it
is crisp and concise, and leave the original issue as a post-MVC vision.

### Avoiding language/platform specific features

We try to prevent maintaining functionality that is language or platform
specific because they slow down our ability to get results. Examples of how we
handle it instead are:

1. We don't make native mobile clients, we make sure our mobile web pages are great.
1. We don't make native clients for desktop operating systems, people can use [Tower](https://www.git-tower.com/) and for example GitLab was the first to have merge conflict resolution in our web applications.
1. For language translations we [rely on the wider community](https://docs.gitlab.com/ee/development/i18n/translation.html).
1. For Static Application Security Testing we rely on [open source security scanners](https://docs.gitlab.com/ee/ci/examples/sast.html#supported-languages-and-frameworks).
1. For code navigation we're hesitant to introduce navigation improvements that only work for a subset of languages.
1. For [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality_diff.html) we reuse Codeclimate Engines.
1. For building and testing with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/) we use Heroku Buildpacks.

Outside our scope are Kubernetes and everything it depends on:

1. **Network** (fabric) [Flannel](https://github.com/coreos/flannel/), Openflow, VMware NSX, Cisco ACI
1. **Proxy** (layer 7) [Envoy](https://envoyproxy.github.io/), [nginx](https://nginx.org/en/), [HAProxy](http://www.haproxy.org/), [traefik](https://traefik.io/)
1. **Ingress** [(north/south)](https://networkengineering.stackexchange.com/a/18877) [Contour](https://github.com/heptio/contour), [Ambassador](https://www.getambassador.io/),
1. **Service mesh** [(east/west)](https://networkengineering.stackexchange.com/a/18877) [Istio](https://istio.io/), [Linkerd](https://linkerd.io/)
1. **Container Scheduler** we mainly focus on Kubernetes, other container schedulers are: CloudFoundry, OpenStack, OpenShift, Mesos DCOS, Docker Swarm, Atlas/Terraform, [Nomad](https://nomadproject.io/), [Deis](http://deis.io/), [Convox](http://www.convox.com/), [Flynn](https://flynn.io/), [Tutum](https://www.tutum.co/), [GiantSwarm](https://giantswarm.io/), [Rancher](https://github.com/rancher/rancher/blob/master/README.md)
1. **Package manager** [Helm](https://github.com/kubernetes/helm), [ksonnet](http://ksonnet.heptio.com/)
1. **Operating System** Ubuntu, CentOS, [RHEL](https://www.redhat.com/en/technologies/linux-platforms/enterprise-linux), [CoreOS](https://coreos.com/), [Alpine Linux](https://alpinelinux.org/about/)

During a presentation of Kubernetes Brendan Burns talks about the 4 Ops layers
at [the 2:00 mark](https://youtu.be/WwBdNXt6wO4?t=120):

1. Application Ops
1. Cluster Ops
1. Kernel/OS Ops
1. Hardware Ops

GitLab helps you mainly with application ops. And where needed we also allow you
to monitor clusters and link them to application environments. But we intend to
use vanilla Kubernetes instead of something specific to GitLab.

Also outside our scope are products that are not specific to developing,
securing, or operating applications and digital products.

1. Identity management: Okta and Duo, you use this mainly with SaaS applications
   you don't develop, secure, or operate.
1. SaaS integration: Zapier and IFTTT
1. Ecommerce: Shopify

In scope are things that are not mainly for SaaS applications:

1. Network security, since it overlaps with application security to some extent.
1. Security information and event management (SIEM), since that measures
   applications and network.
1. Office productivity applications, since
   ["We believe that all digital products should be open to contributions, from legal documents to movie scripts and from websites to chip designs"](/company/strategy/#why)

### Convention over Configuration

#### Inspiration

We admire other convention over configuration tools like [Ruby on
Rails](http://rubyonrails.org/) (that doctrine of which perfectly describes the [value of integrated systems](http://rubyonrails.org/doctrine#integrated-systems)), [Ember](http://emberjs.com/), and
[Heroku](https://www.heroku.com/), and strive to offer the same advantages for a
continuous delivery of software.

Furthermore, Ruby on Rails has been of massive influence to the Ruby community.
Uplifting it, and making it more powerful and useful than ever before, for many
more usecases. We want GitLab to be to Kubernetes, what Rails is to Ruby.

You should prefer choices that are well thought out and based on current best practices.
Avoid unnecessary configuration. Avoid configuration to support fragile workflows.

For example, when considering whether to add a checkbox or two radio boxes, think
carefully about what users really want. Most of the time, you'll find you really only need
one solution, so remove the other option. When two possible choices really are
necessary, the best or most common one should be the default, and the other one
should be available. If the non-default choices are significantly less common,
then consider taking them out of the main workflow for making decisions, by
putting them behind an Advanced configuration tab, for example.

#### Avoid configuration completely when possible

Every configuration option in GitLab multiplies its complexity, which means
the application is harder to use, harder to develop, and
less friendly to users.

Requests for configuration can be a proxy for trying to support a fragile
workflow. Rather than enabling bad habits and incurring product debt, effort
should be spent helping customers adopt best practices.

Making features configurable is _easy_ and _lazy_.
It's a natural reaction to propose a big change to be configurable,
as you worry it'll negatively affect certain users. However,
by making a feature configurable, you've now created two problems.

[![xkcd.com](https://imgs.xkcd.com/comics/standards.png)](https://xkcd.com/927/)

Removing a configuration after it shipped and in use is much more work than not introducing it in the first place.
This is because you change the behavior for customers that selected the less popular option.
Adding a configuration is a [one way door](/handbook/values/#make-two-way-door-decisions) that should be avoided if possible.
And for sure should be left out of the first iteration of a feature if at all possible.
You can add it easily but removing it is hard.

Work on solutions that work for everyone, and that replace all
previous solutions.

#### Get out of the way

Sometimes configuration is inevitable or preferable. GitLab should
work perfectly right out of the box for most users. Your configuration
must not make that experience worse and should always _get out of the
way of the user_.

#### Encouraging behavior

Convention also implies that we're encouraging our customers to do things
in a certain way. A very concrete example of this is the ability to disable
pipelines. We believe that our integrated solution will give a superior user
experience and we're motivated to encourage this behavior. For this reason, adding a
configuration to allow disabling this permanently (be that in a template or
instance-wide), is something that should be avoided.

Encourage favorable behaviors by limiting configuration.

#### Default to ON

In addition to encouraging behavior by limiting the ability to toggle features,
new features, when introduced, should default to turning things ON (if they are
configurable at all), and when necessary, automatically migrate existing
users/groups/projects to have the new features ON.

#### Deciding on configurations

Avoiding configurations is not always possible. When we have no choice,
the secondary priority is to configure something in the GitLab
interface. A configuration should only appear in a file (`gitlab.rb` or
`gitlab.yml`) as a last resort.

#### Configuration in file

There are two major configuration files available in GitLab. Adding configurations
to either should be avoided as much as possible.

- [`gitlab.yml`](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/config/gitlab.yml.example)
is the configuration file used by the Rails application. This is where the domain is configured. Other configurations should be moved to the UI as much as possible and no new configurations should be added here.
- [`gitlab.rb`](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/master/files/gitlab-config-template/gitlab.rb.template)
is the configuration file for Omnibus-GitLab. It acts not only as
an abstraction of the configuration of `gitlab.yml` for GitLab-Rails, but also
as the source for _all configurations_ for services included and managed within
the Omnibus-GitLab. Newly introduced services probably need to be configured
here.

When you have to add a new configuration, make sure that the features and
services are on by default.
Only add a configuration line to either of these configuration files if the
feature or service cannot be fully disabled from the admin UI.

[Convention over configuration is also listed in the CONTRIBUTING file in GitLab's repositories.](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria)

### Be Ambitious

Many crazy, over-ambitious ideas sound like they are impossible just
because no one else is doing them.

Since we have amazing engineers and a culture of shipping minimally
viable changes, we are able to do a lot more 'impossible' things than others.

That's why we're shipping merge conflict resolution, why we shipped built-in CI
before anyone else, why we built a better static pages solution, and why
we're able to compete.

[![xkcd.com](https://imgs.xkcd.com/comics/squirrel_plan.png)](https://xkcd.com/1503/)

#### How this impacts Planning

Here at GitLab, we are an [ambitious](#be-ambitious) company and this means we
aim for big things with every release. The reality of taking chances and
planning aspirationally means that we won't always be able to deliver everything
that we wanted to try in every release, and similar to our [OKRs](/handbook/ceo/#three-levels-of-performance),
we believe this is a good thing. We don't want to shy away from challenging
ourselves and always want to keep a sense of urgency, and aiming for more helps
us do that. Also see [the importance of velocity](/handbook/engineering/#the-importance-of-velocity)

### Simplicity

Doing something simple in GitLab should BE simple and require no
human cpu-cycles to do so. Things that are simple now should still
be simple two years from now, ten years from now, and onwards.

This sounds obvious, but messing with flow is easily done. In most
cases, flow is disrupted by adding another action, or another click.

For instance: You want users to be made aware of the rules of a project.
Your proposal is a little popup that shows the rules before they create an
issue. This means that every time that someone creates an issue they need
to click once before resuming their normal action. This is unacceptable.
A better solution is to add a link to the issue that points the user to this.

[![xkcd.com](https://imgs.xkcd.com/comics/app.png)](https://xkcd.com/1174/)

It's very hard to maintain flow with a lot of configurations and options.
In cases where you want to enforce a certain behaviour, the most obvious step
may be to add another step or action. This can be avoided by making the action
work in parallel (like a link in the issue), encouraging rather than enforcing
certain behaviours.

Also, we don't want users to be able to construct workflows that break GitLab or
make it work in unpredictable ways.

### Discoverability Without Being Annoying

Feature discoverability is important for allowing new and existing users to access
old and new features, thereby increasing the value for them. It also allows GitLab
to get as much feedback as possible, as fast as possible, in order to quickly
iterate.

However, UI that purports to increase discoverability, but that is not carefully
designed and implemented, may actually _harm_ the overall experience by constantly shoving
unwanted images and text in the face of the user. The end result is that the user
loses trust in GitLab, and they no longer take the time to carefully parse text and
other UI elements in the future. Even worse, they might leave GitLab because of this
degraded experience. The following are a few illustrative examples and best practices.

#### Empty states

- Empty states are the simplest paradigm for feature discoverability. They should always be considered.
- Typically, any empty list view should be replaced with an empty state design, similar to below.
- Empty state designs should provide an immediate call to action, thus improving discoverability.
- Once the UI area is no longer "empty", that design is no longer present and there is no harm to the user experience at all.

![pipelines_empty_state.png](/handbook/product/pipelines_empty_state.png)

#### Banners

- Banners draw attention to the user by introducing a feature to them in a nearby context.
- Benefits
  - Attractive graphics to draw attention.
  - Brief explanatory text.
  - A call to action for the user to start using the feature.
- Downsides
  - They invade the user's current flow and intended actions.
  - They are visually disruptive.
- To offset the downsides
  - There should only be one banner on the page at a time.
  - They should carry minimal information and not overwhelm the page.

Think of this: Your co-worker is hard at work in front of their computer, and you
suddenly tap their shoulder or yell at them to tell them about some new cool widget.
You better have a good reason for that: that widget better be awesome.

- Banners need to be dismissible permanently so that a user _never_ sees it again,
_ever_, after they have dismissed it.
- We have only one chance to introduce a feature to a user with a banner.
- Once they have chosen to dismiss it, it should never appear again because we do
not want to betray their trust when they click dismiss.
- Trust is incrementally earned with delightful experiences. If a banner
re-appears after dismissal, trust is destroyed.

Back to the analogy: Your co-worker said they don't care about that new cool widget. Never,
ever, _ever_, bring it up again. They told you they don't care, and you need to respect
that.

- Banner dismissal must be associated with the user in the system database, and
it must persist, even across version upgrades.
- If a user accesses GitLab with different clients, the dismissal state must be
consistent and the user has zero chance of seeing that banner again.
- A corollary is that banners should only be shown when a user is logged in.

![pipelines_empty_state.png](/handbook/product/banner_customize_experience.png)

#### Navigation

Leveraging navigation is an effective design paradigm to introduce a user to a new
feature or area of GitLab.

- See example design below: There is a subtle pulsating blue dot to draw a user's attention.
- This plants a seed in the user's mind that they can go and investigate that feature at a later time.
- If at the current moment they don't want to be disturbed, they can just ignore it because it is only
a slight visual disturbance (as compared to a banner which takes up more screen real estate).

- The pulsating blue dot UI element should be dismissible, with the same dismissibility
requirements as banners, for the same trust reasons discussed above.
- If dismissed once, it stays dismissed forever, for that user, across all clients that the user can
access GitLab with.

- In the same way that a page should not have more than one banner, the navigation should
_not_ have more than one call to action (the blue dot in this case).
- We do not want to overload the user with too much noise.

Back to the analogy. We're not going to bother our co-worker with 5 different cool new widgets at the same time.

- GitLab should only ever show at most one blue dot. This should be implemented by
GitLab having a prioritized list of blue dots stored in the backend.
- It should show the highest priority blue dot that has not already been dismissed.

![tooltip_boards.png](/handbook/product/tooltip_boards.png)

### Flow One

Shipping only MVCs can result in a large set of loosely connected pieces that
don't necessarily combine into a single, great user experience.

An obvious solution to this would be to plan out the future in detail,
creating a long-term roadmap. However, this is unwanted as it can restrict
your flexibility and ability to respond to changing needs or feedback.

Flow One offers an alternative. You draw out a workflow consisting of
MVCs (that can be shipped individually). The workflow should only cover a
specific, narrow use-case, and nothing more.

This means you:

- avoid creating an inflexible, long-term plan
- can more easily construct a full feature/ capability, which is more easily marketed
- can provide context to each individual change ("we need this as part of X")
- can continue to ship MVCs
- work concurrently on several items, none of which are blocking

Flow One should cover the first iteration of a particular workflow.
After this, individual MVCs can be introduced to expand the use-cases
or loosen the assumptions (e.g. from a feature that can only be used
if you're using feature branches, to one that works for other git strategies).

### Breadth over depth

See our thoughts on breadth over depth [on our strategy page](/company/strategy/#breadth-over-depth). While we prioritize breadth over depth, Product Managers should not lose sight of maturing each product category, with the ultimate goal of each category moving into a lovable state. A good rule of thumb to consider when planning product improvements, is that over time, approximately 70% of engineering effort should be allocated on breadth, and 30% on depth.

### Data-driven work

Using data to learn from our users is important. Our users are spread across GitLab.com
and self-managed instances, so we have to focus our efforts on learning and
providing benefit to both when we decide to collect more data, or build and use
additional analytics tools. If we do this, we can help make the rest of the
company successful as well. This means that we should:

- Build and use tools that work for both GitLab.com and self-managed.
- Start from a question, and build / collect what we need to answer that question. This avoids wasting time with data we don’t need.
- Use and improve existing tools we have inside of GitLab before leaning towards off-the-shelf products.
- Our customers, sales team and customer success teams all benefit greatly from similar insights into their usage as the product team does. Make things that help all of these people.

### No artificial limits in Core

Per [GitLab Stewardship](/company/stewardship/#promises), we will not introduce _artificial_ limits in Core. Artificial means
arbitrarily setting a small number (such as: 1) as a limit on a given GitLab object category,
that would incur _no additional_ effort or cost had we chosen a larger number. The additional
effort includes product, design, and engineering effort to create the feature in the first place,
and to maintain it over time.

For example, GitLab Core has the [issue board feature](https://docs.gitlab.com/ee/user/project/issue_board.html) in every project.
In GitLab EE, each project supports [multiple boards](https://docs.gitlab.com/ee/user/project/issue_board.html#multiple-issue-boards).
This _does not_ mean that Core has an artificial limit of one board per project, because there is additional effort
to manage multiple boards such as supporting the navigation interface, and all the associated engineering work.

### No enforced workflows

We're discussing enforced workflows [in this issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/2059).

Enforced workflows should be avoided in GitLab. For example, there are three issue
states (`Open`, `In Progress` (as of 10.2), and `Closed`), and any issue should be
allowed to transition from one state to any other state
without workflow restrictions. (Roles and permissions is a separate concern.)

- Enforced workflows restrict GitLab to a smaller number of use cases, thus reducing the value of GitLab.
- Enforced workflows require overhead to maintain in the product. Each new feature
must account for any existing enforced workflows.
- We should trust our users to use GitLab responsibly, giving them freedom, instead
of imposing enforced workflows that we think made sense at the time of design and implementation.

[A comment on Hacker News](https://news.ycombinator.com/item?id=16056678) perfectly details what can go wrong when enforcing workflows:

"The down side for the true end-users, those who actually use the software day-to-day,
is that most business processes are awful. If your experience is the hellish existence
that I see strolled about on threads where JIRA comes up ...:

1. Your admin(s) set it up once and hasn't bothered to iterate on those workflows.
1. The business mapped their autonomy stripping processes onto JIRA intentionally.
I'd guess that most of your work experience is similar. Process stifled nonsense."

But that comment also specifies the advantage:

  "JIRA's most powerful feature is that it affords for mapping businesses processes onto software.
  This is incredibly compelling to enterprise customers. Software that enforces workflows, procedures
  and requirements can be an incredible lever and JIRA's price point makes build vs buy decisions an absolute no-brainer."

We should ensure that GitLab makes it easy to help with enterprise workflows:

- When starting a branch with an issue (number) we link it to the branch.
- When merging an MR you automatically close the issue(s) it fixes.
- In GitLab CI you can define your deployment stage progression (staging, pre-production, production) including manual approval.
- We run quality and security tools automatically and provide dashboards to check status instead of making it a step in the process.
- We limit the impact of mistakes with incremental rollout and automatic rollback.

### Prefer small primitives

Small primitives are building blocks in GitLab. They are an abstraction _not_ at
the technical level, but truly at the product level. Small primitives can be combined,
built-upon further, and otherwise leveraged to create new functionality in GitLab.
For example, the label lists in [issue boards](https://docs.gitlab.com/ee/user/project/issue_board.html)
use the smaller primitive of [labels](https://docs.gitlab.com/ee/user/project/labels.html).

They are especially powerful because they usually take less effort _and_ provide
higher leverage than you would get from a more "complete" but standalone feature.
Think of how simple Unix command line utilities can be chained together to do
really complicated things, much easier (and certainly more flexibly) than you
could have done with a dedicated tool.

When iterating on GitLab, strongly consider using small primitives instead
of creating new abstractions, especially when considering MVC features
that will provide the foundations for further improvements. To do this
you can start with easy to apply concepts that meet the needs of intermediate
to advanced users; from here document the usage clearly and be sure to think about
discoverability. The UX can very often be refactored or enhanced later when there's
a demonstrated need for refinement, onboarding for less sophisticated users, or
other new abstractions needed that were identified through real-world usage.

### Modern first

When developing features to compete with existing competitors, make sure to
solve problems for modern development teams first, and *then* see what's missing
for legacy teams. e.g. For project management, make great project management
capabilities for teams doing conversational development, lean, or even agile
development before doing Scaled Agile Framework (SAFe) or waterfall.

### Developer first

Our vision includes going after a lot of new personas, going from developers to
operations, security, product managers, designers, etc. But when developing
features in these new areas, it's important to remember to start with the
developer. If we can make security great for developers and *then* great for
security professionals, we'll be much more successful.

### Cloud-native first

Development teams deploy to tons of different platforms, from bare metal to
cloud VMs to cloud-native Kubernetes clusters. We build features for
cloud-native first, and *then* support the rest. This allows us to focus on
where development is going, and deliver solutions that every company aspires to
use eventually, even if they're not ready to today.

## Product Process

Introducing changes requires a number of steps, with some overlap, that should be
completed in order:

### Important dates PMs should keep in mind
The following dates are derived from the [Product Development Timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline).

- **1st** - Scope/capacity for the upcoming release has been agreed on between
  product/engineering/UX (released the month following, not the current month).
  Also, the [Kickoff document](https://docs.google.com/document/d/1ElPkZ90A8ey_iOkTvUs_ByMlwKK6NAB2VOK5835wYK0/edit)
  is updated with features that will be developed in the upcoming release.
  The planning may still change at any time before the 8th, and the Kickoff
  document should be kept consistent with further updates.
- **7th** - Code freeze for the current release goes into effect. After this
  date you should not commit to any last minute requests (issues that are
  critical, such as security vulnerabilities, are generally handled directly
  by engineering). Also, update the [Kickoff document](https://docs.google.com/document/d/1ElPkZ90A8ey_iOkTvUs_ByMlwKK6NAB2VOK5835wYK0/edit)
  to explicitly declare which items did and did not get merged for the current
  release. Notify your manager of anything that did not make it, [if you haven't already](https://about.gitlab.com/handbook/product/#shifting-commitment-mid-iteration).
- **8th** - Kickoff call for upcoming release (published the month
  following). This is a live-streamed call where PMs communicate the features
  their teams will focus on for the upcoming release. The kickoff does not have
  to include every single issue included in the iteration; it should focus on
  major work/features as defined by the `direction` label. i.e. all items
  mentioned at the Kickoff should have the `direction` label, and all
  `direction` items that are marked as `Deliverable` for the upcoming release
  should be mentioned. Issues such as documentation or minor fixes need not be
  mentioned. Same applies to exploration issues.
- **14th** (6th working day before the 22nd) - All relevant items in your area have been added to the [release post draft](https://about.gitlab.com/handbook/marketing/blog/release-posts/) for the
  current release.
- **15th** - [Team retrospective](https://about.gitlab.com/handbook/engineering/management/team-retrospectives)
- **22nd** - New release and blog post are published. Also, by now you should
  have a draft on the issues that will be included in the next kickoff
  (released 2 months later). Start capacity and technical discussions with
  engineering/UX.

(current release = published this month, upcoming release = published next month)

### Planning horizon

1. [Mission](/company/strategy/#mission): generation
1. [Strategy](/company/strategy/#sequence-): lustrum
1. [Vision](/direction/product-vision/): year
1. [Milestones](/direction/#future-releases): quarter (we have issues assigned to specific release milestone for a rolling 3 months)
1. [Release](https://gitlab.com/gitlab-org/release-tools/blob/master/templates/monthly.md.erb): month (we try to not change the release that is started)

Inspired by:

- First part of [Dear PMs, It's Time to Rethink Agile at Enterprise Startups](http://firstround.com/review/dear-pms-its-time-to-rethink-agile-at-enterprise-startups/)
- [The Nearsighted Roadmap](https://medium.com/@jobv/the-nearsighted-roadmap-57fa57b5906a)

### Managing your Product Vision

The product vision is an important part of each PMs work. PMs are expected to maintain the accuracy
of their vision throughout the year through stakeholder conversations and other research.

Around November we set the new vision for the coming year. During this period we create a new
"Product Vision 20XX" label and apply it to issues that have been identified as contributing
to the vision.

PMs are able to refine their vision throughout the year, but since the vision is communicated widely,
major updates to the content that happen ad-hoc should also be communicated widely - for example,
using the Product FGC meetings or other communication channels.

#### What makes a product vision issue?

It's important to note here that vision _is not_ simply a synonym for new features and innovation.
Those are included for sure, but so are issues related to all of your [sensing mechanisms](/handbook/product/#sensing-mechanisms).
A category upgrade from minimal to viable or delivery of a top customer issue (for example) can contribute
to your vision just as much as a brilliant new innovative feature can. It's up to PMs to balance this
through a coherent strategy.

Conversely, in a broad sense anything could move the vision forward in a general way. Product
Vision items (i.e., with the label) should be direction-level items that move the vision forward
meaningfully. This is up to the PM to set the bar for, but there should be a clear step forward
with real user value.

Finally, issues are the atomic unit for the vision label. Epics can receive the label as well,
to keep things organized, but we track the delivery of our vision on a per-issue basis. Ensure
you are applying the label appropriately.

#### Moving the product vision forward

In each release, you should be moving your vision forward. At least one item with this label
should be included; if not, it's possible you're getting off track from where you planned
to go and should assess.

Note that, by their nature, Product Vision items should not be sitting in the backlog or with no
milestone. They can be in the bucket milestones (such as Q4 2019) if they are planned to be delivered
more than three or so months out, but they should not be unplanned - if they are this is a signal
that either they are not as important as originally thought, or that something important is not
getting due attention.

### Prioritization: Ahead of kickoff

1. Proper discovery has been done and requirements for the change are defined
1. Engineering has been involved in the discovery process (FE, BE, UX, as needed)
1. Technical architecture has been drafted and is agreed upon
1. Scoping and splitting up into actionable issues has happened
1. Issues that move your vision forward have the appropriate [Product Vision label](/handbook/product/#managing-your-product-vision) applied

### Kickoff meeting

#### Preparation

1. The [release post manager](/handbook/marketing/blog/release-posts/managers/) that will lead the release post will be the [directly reponsible individual](/handbook/people-operations/directly-responsible-individuals/) for presenting and leading the meeting for the same release. So the release post manager for GitLab X.Y will present the X.Y Kickoff.
1. The format of the meeting will be that of a [panel interview](https://www.thebalancecareers.com/panel-interview-questions-and-answers-2061158) focused on the product. This will mean they they will prepare the introduction and closing thoughts.
1. Relevant issues are added to the [Kickoff doc](https://docs.google.com/document/d/1ElPkZ90A8ey_iOkTvUs_ByMlwKK6NAB2VOK5835wYK0/edit?usp=sharing)
   * Product managers should add any issue that is marked ~direction and ~Deliverable for the upcoming milestone (no ~Stretch issues)
   * Issues should report the tier they will be included in
   * At least one item that [moves your vision forward](/handbook/product/#managing-your-product-vision) is included, and has the marker label in the kickoff.
1. Issues from previous release are marked `merged` or `not merged` on the rightmost column. If any direction items were added (new items or newly marked as ~direction) add those to the document and mark them in **bold**.
1. PM should consider if issues from previous release which are marked `not merged` should be included again in the next release
1. Issue descriptions are updated to include an introduction to the underlying
   problem, use cases, and a complete proposal. It should be easy to
   understand the problem from reading the issue title and description without
   digging through comments.

#### Meeting

1. Follow the instructions on [live streaming](https://about.gitlab.com/handbook/communication/youtube/#live-streaming) to set up the Google Hangout for the call, and share the link in #product chat (mention @product-team.)
1. The person presenting their screen should make sure they are sharing a smaller window (default YouTube resolution is 320p, so don't fill your screen on a 1080p monitor. 1/4 of the screen is about right to make things readable.)
1. In order to prevent distraction, when presenting the Kickoff doc use the ["View for Web" version](https://docs.google.com/document/u/1/d/e/2PACX-1vRtlb3PyZzouTnAql-fopSL6IlAxkhztueqrHqvdvCk3eIg0Yna2L54vrWWtYd5_1TWw-yAU2MhUC4k/pub)
1. The [release post manager](/handbook/marketing/blog/release-posts/managers/) starts the meeting by:
   * Giving a small introduction about the topic of this meeting
   * Introducing the panel interviewees and themselves
   * Reminding anyone who may be watching the video/stream about how we [plan ambitiously](#ambitious-planning).
1. During the discussion about a product category
   * The [release post manager](/handbook/marketing/blog/release-posts/managers/) will continue to screenshare to aid the interviewee.
   * They should also be sure to use display cues (highlighting, mouse pointer movement) to indicate where in the document we are, so nobody watching gets lost.
   * The interviewee will explaining the problem and proposal of listed items and guides the RPM through the inividual issues. If there is a UX design or mockup available, it will be shown.
   * The PM presenting the content should not take more than 2.5 minutes for their part. The PM chooses which items are important in their list to present to ensure they remain within the allotted time, prioritizing Product Vision items when available. If the release post manager sees the person taking more time, or if the overall schedule is getting off track, they will ping in chat to ensure everyone is aware.
   * Each PM should try to have one visual item that can be opened up and looked at.
   * Be sure you're on do not disturb mode so audio alerts do not play.
1. The [release post manager](/handbook/marketing/blog/release-posts/managers/) often ends the meeting by quickly highlighting several high impact issues to communicate our excitement in the
   upcoming release. Consider even using one of our popular phrases: “This will be the best release ever!”

### Execution

1. Design
1. Backend development
1. Frontend development
1. QA and feature assurance
1. Deploy

### QA RCs on staging and elsewhere

After the feature freeze, it's expected of each product manager to test their own features and perform quality assurance
to the best of their ability and follow up where necessary.

Product managers can use the staging environment once the release managers have deployed a release candidate to staging.
Release managers should post in the #product channel in Slack that a new release candidate is available. Product managers
can also use other environments as needed, such as GitLab provisioned on Kubernetes with GKE.

### Feature assurance

Before a new features is shipped, the PM should test it out to make sure it
solves the original problem effectively. This is not about quality assurance
(QA), as developers are responsible for the quality of their code. This is about
feature assurance (FA). Feature assurance is necessary because sometimes there
are misunderstandings between the original issue proposal and the final
implementation. Sometimes features don't actually solve the intended problem,
even though it seemed like it would, and sometimes solutions just don't feel as
useful as intended when actually implemented.

If you can test out the feature during development, pulling down branches
locally (or with a review app!), that's great. But sometimes it's not feasible
to test a feature until it's bundled into a release candidate and deployed to
GitLab.com. If so, make sure to test out features as soon as possible so any new
issues can be addressed before final release. Also, take the FA cycle into
account when scheduling new milestone work.

If you are looking to test code that has not been merged to GitLab.com or is not yet
part of an RC, you can pull the branch down locally and test it using the [GitLab
Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit).

### Dealing with security issues

Product managers should set Milestones for issues marked with the `security`
label to guarantee they are shipped by their due date, as defined in the
[Security Team process](https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues).
Product Managers are the DRIs for prioritization.
As such they should deeply understand the implications and risks of security
related issues and balance those when prioritizing.
You may need to set a different milestone for security issues, for example
because of low capacity, but before doing that you should engage a conversation
with the Security Team.
Priority labels and Due Date designations should not be modified by product managers in any case,
since they are directly managed by the Security Team, and used to track metrics and progress.

### Cross-stage features

There are certain features of the product that don't fit squarely into a particular stage.
Instead, the relevant _functionality_ within the feature will inform where the work should be assigned. For example "Chat"
is not owned by any particular stage, so functionality should be considered when labeling such work, for example:
  - Integration with a new chat tool, would fall under `Plan` since this stage is responsible for enabling users to collaborate with team members.
  - Approving MRs from a chat client, would fall under `Create` since this stage is responsible for code management and merge requests.

Finally, any work that is deemed necessary or urgently needed for a particular group to move forward (for example, to unblock another planned feature)
should be owned by that group if a more fitting group is unable to complete in a reasonable timeframe. This model allows teams to be flexible and
calibrate their priorities accordingly, and a team should thus never be blocked. (An exception would be if a change required anything that a software
engineer would not be allowed to do, such as a production change, in which case the infrastructure team would be the blocker.)

### Updating product categories

#### Update responsible product managers

The [product categories](https://about.gitlab.com/handbook/product/categories/) page needs to be updated regularly in order to ensure that the information provided is up to date. To update the responsible person for a role, follow these steps:

1. Go to the [GitLab.com / www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) project
1. Change the name of the responsible person in `source/data/stages.yml` at the relevant position e.g. `pm: John Doe`.
1. Add the markdown reference link of the responsible person in `source/includes/product/_categories.erb`

Here's an [example Merge Request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/22765).

If the person is not yet listed on the [team page](https://about.gitlab.com/company/team/), please follow [these instructions](https://about.gitlab.com/handbook/git-page-update/#11-add-yourself-to-the-team-page) to add them.

## How to work as a PM

If you follow the principles and workflow above, you won't be writing long, detailed
specs for a part of the product for next year. So how should you be
spending your time?

Invest the majority of your time (say 70%) in deeply understanding the problem.
Then spend 10% of your time writing the spec _for the first iteration only_ and
handling comments, and use the remaining 20% to work on promoting it.

A problem you understand well should always have a (seemingly) simple or obvious
solution. Reduce it to its simplest form (see above) and only ship that.

Once you've shipped your solution, both you and the community will
have a much better idea of what can be improved and what should be prioritized
for future iterations.

As a PM, you're the person that has to kick-off new initiatives. You're not
responsible for shipping something on time, but you _are_ responsible for taking
action and setting the direction. Be active everywhere, over-communicate, and
sell the things you think are important to the rest of the team and community.

As a PM, you need to set the bar for engineering. That is, to push engineering and
the rest of the company. You almost want engineering to complain about the pace
that product is setting. Our default instinct will be to slow down, but we can't
give in to that.

As a PM you don't own the product; ask other people for feedback and give
team members and the community the space to suggest and create things without your
direct intervention. It's your job to make sure things are decided and
planned, not come up with every idea or change.

### Where should you look when you need help?

* The first thing you should do is read this page carefully, as well as the
[general handbook](/handbook/).
* You can ask questions related to Product in the `#product` Slack channel.
* General questions should be asked in `#questions`.
* Specific Git related questions should be asked in `#git-help`.
* HR questions should be asked in `#peopleops`.

### Responsibilities and Expectations

The responsibilities for [Product Managers](/job-families/product/product-manager/),
[Director of Product](/job-families/product/director-of-product/),
[VP of Product](/job-families/product/vice-president-of-product/) and
[Head of Product](/job-families/product/head-of-product/) are outlined in our job families pages.

The progression of responsibilitie allocation between tactical, operational and strategic
is well illustrated by this helpful chart.

![PM Responsibility Allocation Chart](https://pbs.twimg.com/media/Dy54prbX0AUAO_X.jpg:small)

*Thanks to [Melissa Perri](https://twitter.com/lissijean/) for the image*

In addition, as a Product Manager you're expected to:

- Follow the issues you've been involved with / are assigned to as a PM. That
  includes reading all comments. Use email notifications for this.
- Make sure the issue description and title is updated when necessary. It
  should always reflect the current state of the issue.
- Make sure issues are moved forward when needed. You should not only avoid
  being the bottleneck, you should also be the person moving issues forward when
  they get stuck or overlooked.
- Make sure features solve the original problem effectively.
- Make sure features are complete: documentation, marketing, API, etc.
- Know when to cut corners and when not to. If we merge documentation a day
  later, that's usually acceptable. Conversely though, learning from a customer
  that documentation is lacking is not.
- Excite and market new features and changes internally and externally.
- Help build a vision for GitLab and GitLab's features.
- Understand deeply whatever it is you're working on. You should be spending a
  lot of time learning about your subject matter.
- Have regular meetings (at least once a week) with customers.
- Make sure marketing materials related to your work are up to date.

Occasionally, Product Managers are asked to perform the role of "Life Support" Product Manager for
a group. When doing so please refer to the [Life Support PM expectations](#life-support-pm-expectations).

As we grow, Product Managers can be listed across multiple stages and be asked to
perform the role of Product Manager across split or multiple teams in an "Interim" basis. While
temporary and based on future hiring, these positions are not considered
"Life Support" and thus the standard Responsibilities and Expectations apply.

### Scope of responsibilities

The product team is responsible for iteration on most of GitLab's products and
projects:

- GitLab CE and EE
- GitLab.com
- about.gitlab.com
- customers.gitlab.com
- version.gitlab.com
- license.gitlab.com

This includes the entire stack and all its facets. The product team needs to
weigh and prioritize not only bugs, features, regressions, performance, but also
architectural changes and other changes required for ensuring GitLab's excellence.

### Goals of Product

Everyone at GitLab is involved with the product. It's the reason why we are
working together.

With every release of GitLab, we want to achieve each of the following goals.

1. Improve GitLab's existing tools.
1. Achieve [our vision](/direction/#vision) of a complete toolset.
1. Make our product more interesting for our customers through Products and
EE exclusive features.

### Objectives and Key Results (OKRs)

Like all teams, the Product team participates in GitLab [OKRs](/company/okrs/).
The Product team OKRs are tracked as issues in the [Product project](https://gitlab.com/gitlab-com/Product).

The process for doing so is:

- At the start of each quarter, after OKRs have been [reviewed and approved by the E-Group](/company/okrs/#how-to-achieve-presentation), Issues are created for
each key result with the issue title started with `KEY RESULT TITLE => 0% (Tracking to 0%)`. All issues should be tagged
with the `OKR` label and assigned the appropriate quarter Milestone (eg, `Q1 2019`) such that they appear on [this board](https://gitlab.com/gitlab-com/Product/boards/906048).
- In the first two weeks of the quarter, every OKR should be assigned to a single Product Leader. Most OKRs will consist of one or more sub-tasks required in order to satisfy the OKR and these sub-tasks will be assigned to individual Product Managers.
or Director of Product Management. The [company OKR page](/company/okrs/#quarterly-okrs) will be updated with links to the issues.
- While the [team is responsible for achieving the OKR](company/okrs/#levels),
the assigned individual is responsible for:
  - Driving progress against the OKR within the team
  - Requesting and providing updates in relevant [team](#weekly-product-management-meeting) and [leadership meetings](#weekly-product-leadership-meeting)
  - Weekly updates to the issues including scoring (in the title) updates when needed
  - Monthly scoring updates to the [active company OKR document](/company/okrs/#quarterly-okrs)
- Final [scoring](/company/okrs/#scoring) is submitted in the final week of the quarter, and a retrospective is scheduled for the first week
of the following quarter.

### Long Term Financial Success Criteria for GitLab.com

The Product team is responsible for the direction and financial measurement of the Gitlab.com strategy but does not carry direct P&L responsibility.  Long term financial success for GitLab.com is measured as:  
* 80% Gross Margin on paid users
* 2% free user expense target as a percentage of revenue

### Prioritization

As this document and the [direction page](/direction) shows,
there are a million things we want to do.
So, how do we prioritize them and schedule things properly?
Roughly speaking, we balance the following priorities:

1. Security fixes and data-loss prevention
1. Regression fixes (things that worked before including major performance regressions)
1. New features, user experience improvements, technical debt, community contributions, and all other improvements

Issues like security come in [different severity levels](https://about.gitlab.com/handbook/engineering/security/#labels-and-confidentiality).
Lower severity means a lower priority, at ~S4/~P4 their priority is no different then other feature proposal.

Any new feature will have to meet the following requirements:

1. It is [technically viable](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#contribution-acceptance-criteria)
1. The technical complexity is acceptable. We want to preserve our ability to make
changes quickly in the future so we try to avoid complex code, complex data structures, and optional settings.
1. It is orthogonal to other features (prevents overlap with current and future features).
1. The requirements are clear.
1. It can be achieved within the scheduled milestone. Larger issues should be
split up, so that individual steps can be achieved within a single milestone.

New features are prioritized based on several factors:

- Is it a top project from the [OKRs](/company/okrs/)?
- Does it bring our [vision](/direction/#vision) closer to reality?
- Does it help make our community safer though [moderation tools](https://gitlab.com/gitlab-org/gitlab-ce/issues/19313)?
- Is this something requested by many of our customers?
- Does it help generate revenue for us through our [paid tiers](/handbook/product/#paid-tiers)? (i.e what is the impact on IACV?)
- Is it something we need ourselves?
- Has it been much requested on the issue tracker?
- Does it benefit a large portion of our users?

Every release of GitLab has to be better than the last. This means that
bugs, regressions, security issues, and necessary performance and architecture changes always take up
whatever capacity they require to ensure GitLab remains stable, secure and fast.

We hope to realize our [vision](/direction/#vision),
while making sure we're building things that our customers want. In practice this means
that we aim to ship features that align with our vision,
but also solve problems our customers have. These features bring the product forward,
and build value for the largest group of people possible. For example, issue relationships is a highly
requested feature that also fits neatly within our vision. Everyone will benefit from this.

In practice, it is not possible to always ship things that only fall within our vision.
Other changes are prioritized and scheduled based on demand. An example would be a specific
form of authentication that is only used in particular organizations. This is not a big
win to all our customers, but if it's not too much work, it can be a very big win to a subset of
customers. Demand can also come internally, such as things that'll help GitLab achieve goals
within a team or specifically drive business goals such as specific EE features.

We take all priorities in account when planning and scheduling larger initiatives across the
teams (such as: "we're integrating CI, so everyone has to contribute to this in some way").
Our teams always self-manage priorities with an eye towards [global optimization](#prioritize-global).
Still, most changes are scheduled at a team-level, like: "make issues faster", or "add a new way to schedule pipelines".

#### Community Considerations

GitLab is open-source, and encouraging and promoting a large ecosystem of contributors is critical
to our success. When making prioritization decisions, it's important to heavily weight activities which
will encourage a stronger community of contributors. Some of those activities are:
* The swift review and merge of community contributions
* The creation of small primatives that can be utilized and iterated on by community members
* The building of integration points which can entice independent third parties to contribute an integration
* The addition of tools or features which make the contribution experience easier

#### Example

To make it clear with an example, the CI/CD team might ask:

* What else is needed for the idea-to-production vision? Are there other APIs needed
for ChatOps integration? Can we trigger manual actions via API? (e.g. [Environment-specific variables](https://gitlab.com/gitlab-org/gitlab-ce/issues/20367),
and [show deployment activity](https://gitlab.com/gitlab-org/gitlab-ce/issues/19992))
* What moves us towards automatic deploys of `gitlab-ce`?
* What moves the CI/CD vision forward?
* What community contributions need to be reviewed and merged?
* Can we polish the existing feature set? (e.g. [#21126](https://gitlab.com/gitlab-org/gitlab-ce/issues/21126), [#18178](https://gitlab.com/gitlab-org/gitlab-ce/issues/18178), [Show builds in context of pipeline](https://gitlab.com/gitlab-org/gitlab-ce/issues/20863), [#5983](https://gitlab.com/gitlab-org/gitlab-ce/issues/5983), [#3976](https://gitlab.com/gitlab-org/gitlab-ce/issues/3976))
* Can we speed up CI/CD pipelines? (e.g. sticky runners, [automatic parallelization](https://gitlab.com/gitlab-org/gitlab-ce/issues/21480))
* What can we do to make customers happy?
* How can we make existing integration points easier for others to contribute their own integration?
* How can we ensure that existing integrations retain their ability to stay viable (e.g. [#58301](https://gitlab.com/gitlab-org/gitlab-ce/issues/58301))?  While GitLab does not maintain external integrations, we should also [play well with others](/direction/#plays-well-with-others) and consider any external changes to libraries that users may rely on as breaking changes to GitLab itself. This also goes both ways, for breaking changes we make to a plugin, we actively contribute changes and guidance as needed to maintain interoperability.
* What can we do to ship EE features for CI? (e.g. [gitlab-ee#933](https://gitlab.com/gitlab-org/gitlab-ee/issues/933))

#### Milestones

We schedule an issue by assigning it a milestone; for more on this see
[Planning a Future Release](#planning-future-release).

### Working with Your Group

As a product manager, you will be assigned as the [stable counterpart](/company/team/structure/#specialists-experts-and-mentors) to a single [group](/company/team/structure/#groups). At GitLab we abide by
unique, and extremely beneficial guidelines when interacting with our groups. These include:
1. Product Managers are the [DRI](/handbook/people-operations/directly-responsible-individuals/) for all prioritization within their group including bug fixes, security issues, feature, tech debt, and discovery. They alone determine and communicate priority. The process for doing so is [documented](#prioritization) and standard across all groups.
1. Product Managers provide the what and when, Engineering (UX, Backend, Frontend, Quality) provide the how. This process is documented as part of our monthly [product](https://about.gitlab.com/handbook/product/#product-process), [engineering](/handbook/engineering/workflow/#product-development-timeline) and [UX](/handbook/engineering/ux/ux-designer/) cadence. We [define stable counterparts for each of these functions](/handbook/product/categories) within a group.

As an all-remote company, our crispness when it comes to responsibilities throughout the Product Delivery process was born out of necessity, but it pays untold dividends. Some of the benefits include:
* We avoid the ambiguity in handoffs between teams
* We avoid the confusion of many responsible individuals
* We avoid the slowness of consensus driven decision making
* We avoid the disruptiuon of frequent context switching
* We gain the rigidity to be consistent
* We gain the freedom to iterate quickly

#### From Prioritization to Execution

As described above, prioritization is a multi-faceted problem. In order to
translate the priorities of any given group into action by our engineering
teams, we need to be able to translate this multi-faceted problem into a flat
list of priorities for at least the next release cycle. Product Managers are
responsible for taking all these prioritization considerations and creating a
clear, sequenced list of next priorities. This list should be represented as an [issue board](/product/issueboard/)
so that each team has a clear interface for making decisions about work. From
this list, Engineering Managers and Product Managers can work together to
determine what items will be selected for work in the immediate future.

This does not mean that items will be addressed in strict order - EMs and PMs
need to be cognizant of dependencies, available skillsets, and the [rock/pebbles/sand](https://www.developgoodhabits.com/rock-pebbles-sand/)
problem of time management to make the best decisions about selecting work.

#### Prioritizing for Global Optimization
{: #prioritize-global}

Individual product managers must consider, and advocate for [global optimizations](https://about.gitlab.com/handbook/values/#results)
within the teams they are assigned to. If your assigned team requires expertise
(remember [everyone can
contribute](https://about.gitlab.com/handbook/engineering/#collaboration))
outside the team you should make all reasonable efforts to proceed forward
without the hard dependency while advocating within the product management team
for increased prioritization of your now soft dependencies.

If you have a request for additional help within your group, it is your
responsibility to advocate for it. It's important to avoid generic "we need
another Developer in X group" statements. Instead, create an issue which
includes a concrete list of critical issues for no more than the next three
months ([example](https://gitlab.com/gitlab-com/Product/issues/84)).

These issues should all be considered "top priority" and generally fall in one
of the following three categories:
- Customer Health - Issues identified by customers and the customer success team
  as critical to retaining or acquiring critical customers.
- Product Vision - Issues the broader Product Organization has externally
  committed to deliver and you view as critical
- Security - Issues that will significantly miss our security SLAs

Include your proposal for other issues and groups outside of your own which
should be considered a lower priority. Once you've reviewed the issue with your
assigned Engineering Manager - ping your Product and Engineering Directors for
review and refinement. If a global optimization which spans beyond your
Department leaders is required, then the Senior Director of Engineering, VP of
Product, and VP of Engineering should be pinged.

##### Exit Plan
The following exit plan is an initial attempt to spell out when and how the loaned engineers will be released to their original teams. A new hire is expected to be brought up to speed in this context. The plan will be iterated and amended as necessary.

Week 1 is the first week a new hire joins the team.

| Phase | Timeline (recommendation) | What happens |Notes|
| ------ | ------ | ------ | ------ |
| Onboarding | Week 1 ~ 3 | On-loan engineer works full-time on receiving team to bring up new hire. | New hire's first week priority is onboarding. |
| Collaboration | Week 4 ~ 5  | On-loan engineer works half-time on receiving team while new hire is able to submit MRs of low complexity issues. | On-loan engineer's time may be dedicated completely in pair programming or code review, not necessarily making direct contribution to issues. |
| Handoff | Week 6 ~ 8 | New hire is now the owner. On-loan engineer transitions to a consultant role, reverting back to full-time in original team. | |


#### Private tools and dashboards for monitoring and KPI tracking

These information sources may be useful to help you prioritize.

- [Feature usage](https://redash.gitlab.com/dashboard/feature-adoption)
- [EE usage](https://version.gitlab.com/): dev.gitlab.org account
- [Grafana](https://dashboards.gitlab.net): Google gitlab.com account
- [Kibana](https://log.gitlab.net): dev.gitlab.org account
- [S3stat](https://www.s3stat.com): GitLab 1Password account
- [Sentry](https://sentry.gitlap.com): dev.gitlab.org account

### Managing creation of new groups, stages, and categories

As GitLab grows we will need to create new groups, stages, and categories. During this transition period
we need to [globally optimize](../values/#global-optimization), and ensure that important issues are not blocked during the creation of a new group.

There are three common scenarios which may encounter these transition periods:

* Splitting a Stage into multiple Groups
* Changing the Categories in a Stage
* Adding a new Stage

In each of these scenarios, a Group may become temporarily responsible for a future Group's issues and backlog. During this time, the responsible Group should
balance the needs of our users and organization, prioritizing across the common backlog.

#### Splitting a Stage into multiple Groups

As the group(s) working on a Stage grow in size, a new Group may need to be formed so group sizes remain manageable. For example, today
there is a single group for `Manage`, but will be splitting into `control` and `framework` groups.

To prepare for splitting a Stage into multiple Groups, we should:

1. Update `categories.yml` and `stages.yml`, assigning each Group a set of Categories
1. Ensure all issues remain labelled with the Stage name, like `devops:manage`
1. Ensure all issues also have a group label, like `Control` or `Framework`
1. Prior to the new groups being formed, the PM and EM prioritize the shared `devops:manage` backlog

Once the first PM or EM is hired, the new Group should be formed:

1. The other PM/EM's will need to continue working across both Groups. For example if a backend EM is hired, the frontend EM and PM will continue to work across both Groups until additional hires are made.
1. EM's and engineers should work together and divide the existing engineering team to staff the new groups, like `Control` and `Framework`. Ideally, each group would have at least two backend engineers, but that is not always possible.
1. Update `stages.yml` and `team.yml` to reflect the current group members.

As the rest of the EM/PM's are hired, they take over that role for the respective group.

#### Adding a new category to a Stage

The categories within a Stage will change over time, based on GitLab's direction and the market landscape. The groups within a Stage will need to
be able to handle these changes, without issues falling in the cracks.

When the categories change, we should:
1. Update `categories.yml` and `stages.yml`, ensure all categories are assigned to a Group
1. If two categories are merging, apply the new category label to issues from both of the old categories
1. If a new category is being added, create a new category label and apply it to relevant issues
1. Update category epics to reflect the new labels and categories
1. Review the handbook and other material which may link to old categories
1. Remove old category labels

#### Adding a new Stage

When GitLab decides to address additional needs within the single application, a new Stage may need to be created. For example, `Defend` may be created to
address additional needs beyond what `Secure` focuses on.

When a new Stage is added, and its Group has yet to be formed, we should:
1. Ensure all issues for the new Stage are assigned with the Stage labels, like `devops:defend` and `Defend`
1. Identify an existing Group, like `Secure`, which will be initially responsible for the new Stage
1. The existing Group will prioritize across a common backlog of both Stages, in this example `devops:defend` and `devops:secure`
1. Update `categories.yml` and `stages.yml`, listing the new Stage with the members of the existing responsible Group

Once the first PM or EM is hired, a new Group for the Stage should be formed:

1. The other PM/EM's will need to continue working across both groups. For example if a backend EM is hired, the frontend EM and PM will continue to work across both groups until additional hires are made.
1. EM's and engineers should work together to staff the new Group, like `Defend`. Each Group should have at least two backend engineers.
1. Now that the new Group is formed, both Groups can focus on their respective Stages. In this case, `Secure` on Secure and `Defend` on Defend.
1. Update `stages.yml` to reflect the new Group and its members.

As the rest of the EM/PM's are hired, they take over that role for the new Group.

### Planning and roadmaps

As a PM, you must plan for the near term (more detailed) as well as for the long
term (more broad). Together, these form a
[nearsighted roadmap](https://medium.com/@jobv/the-nearsighted-roadmap-57fa57b5906a).
This will enable you to efficiently communicate both internally and externally
how the team is planning to deliver on the [product vision](/direction/#vision).

#### Roadmap dates

Use [calendar year (CY) dates](/handbook/communication/#writing-style-guidelines) for your issues, milestones, and lables to communicate timelines. Fiscal year (FY) does not translate well outside the company and roadmaps and vision pages are intended for external consumption. If you'd like to denote that an issue will be completed within a fiscal quarter use the month or GitLab release number for the last month in the fiscal quarter. E.g. use `12.4 (2018-10-22)` instead of `FY20-Q3`. The issue can always be moved up in date and users/customers will seldom complain when functionality ships ealier than expected.

#### Stage vision

You need to create and maintain a vision for your stage of the DevOps lifecycle
through the medium-to-long term of 1-2 years. This lives in a page linked from
the [direction page](/direction), e.g. `/direction/create`.

Your long-term vision should be more concrete for things that are closer in
time, and vice versa. The format of the [2018 vision](/direction/product-vision/)
is a great way to structure your vision. You're free to do otherwise, just make
sure it's easily consumable from the website.

The purpose of this page is to make it clear in what greater context individual
changes fall. E.g. you might deliver a feature that allows admins to track
username changes. That is not very interesting and hard to sell - but if it's
clear that this is part of a greater push to improve auditing in GitLab, this
becomes an enticing part of a greater whole.

The vision for the stage should also include:
1. A list of categories within your stage with a brief description and link to the
[category vision epic](#category-vision).
1. A link to a YouTube video recording of you presenting your vision updated in
   the last three months.

#### Category vision

Create and groom epics for each of the categories within your group. When creating
these epics, it's important to focus your time and attention on specific actions and future
iterations. It's natural to want to spend significant effort predicting the future,
but [iteration is one of our primary values](https://about.gitlab.com/handbook/values/#iteration) and
that applies to category visions as well. Your category visions should contain short paragraphs with lots of references
to specific issues. [Here](https://gitlab.com/groups/gitlab-org/-/epics/531) is an example. While
we cannot create epic templates, below is the markdown template to use for category
vision epics:

* Define the [personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) related to the problem space by populating the target audience section in the issue or epic description and by labeling accordingly with the [following labels](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=persona%3A). That way everyone is aware for whom we are developing and is able to discover related problems/features.

```
Category Label: ~"ADJUST LABEL" | [ADJUST - Issue List]() | [ADJUST - Overall Vision]() | [ADJUST - Other Links]()

## Description
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

## Target audience and experience
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

```

In order to filter and recognize these epics more easily, you can label them with
`product category`, the [Team label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#team-labels),
the [Stage label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/issue_workflow.md#stage-labels),
and the label for the given category (e.g., `sast`).

You must keep these categories in sync with `categories.yml` and for
new categories, link to the category epic from the `alt_link` in
`categories.yml`. Adding the category epic as the `alt_link` will automatically
create links in the [home page](/) and the [categories page](/handbook/product/categories/)
so people have an understanding of what each new category really means.

Of course for categories that have already shipped, and that have a marketing
product page, `categories.yml` should link to the product page, and the product
page should then have a link to the category epic (you can see an example for
GitLab Pages with a Vision button [here](/product/pages/)). You should also, of
course, link to your category epics from your [stage vision](#stage-vision)
page.

It's okay if your vision changes over time. Ideally, what you'll change are
minor things: links, issues, smaller features. Large, strategic initiatives are
unlikely to change frequently - when they do, it's probably worth talking about. See the [categories.yml file itself](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/categories.yml#L3) for more information about formatting and available values.

#### Maturity plans

For each category, we track the improvements required to advance to the next
level of maturity. These issues are indicated with an epic and/or labels, and
the planned feature set should be clearly visible from the corresponding
category vision page. e.g.:

* Stage vision (page)
  * Category vision (page)
    * Minimal maturity (epic)
        * Cool Feature/Capability A (epic)
            * Issue
            * Issue
            * Issue
        * Cool Feature/Capability B
        * Cool Feature/Capability C
    * Viable maturity (epic)
    * ...

#### 3-month roadmap

In order to plan effectively around releases, as a PM you should have a detailed
3-month roadmap at all times. The issues contained in this near-term roadmap are
the ones you should be spending the most energy on (fleshing them out, adding
detail, discussing, etc). These issues will go through a refinement period where
Product, Engineering, UX, and other parties will discuss the proposal until a
proper MVC proposal is reached (see
[Contents of an MVP](https://about.gitlab.com/handbook/product/#content-of-an-mvp)
for more detail). Most of the communication should happen within the issue
comments, but some may happen offline (such as via Slack). Ensure that any
relevant information that arise as part of an offline conversation is added to
the issue title and issue description. As a PM you must ensure that issues have
enough detail added to them before they become actionable as part of an
iteration.

The near-term roadmap will also help planning for capacity with engineering and
UX in order to commit to the contents of the next release.

#### Sensing Mechanisms

Our ability to iterate quickly is a measure of our efficiency, but our effectiveness
is just as critical. As a product manager you are critical to us not just working correctly,
but working on the correct things. You do that by [prioritizing appropriately](#prioritization). Your
prioritzation decisions will be enhanced if you maintain a sufficient understanding of the
context in which you make them.

There is no limit to the amount of inputs you can utilize for making prioritization decisions.
We've organized these mechanisms into three lists. One for those that primarily sense feedback from users,
one that primarily senses feedback from buyers and another that senses internally
generated feedback which could represent buyers or users. For new PMs consider
these lists as guidance for places to ensure you are plugged in to maintain
sufficient context.

User
- Triaging community generated issues and ideas
- Reviewing [top up voted issues](#category-vision)
- Asking probing questions in customer discovery meetings
- Proposing and analyzing results from [UX research](community/gitlab-first-look/index.html)
- Engagement directly with [customers](#a-customer-expressed-interest-in-a-feature) and via the [customer](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=customer) label

Buyer
- Asking probing questions in sales support meetings
- Reviewing Win/Loss reports
- [Chorus transcriptions](https://about.gitlab.com/handbook/business-ops/tech-stack/#chorus) of sales calls and demos
- Reviewing Customer Success [designated top issues](/handbook/customer-success/tam/cs-top-10)
- Maintaining [competitive](/handbook/marketing/product-marketing/competitive/) and [market assessments](#analyst-engagement) as well as missing [features](/features/)

Internal
- Leadership [OKRs](https://about.gitlab.com/company/okrs) set the direction for the company
- Each PM should be having regular [Group Conversations](https://about.gitlab.com/handbook/people-operations/group-conversations/) with their stage groups to discuss their vision with the company. PMs should share your 3 mo plan, year-long vision here, and relevant OKRs (with status) so everyone can contribute feedback.
- Dialogue with [internal customers](#dogfood-everything), and review of the [internal customer](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=internal%20customer) and [rebuild in GitLab](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=rebuild%20in%20GitLab) labels

#### Special consideration

The roadmap should also delimit which items will be delivered prior to the
next [summit](/company/culture/contribute/), as well as prior to
the end of the current calendar year. These delimitations will help determine
both the internal and external communication for both product and marketing.

#### Managing Upcoming Releases

Refer to the [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline)
for details on how Product works with UX and Engineering to schedule and work on
issues in upcoming releases.

#### Planning for Future Releases
{: #planning-future-release}

Product Managers assign milestones to issues to indicate when an issue is likely
to be scheduled and worked on. As we consider more distant milestones, the certainty of
the scope of their assigned issues and their implementation timelines is increasingly
vague. In particular, issues may be moved to another project, disassembled, or merged
with other issues over time as they bounce between different milestones.

The milestone of an issue can be changed at any moment. The current assigned milestone
reflects the current planning, so if the plan changes, the milestone should be updated
as soon as possible to reflect the changed plan. We make sure to do this ahead
of starting work on a release. Capacity is discussed between the PMs and the
engineering managers.

In general, closer-to-current-time milestones are assigned to issues that are
higher priority. The timing also reflects an approximate product roadmap, with more
distant milestones reflecting increasing uncertainty.

The milestones are:

- Milestones associated with an actual upcoming release, e.g. `9.4`, `9.5`, etc.
- `Next 3-4 releases`
- `Next 4-7 releases`

These assigned milestones should be used as a signal for GitLab stakeholders and
collaborators, to help them with their own respective workflows.

In addition, we have two special milestones: `Backlog` and `Awaiting further demand`.
Product Managers assign these issues to milestones that they have reviewed and
make sense, but do not fit within the upcoming release milestones due to either
a lack of comparitive urgency or because we have not yet seen enough user
demand to prioritize the item yet. The best way to demonstrate urgency on
either of these items is to vote on them and, if possible, add comments
explaining your use case and why this is important to you.

Again, the milestone of an issue can be changed at any moment, including for both
of these special milestones.

For a detailed timeline, see [the product development timeline](/handbook/engineering/workflow/#product-development-timeline).

#### Shifting commitment mid-iteration

From time to time, there may be circumstances that change the ability for a team
to ship the features/issues they committed to at the beginning of the iteration.
When this happens, as a PM you must ensure the related issues  and the roadmap
are updated to reflect the new plan (for example, remove `deliverable`
tag, update `milestone`, etc.). Additionally, notify your manager of the shift
and update the [kick-off document](https://docs.google.com/document/d/1ElPkZ90A8ey_iOkTvUs_ByMlwKK6NAB2VOK5835wYK0/edit)
to reflect the status of the item within the iteration (merged, not merged).

#### Utilizing our design system to work autonomously

Our [design system](https://design.gitlab.com) provides the means to work
autonomously, without always needing UX insight and feedback. When problems can
be solved using an already documented paradigm, you don't need to wait for UX
approval to bring an issue to a reasonable state within a first iteration.

If lingering questions remain, subsequent iterations can address any shortcomings
the feature might have.

#### Introducing a breaking change in a minor release

As a product manager, you should carefully consider the costs and benefits when
planning to introduce a breaking change. Breaking changes may heavily impact existing
users, and it is your responsibility to minimize the negative effects.

If you want to introduce an urgent breaking change in a minor release (e.g. you
need to provide support for a new feature that cannot be backward compatible), you
have to consider how many users will be affected by the change, and whether the change
can wait until the next major release instead.

If you evaluate that the impact on users is acceptable (e.g., you have evidence
that the feature is not used significantly), and the change will allow many users
to benefit of new features or to solve problems, follow this process:

1. Create an issue to discuss the breaking change, describing it in details including
the impact and benefits
1. Consider if you really need to do a breaking change in a minor. Can you wait
for the next major release
1. Communicate the breaking change as soon as possible, for example:
  - Publish a blog post about the upcoming change, with a timeline and a simple way
    to recognize who is affected, and who is not
  - Ask to schedule tweets about the blog post in the `#twitter` Slack channel
  - Ask to reach out customers that may be affected by the change in the
    `#customer-success` and `#sales` Slack channels
  - Mention the problems customers may report and how to address them in the
    `#support_self-managed` and `#support_gitlab-com` Slack channels
1. Throughout this process, think like a customer. Figure out actions that could
make the breaking change less painful from their point of view.
1. Always keep the issue up-to-date

### Community participation

Engaging directly with the community of users is an important part of our jobs. We
do this through conference attendance, tweeting about our new features, engaging on
Reddit, Hacker News, and other sites. There are a few Slack channels that can help
you keep abreast of mentions:

- #mentions-of-gitlab
- #hn-mentions
- #reddit
- #twitter

#### Conferences

A general list of conferences the company is participating in can be found on our
[corporate marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues?label_name%5B%5D=Corporate+Event&sort=due_date) project.

There are a few notable conferences that we would typically always send PMs to:

- [KubeCon](https://events.linuxfoundation.org/)
- [Atlassian Summit](https://www.atlassian.com/company/events/summit)
- [GitHub Universe](https://githubuniverse.com/)
- [DevOps Enterprise Summit](https://events.itrevolution.com/)
- [Google Next](https://cloud.withgoogle.com/next)
- [AWS Reinvent](https://reinvent.awsevents.com/)
- [Velocity](https://conferences.oreilly.com/velocity)

If you're interested in attending, check out the issue in the corporate marketing
site and volunteer there, or reach out to your manager if you don't see it listed
yet.

### Communication

- [**Public Issue Tracker (for GitLab Community Edition)**](https://gitlab.com/gitlab-org/gitlab-ce/issues)
and [**for GitLab Enterprise Edition**](https://gitlab.com/gitlab-org/gitlab-ee/issues) - please use
confidential issues for topics that should only be visible to team members at GitLab.
- [**Chat channel**](https://gitlab.slack.com/archives/product) - please use the
`#product` chat channels for questions that don't seem appropriate for the
issue tracker or more generic chat channels.

#### Communication guidelines for product managers

As a product manager, you're vastly more knowledgeable about GitLab than others.
This means that you have the responsibility to always provide a balanced and complete
view when discussing anything related to product. Other people won't have the same
background and context you might have.

For example, when someone involved in sales asks you about upcoming issues in a particular
area, you would respond with:

- a clear overview of what is coming (link to the [single source of truth](https://www.youtube.com/watch?v=Ne0QwGHwiP4))
- how that relates to the customer's request
- that this is flexible and we are interested in hearing more from the customer
- if necessary: a suggestion to discuss this directly with the customer

This seems obvious, but a slight misunderstanding can have big consequences, for
example: promising a customer that something will be done by a certain date is very
different than indicating that we're working on something and hope to have a first
iteration in the near future.

#### Internal and external evangelization

Before shipping a new or updated feature, you are responsible for championing
it, both internally and externally. When something is released, the
following teams need to be aware of it as they will all need to do something
about it:

* Marketing: depending on the importance of the feature, we need the help of
marketing to promote this feature on our different communication channels.
* Sales: sales needs to know what's new or changed in the product so they can
have better arguments to convince new or existing customers during their sales
process.
* Support: as they are in constant contact with our users and customers,
support should know exactly how our products work.

You can promote your work in several ways:

* start with documenting what will be released and share this documentation with
the different teams
* schedule meetings, if you think it's important, with the teams listed above.

#### GitLab University

To promote a major new feature internally, you can ask to host a GitLab <-- internal promotion, but GitLab University is external? -->
University, a company wide demo session. This is a great opportunity to make
sure every one is on the same page.

#### Communicating product vision

Occasionally, we rally around a product vision, a direction to aim towards, to
improve alignment internally and externally. We usually start out with a clear,
simple concept like [going faster from idea to production](/2016/08/05/continuous-integration-delivery-and-deployment-with-gitlab/#from-idea-to-production-with-gitlab),
or shipping [complete DevOps](/2017/10/04/devops-strategy/). We announced our
first Master Plan as part of our [Series B funding announcement](/2016/09/13/gitlab-master-plan/),
and our second master plan with our [Series C announcement](/2017/10/09/gitlab-raises-20-million-to-complete-devops/).
While the first iterations and presentations are great for aligning the product team
and announcing the vision to the world, the text and slides only go so far to
convey the depth of the vision. So, we usually iterate on the vision by fleshing
it out more in various stages and communicating it in different ways, usually
increasing fidelity over time.

This iteration may look like this, for example:
* start with a [Google doc](https://docs.google.com/document/d/1TKqHSwCdxXOb27cqKoHOrnYIA4waZE25SVsexLqp8zE/edit),
* turn that into a [single-source-of-truth based on issues](/direction/product-vision/),
* then [slideware](https://docs.google.com/presentation/d/1d6vL4dz-V_JxiStu4keL01SLd7w0JfNfcHzyetczp7k/edit)
* then a [mockupware video](https://youtu.be/lRAKmTzpGXE)
* then an [interactive prototype](https://framer.cloud/UaofH/index.html) and [recorded video](https://youtu.be/RmSTLGnEmpQ)
* then a seamless, but still fake video demo
* then finally a production demo.

The interactive prototype video is a good time to reiterate the vision
with a blog post.

Additionally, the [direction page](/direction/#devops-stages) contains the specific vision for each devops stage along with
links to the epics that contain the work to be done. We aim to use these epics and issues as the single source of truth for
our plans, and as such we strive to maintain them up-to-date with the latest developments and plans.

#### Working with Product Marketing (PMM)

Product marketers and managers should be joined at the hip. Just as a feature without documentation
should not be considered shipped, benefits of GitLab that we're not actively talking about might
as well not exist.

Product marketers rely on product managers to be guided to what is important and high impact.
In general, you should:

- always mention the [appropriate PMM](/handbook/product/categories) on epics and high level issues
- regularly meet/talk async with the PMM that is aligned with your product area
- proactively reach out for input when contemplating new features
- involve PMM as early as possible with work on important changes

#### Marketing materials

As a PM you're responsible for making sure changes you've shipped are well represented
throughout GitLab's documentation and marketing materials. This means that on
release, [`features.yml`][features] is updated, documentation is merged and deployed, and
any existing content is updated where necessary.

It's not acceptable to do this after the release. GitLab is very complex, and features
and functions are easily missed, even those that provide significant value to customers
(e.g. the many ways you can authenticate with GitLab).

You can recruit the help of the marketing and technical writing team if needed,
but it's highly recommended to do small updates yourself. This takes less time
and overhead than communicating what needs to be done to someone else.

#### Major feature rollout

Major features deserve proper attention from Product and Marketing. With a
proper rollout, we'll have ample marketing opportunities and receive more
feedback ahead of, during, and after the release.

Here is the ideal rollout schedule. For each step there is an indication for
who is responsible for it.

1. Feature is drafted in an issue (PM)
1. Feature is planned in an upcoming release (PM)
1. A feature proposal blog post is made (PM or Dev), which includes:
	* What we are planning on doing.
	* How people will be able to get it: CE or any EE Editions.
	* A link to the issue.
	* When it'll be available, if possible.
	* Anything else that is interesting to share in order to fuel the discussion.
1. Feature is implemented, and documentation is written (Dev).
1. Feature should appear on the website (Marketing)
	* For very significant features: Feature page on the website is made and
      pushed, with the mention "Available from X.X"
	* For other features: Feature should be listed on some page (/devops-tools,
      Enterprise page, /features page).
1. Feature is launched with the release (Marketing)
	* "Available from X.X" is removed
	* Documentation and other resources are linked
	* Pricing page is updated if needed
1. Feature is highlighted in a blog post (Marketing)
	* This post is linked from the feature page on the website (if applicable)

#### Release posts

As a PM, you are [accountable](/handbook/marketing/blog/release-posts/#general-contributions)
for adding new features (under your umbrella) to the monthly release post, respecting the
guidelines defined in the
[release posts handbook](/handbook/marketing/blog/release-posts/) and its **due dates**.
Be sure to go over all the details.

Every month, a PM will take the
[leadership](/handbook/marketing/blog/release-posts/#authorship)
of the release post, and will be responsible for delivering it in time.

#### Writing release blog posts

For every monthly release, there is a blog post announcing features.
The blog post should contain everything _exciting_ or _disruptive_.
All new features should appear in the blog post.
We want to help people understand exciting features (which are often new), and increase adoption.
Disruptive features may change workflows or even introduce unavoidable inconveniences.
We want to anticipate questions and avoid confusion by communicating these changes through the blog post.
Smaller tweaks and bug fixes don't necessarily need to be mentioned,
but if interesting, can be included at the bottom of the post.

#### Writing about features

As PM we need to constantly write about the features we ship: in a blog post,
internally to promote something, and in emails sent to customers.

While we want every PM to have a unique voice and style, there are some
guidelines that one should take into account when writing about features. Let's
highlight them with a concrete example, Preventing Secrets in your repositories,
 that [we shipped in 8.12](/2016/09/22/gitlab-8-12-released/#preventing-secrets-in-your-repositories-ee).

* Start with the context. Explain what the current situation is without the
  feature. Describe the pain points.

> It's a bad idea to commit secrets (such as keys and certificates) to your
repositories: they'll be cloned to the machines of anyone that has access to the
repository. If just a single one is insecure, the information will be
compromised. Unfortunately, it can happen quite easily. You write
`git commit -am 'quickfix' && git push` and suddenly you've committed files that
were meant to stay local!

* Explain what we've shipped to fix this problem.

> GitLab now has a new push rule that will prevent commits with secrets from entering the repository.

* Describe how to use the feature in simple terms.

> Just check the checkbox in the repository settings, under push rules and
GitLab will prevent common unsafe files such as .pem and .key from being committed.

* Point to the documentation and any other relevant links (previous posts, etc).

### Stakeholder Management

#### Updated SSOT for stakeholder collaboration

Stakeholder collaboration and feedback is a critical competitive advantage here
at GitLab. To ensure this is possible, and facilitate collaboration, you should
maintain an updated single source of truth (SSOT) of your stage direction, category
visions, and roadmap, at all times. This equips anyone who wants to contribute to
your stage’s product direction with the latest information in order to effectively
collaborate.

Actively and regularly reach out to stakeholders. Encourage them to view and collaborate
on these artifacts via these (non-exhaustive) opportunities:

- Engage with users in epics, issues, and merge requests on GitLab.com.
- [Meet with customers directly.](#customer-meetings)
- Participate in the [CAB].
- Talk with GitLabbers using GitLab.
- Talk with GitLabbers in [group conversations](/handbook/people-operations/group-conversations/).
- Talk with other PMs and Product leadership to align your stage’s product direction with the rest of GitLab.

Here is some guidance for new PMs to ensure your stage direction, category visions and roadmap
are up-to-date and visible to critical stakeholders:

- Seek feedback from the [CAB] once every six months.
- Present your roadmap to your manager once a month.
- Present the roadmap and category visions to the company via your regular [group conversations](/handbook/people-operations/group-conversations/).
  Ensure your content covers the group's progress against product [OKRs](/company/okrs/)
  and [maturity](/handbook/product/category/maturity/) goals. Specifically,
  report on what has changed since the last group conversation, including what
  categories have matured and any changes to future maturity plans such as
  descoping a category.
- Present your stage vision and roadmap in a [customer meeting](#customer-meetings) once every two weeks.
- Present changes to your stage vision, category visions, and roadmap to your
  stage group weekly meeting once a month.

#### Customer meetings

It's important to get direct feedback from our customers on things we've built,
are building, or should be building. Some opportunities to do that will arise during
[sales support meetings](#sales-support-meetings). As a PM you should also have dedicated
[customer discovery meetings](#customer-discovery-meetings) with customers and prospects
to better understand their pain points.

As a PM you should facilitate opportunities for your engineering group to hear
directly from customers too. Try to schedule customer meetings at times that
are friendly to your group, invite them, and send them the recording and notes.

##### Sales Support Meetings

**Before the meeting**, ensure the Sales lead on the account has provided you with sufficient
background documentation to ensure a customer doesn't have to repeat information they've already
provided to GitLab.

**During the meeting**, spend most of your time listening and obtaining information.
It's not your job to sell GitLab, but it should be obvious when it's the time
to give more information about our products.

After the meeting, make sure all your notes and feedback land in issues.

##### Customer Discovery Meetings
Customer discovery meetings aren't UX Research. Target them to broad-based needs
and roadmap tradeoff discussions, not specific feature review. There are
two primary techniques for targetting those topics:

* **Top Competitors** - Identify the top 3 competitors in your categories and talk to
  customers using those competitor asking: What is missing to have you switch from
  X to us? We’re not aiming for feature parity with competitors, and we’re not
  just looking at the features competitors talk about, but we’re talking with
  customers about what they actually use, and ultimately what they *need*.
* **User Need** - Identify GitLab users from key customers of your group's
  categories and features. Solicit them for what they love about the features and
  ask about their current pain points with both the features as well as the surrounding
  workflows when using those components of GitLab?

**Set up a meeting** by identifying what you're interested in learning
and prepare appropriately. You can find information about how customers are
using GitLab through Sales and version.gitlab.com. Sales and support should
also be able to bring you into contact with customers. There is no formal internal
process to schedule a customer meeting, but if the need arises, we can formulate one.

**During and after** spend most of your time listening and documenting information.
Listen for pain points. Listen for delightful moments. Listen for frustrations. Write down
your findings in GitLab issues. Read back and review what you've written down with the customer
to ensure you've captured it correctly.

**After the meeting** make sure all your notes and feedback land in appropriate issues. Make
appropriate adjustments to category visions, feature epics, and personas. Share your findings with your
fellow product managers and the sales and customer success account teams for the customer.

You can find some additional guidance on conducting Customer Discovery Meetings from these
resources:
* [How to Interview Your Customers](https://customerdevlabs.com/2013/11/05/how-i-interview-customers/)
* [Effective User Interviews](https://www.productmanagerhq.com/2018/08/effective-user-interviews/)

#### Customer Advisory Board Meetings

One specific, recurring opportunity to get direct feedback from highly engaged customers
is the [GitLab DevOps Customer Advisory Board](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#gitlab-devops-customer-advisory-board).
You may be asked by the CAB to present your stage at these meetings. Here are
some guidelines when doing so:
- Since it will be sent out in advance of your presentation, take the opportunity to update your [stage vision](#stage-vision) video
- Start the presentation with an overview of your stage vision
- Emphasize the importance of feedback and dialog in our [prioritization process](#prioritization)
- Highlight recently completed roadmap items that were driven by customer feedback
- Come prepared with five questions to facilitate a discussion about industry trends,
roadmap tradeoffs, pain points and existing features
- Don't simply look for places to improve, seek to clarify your understanding of what customers
currently value and love

#### Working with (customer) feature proposals

When someone requests a particular feature, it is the duty of the PM to investigate
and understand the need for this change. This means you focus on what is the problem
that the proposed solution tries to solve. Doing this often allows you to find that:

1. An existing solution already exists within GitLab
1. Or: a better or more elegant solution exists

Do not take a feature request and just implement it.
It is your job to find the underlying use case and address that in an elegant way that is orthogonal to existing functionality.

This prevents us from building an overly complex application.

Take this into consideration even when getting feedback or requests from colleagues.
As a PM you are ultimately responsible for the quality of the solutions you ship,
make sure they're the (first iteration of the) best possible solution.

#### Competition Channel

When someone posts information in the `#competition` channel that warrants
creating an issue and/or [a change in `features.yml`][features], follow this
procedure:

- Create a thread on the item by posting `I'm documenting this`
- Either do the following yourself, or [link](#competition-channel)
to this paragraph for the person picking this up to follow
- If needed: create an issue
- [Add the item to the `features.yml`][features]
- If GitLab does not have this feature yet, link to the issue you created
- Finish the thread with a link to the commit and issue

#### How and when to reject a feature request

Rejecting a feature request or a merge request is not an easy thing. People can
feel quite protective of their ideas. They might have invested a lot of time and
energy in writing those ideas. You can be tempted to accept a feature only to
avoid hurting the people who thought of it. Even Worse, if you reject an idea too
harshly, you might discourage other people to contribute, which is something we
should strive to avoid.

However, as the number of issues and merge requests grows incessantly, we should
be diligent about rejecting features we are sure will not work out. It's better for
everyone: for the product team, so we don't maintain a huge backlog of things we
will never do anyway, and for the users who won't have to wait for our feedback
indefinitely.

Note: it's fine to not respond to issues that we think have potential until they
gather momentum.

Feature requests and merge requests can be rejected for the following reasons:

* Not within our scope: the Direction page [lists all the areas](/direction/#scope)
where GitLab, the product, won't go. Point the issue's author to this article
and close the issue.
* We don't want another setting: whenever we can, we try to [avoid having settings](#convention-over-configuration).
Some settings are unavoidable, but most aren't. Ask the user to change how she
approaches the feature in order to get rid of the setting.
* Too complex: We want to have a simple, user-friendly product that does complex
things, not the other way around. Tell the user to take a step back and think
about the problem to be solved in the first place. Offer directions on
what could be done instead. If she's not willing to do it, indicate that you will
have to close the issue/merge request because it will go nowhere.
* Brings an Enterprise exclusive feature to the Community Edition: this problem
is already addressed in the [Stewardship page](/company/stewardship/#contributing-an-existing-ee-feature-to-ce).
Indicate that we will investigate whether the feature can be ported to the
Community Edition, discuss it with other teams members and come back to the user
in a timely fashion.
* Low priority: sometimes features are interesting but we simply don't have the
capacity to implement them. In that case, simply tell the truth and indicate that
we don't have enough resources at our disposal to do it at the moment.

Don't forget to thank the authors for the time and effort taken to submit the
feature request/merge request. In all cases and without exception, you should be
nice and polite when interacting with users and customers.

#### Analyst Engagement

Part of being a product manager at GitLab is maintaining engagement with
analysts, culminating in various analyst reports that are applicable to your
stage. In order to ensure that this is successful and our products are rated
correctly in the analyst scorecards, we follow a few guidelines:

- Spend time checking in with the analysts for your area so they are familiar with our story and features earlier, and so we can get earlier feedback. This will ensure better alignment of the product and the way we talk about it will already be in place when review time comes. Remember, analysts maintain a deep understanding of the markets they cover, and your relationship will be better if it is bi-directional. Inquire with analysts when you have questions about market trends, growth rates, buyer behavior, competitors, or just want to bounce roadmap ideas off of an expert.
- Make paying attention to analyst requests a priority, bringing in whoever you need to ensure they are successful. If you have a clear benefit from having executives participate, ask. If you need more resources to ensure something is a success, get them. These reports are not a "nice to have", ad-hoc activity, but an important part of ensuring your product areas are successful.
- When responding to the analyst request, challenge yourself to find a way to honestly say "yes" and paint the product in the best light possible. Often, at first glance if we think we don't support a feature or capability, with a bit of reflection and thought you can adapt our existing features to solve the problem at hand. This goes much smoother if you follow the first point and spend ongoing time with your analyst partners.
- Perform retrospectives after the analyst report is finalized to ensure we're learning from and sharing the results of how we can do better.

It's important to be closely connected with your product marketing partner,
since they own the overall engagement. That said, product has a key role to play
and should be in the driver's seat for putting your stage's best foot forward in
the responses/discussions.

#### Internal customers

Product managers should take advantage of the internal customers that their
stage may have, and use them to better understand what they are really using,
what they need and what they think is important to have in order to replace
other products and use GitLab for all their flows.

We want to meet with our internal customers on a regular basis, setting up
recurring calls (e.g., every two weeks) and to invite them to share their
feedback.

This is a mutual collaboration, so we also want to keep them up to date with the
new features that we release, and help them to adopt all our own features.

#### Dogfood everything

The best way to understand what pains users experience is to go through what we
ask them to do to set up or use GitLab. As a PM, you should go through
every feature, at the minimum the ones you are responsible for. All of them. That
includes features that are not in GitLab's UI directly but require server
configuration. If you, as a PM, can't understand the documentation, or if you
struggle to install something, would anyone else bother to do it too? Going
through this is not only beneficial for understanding what the pain points are,
it will also tell you what can be enhanced, such as a better flow or better
documentation.

As a Product function, it is our responsibility to ensure that the entire
company is dogfooding. We do this by:
1. Maintaining a set of internal stakeholders who represent GitLab team members
  who use GitLab for the purposes of developing and operating GitLab and
  GitLab.com.
1. Including top internal customer issues in the relevant [category
  epics](/handbook/product#category-vision).

More than just using the features ourselves as Product Managers, internal usage
of GitLab by our teams has an outsized impact. This impact includes:
- Reduced costs resulting from GitLab not having to build or buy a tool outside of our
single application
- Increased revenue and stickiness of our product resulting from the increased functionality
needed by our internal stakeholders

The impacts are not only outsized, but they are more likely to materialize quickly. That is because
working with internal stakeholders comes with the additional benefit of having immediate feedback. We can reduce
cycle times with more actionable feedback to de-risk early investments in new categories and features by having
a strong internal stakeholder dogfooding early. Due to these impacts, a general rule of thumb is to evaluate an
internal stakeholder request as 10x a customer request.

It's because of this outsized impact that, all other factors being similar, we should choose to more highly prioritize issues
requested by internal stakeholders. For example, when comparing an issue that 120 external customers have upvoted with an issue that 111 customers have upvoted,
some of which are internal, we *should* prioritize the latter issue over the former.

When internal customers are evaluating whether to dogfood, we should use the following process
to confirm the outsized impact is being considered:
1. Internal stakeholder creates an issue for the relevant group.
1. Internal stakeholder applies the `internal customer` label and mentions the relevant Product Mananger.
1. Product Manager prioritizes appropriately and provides an estimate when the feature will be delivered.
1. Internal stakeholder determines whether the timeline is acceptable.
1. If not the internal stakeholder team should use a '2x' rule to evaluate whether they should build the tooling inside of GitLab themselves.
If the effort will take a team up to twice as long to build into our product, they should build it within GitLab.
1. If the internal stakeholder chooses to build tooling outside of the product they should add a note justifying why
they are choosing to do so on the issue.

We should strive that less than 10% of internal stakeholder requests aligned to our product vision choose to build tooling outside of our product.

Here is a [partial list of features and functionality that currently exist outside the GitLab project, that should be brought inside](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=rebuild%20in%20GitLab). Note: use the `Rebuild in GitLab` label to add an issue to this backlog.

##### Example: configuring GitLab

Most of GitLab is configured through the file `gitlab.rb`. It's tempting to
add a new parameter in this file - it's easy, fast to do, and won't require
adding a new UI element to allow the configuration of this new setting. However,
changing this file requires you to reconfigure GitLab. To do that, you have to
login to your server and type in commands to reconfigure your instance,
possibly multiple times if you have more than one server.

This is not something that we can ask our customers to do. Only by using your
own product and features will you realize that some practices should be avoided
as much as possible.

### Issues & Epics

#### When to create an issue

You should create an issue if:

- There isn't already an issue for what you're intending to write. Search first.
- a feature is mentioned in chat channels like #product, #competition or elsewhere
- a customer requests a particular feature

You should consider **not** creating an issue when:

- It's an iteration on something we haven't built yet.
- You're planning very far ahead and it's something specific. The further away something is,
the more vague or encompassing the issue should be. So, for example, create just one issue
for a distant new feature, rather than several issues that will follow each other.
This will reduce the total amount of issues.
- There is already an existing issue. Even if the quality is low, people might
have linked to it. Consider rewriting the description instead and leaving a comment
that you did so. Closing issues to reopen the same issue is generally not a good idea.

#### Issue state
When an issue is open, this signals that we're intending or considering implementing that change.
It's important to close issues that we're not considering implementing in the near future,
so that we avoid creating uncertainty with customers, and colleagues.

When closing an issue for this reason, make sure to update the issue body and leave a comment
explaining why the issue is being closed. Issues can be reopened if we change our stance on them.

#### When to close an issue
In order to clearly communicate to our stakeholders our plans, it's critical that you not only
provide the positive view (what we will do) but also articulate the negative view (what we will not
do). While this should be communicated in [stage](#stage-vision) and [category](#category-vision) visions,
it starts with issues:

As a Product Manager you should close issues that are:

1. duplicated elsewhere.
1. no longer relevant due to other reasons.
1. 'not the next iteration': an iteration on a proposed feature that is unlikely to ship in the next few months.
1. 'won't do': An issue that we have [no intention on implementing](https://gitlab.com/gitlab-org/gitlab-ce/issues/12736#note_50662947) because it does not fit within or is antithetical to our [vision](https://about.gitlab.com/direction/#product-vision), it presents a security risk or other reasons you outline in the issue.

When closing an issue, leave a comment explaining why you're closing the issue and link
to anything of relevance (the other duplicate, the original feature that this is an iteration on, etc).

The 'not the next iteration' issues are the most important ones to resolve.
It is very easy to create a big plan with meta issues and lots of improvements,
but it is essential that we iterate and ship the _minimum viable_ change.
We have to ship the iteration, wait for it to be used, and look for the feedback.
As a product manager you must think about the bigger picture when making a proposal to improve the product.
It's important to avoid writing this down as a bunch of issues.
Come up with a plan but only record the first step.
This way we can preserve the efficiency of [our value of iteration](/handbook/values/#iteration).
Closing issues whenever possible is an important part of your job and helps to keep a clear view of what is next.
Consider using the following template to close an issue:

> Closing this because XXX is something we should do first. When that feature is
finished, we can learn from observing it in use. If we learn that this issue is
still relevant, we can then reopen it. See /handbook/product/#when-to-create-or-close-an-issue
for more detail about this policy.

#### How to submit a new issue

1. If you have time, the first thing you should do is search both CE and EE
projects to see if a similar issue already exists. We shouldn't create
duplicates if we can avoid them.
1. Identify if the issue is about GitLab Community Edition (CE) or GitLab
Enterprise Edition (EE), although this can easily be changed later.
1. You should clearly state what the current pain point is, what we are trying
to solve, what the benefits will be, what it should do, how to accomplish that
and the next steps.
1. The body of the issue should be written in a clear way, without ambiguity.
1. The initial issue should be about the problem we are solving. If a separate [product discovery issue](#product-discovery-issues) is needed for additional research and design work, it will be created by a PM or UX person.
1. If the body contains too many paragraphs, it can surely be rewritten to be shorter.
1. Do not use acronyms or abbreviations. Everyone should be able to jump on the
issue and participate without needing a glossary.
1. Choose labels which are relevant to the issue. If you are unsure about what
certain labels are for, check the labels pages ([CE](https://gitlab.com/gitlab-org/gitlab-ce/labels)
or [EE](https://gitlab.com/gitlab-org/gitlab-ee/labels)), and read the
descriptions. The [contributing doc](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md)
provides a breakdown of the label types and how to choose the right label.
1. Unless you know what you are doing, do not
    - assign someone to the issue
    - assign a milestone
    - set a due date
    - add weight - weight represents the technical complexity and should be
    defined by our developers
1. Mention the different stakeholders in the body of your issue. In most product
related issues, we usually mention the product manager, the design, frontend, and backend managers as appropriate.
Some teams have [experts](/company/team/structure/#expert) or [liaisons](/job-families/liaison) that can be mentioned instead of the managers.
Mentioning the people in the body of the issue will trigger the notification mechanisms
chosen by the people who are mentioned - therefore there is no need to notify
people in another channel after the issue has been created (Slack, email).

#### How to use epics

Issues related to the same feature should be bundled together into an
into an [epic](https://docs.gitlab.com/ee/user/group/epics/).

##### Epics for a single iteration

Features may include multiple issues even if we are just targeting an MVC. In
this case, we should use an epic to collect all of them in one single place.
This epic should have a start and an end date, and it should not span more than
3 releases, otherwise we run the risk of having epics drag on indefinitely.

When these issues are finished and closed, we should have successfully achieved the
epic's goal. A good example of this kind of epic is the first iteration on a new
feature. Epics representing MVCs should clearly state `MVC` at the end of the
title and should have a parent epic relationship towards a category vision epic or a meta epic.

##### Epics for a long-term plan

We use epics to track many issues related to a specific topic, even if there
isn't a specific timeline for shipping. These epics should be marked as ~meta, they
may not have a specific start or end date, and may contain single iteration epics.

This is useful to have an overview of the future so participants and observers can
understand how the pieces fit together. It can be really useful to understand the
context of where we think we’re going so we can do a good job with the MVC.

Also, it conveys confidence internally and externally that we understand what
we need to do when the MVC is particularly minimal. But don't get too caught up
in long-term epics as you risk making proposals too complex, and overrepresenting
certainty in the solution. Don't work too far in the future; we'll know better
once we ship something small.

When creating a vision epic, there's a natural tendency to capture it as quickly
as possible and then move on, but we must always strive to create a more
specific epic for the first iteration, or the next iteration if something
already exists. Describing an MVC means that the community can contribute more
easily. Also, after distilling things down to a first iteration, you might
realize it’s a lot easier than you thought, and prioritize it earlier. You can
have an MVC without a Vision. But **you can't have a Vision without an MVC**.

##### Epics for a category vision

See [category vision](#category-vision).

#### Product Discovery Issues

When a product discovery step is needed to design a feature, PMs should
create a separate issue (linked to the epic and implementation issue) for that
research and design work to occur. We should not repurpose or otherwise reuse
the existing implementation issue; doing so creates a risk of the issue being
closed when the design work is complete and thereby losing track of the actual
delivery of the item.

It's also important to ensure that product discovery issue is really even
needed. It's possible in many situations to simply use the initial ticket for
any design discussions - this will ensure all of the back and forth is in one
place, avoiding the risk of losing track of parts of the discussion. If you
find you are creating a issue simply to reserve time or people on the roadmap,
consider trying to secure the capacity in another way.

In certain cases a product discovery issue can be worked ahead of time, but a
best-practice for good product development is to plan for and do this sort of
research and development work within the milestone, including engineering and other
stakeholders directly. Doing product discovery ahead of time, without the full
group is generally not an effective approach.. Because we aim to deliver MVC
features, there should almost always be enough time in a single release to both
research and deliver a feature that adds value for our customers.

Be sure to follow the [CE contribution guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#implement-design-ui-elements) on correct
labeling practices for the ``product discovery`` label.

#### Wireframes

When relevant, you can include a wireframe in your issues in order to illustrate
your point. You don't need to include wireframes on your own; our UX/design team can
help us on that matter. Simply ping them if you need their help. We like
[Balsamiq](https://balsamiq.com/) for its simplicity and its sketch-style approach.
If are struggling for inspiration, you can also paste screenshots of similar
features in other products.

#### What is a Meta issue?

We assign the `Meta` label to issues that contain a large list of todos. If you
are familiar with the Agile methodology, it's similar to an epic. At GitLab we
have a short release cycle: the 22nd of every month. In some cases we won't be
able to tackle all the tasks of a meta issue in a single release. This is why
we centralize everything that we need to do in a meta issue, then break it
down to issues small enough that they will fit into one release. Most of the
time, meta issues generate lots of comments and passionate discussions. As a
consequence, they always lead to something great.

Meta issues themselves generally should not be assigned to a milestone as the
actual work is covered in sub-issues. Sometimes, for example if you want a meta
issue to show up in our [direction](/direction) page for a given release, you may
add a milestone, but *only* if you know for sure that all sub-issues will be
*completed* by that milestone. Don't assign a milestone for when you're going to
*start* a meta issue.

#### Long-lasting issues

A general guideline is that an issue should only span one release. If we know an
issue is going to span multiple releases, split it up into multiple issues.

Meta/epic issues are the exception to this rule, but should be kept to a
minimum and only serve to guide broader subjects, not a single feature
over multiple releases. This is to make sure we stick to our values of
the [minimally viable change](#the-minimally-viable-change).
This means that feature issues should be closed after the first iteration
whenever possible. We'll know more in the future and this keeps any remaining
issues short and actionable.

In addition, it's often a good idea to throw away an original plan and
start fresh. Try to do this more often, even more than you're comfortable with.
Close the issue and create a new one.

#### Which issue should you be working on?

When you don't have any specific tasks assigned, you should work on issues that are
labeled `Product work`, in both the EE and CE projects. These are issues that need
our attention the most.

### Life Support PM Expectations

When performing the role of Life Support PM only the following are expected:

- Attend group meetings or asynch discussion channels
- Provide prioritization for upcoming milestones
- MVC definition for upcoming milestones
- Increase fidelity of scheduled issues via group discussion
- Ensure features delivered by the group are represented in the release post

Some discouraged responsibilities:

- Long-term MVC definition
- Roadmap beyond 3 months
- Category Epic updates
- Direction page updates
- Analyst engagements
- CAB presentations

### Team Meetings

#### Weekly Product Management Meeting
There is a weekly product management meeting open to any GitLabber. The structure of the meeting is as
follows:

1. We review a [GoogleDoc (internal only)](https://docs.google.com/document/d/1dAs9HoAbuiRzYYvk-AFMggbN-1Hb1l6L17jMzLePbkA/edit)
with `Agenda` items in the order in which they appear in the meeting. The Zoom Link and Meeting time are in the GoogleDoc.
1. We take notes on the discussion
1. We move the agenda items to date on which they were discussed

If you are a Product team member and are unable to attend the call you may add items for `READ-ONLY` to the agenda.

If you would like to join the call to discuss a specific agenda topic, please add it and any relevant context in advance
of the meeting.

#### Weekly Product Leadership Meeting
The Head of Product and their direct reports track our highest priority Product Team iniatives in the
[Product Leadership Issue Board](https://gitlab.com/gitlab-com/Product/boards/1021790?&label_name[]=Product%20Leadership).
In order to maintain strategic focus, there is a WIP limit of 6 for Product
Leadership items with the `Doing` label. We review the content and priority of this
board during 1:1s or staff meetings. Directors can delegate and coordinate cross-product
initiatives as needed based on the issues in this board.

As part of this Product Leadership Meeting we also [review progress towards our OKRs](https://gitlab.com/gitlab-com/Product/issues/187).

Items for `Discussion` only should be prioritized in advance of the meeting by the
meeting members, and ultimately the Head of Product in advance of the meeting.

Non-public items for discussion should be added directly to the agenda document for the meeting.

### Build vs "Buy"

As a Product Manager you may need to make a decision on whether GitLab should engineer a solution to a particular problem, or use off the shelf software to address the need.

First, consider whether our users share a similar need and if it's part of GitLab's scope. If so, strongly consider [building as a feature in GitLab](https://about.gitlab.com/handbook/product/#dogfood-everything):
* Evaluate if there is [compatible open source software](https://docs.gitlab.com/ee/development/licensing.html#acceptable-licenses) which could accelerate delivery.
* If time to market is an issue, a [global optimization issue](https://about.gitlab.com/handbook/product/#prioritize-global) may also be opened to assist with prioritization.
* For a potential acquisition, follow the [acquisition process](https://about.gitlab.com/handbook/acquisitions/acquisition-process/).

If the need is specific to GitLab, and will not be built into the product, consider a few guidelines:
* Necessity: Does this _actually_ need to be solved now? If not, consider proceeding without and gathering data to make an informed decision later.
* Opportunity cost: Is the need core to GitLab's business? Would work on other features return more value to the company and our users?
* Cost: How much are off the shelf solutions? How much is it to build, given the expertise in-house and opportunity cost?
* Time to market: Is there time to engineer the solution in-house?

If after evaluating these considerations buying a commercial solution is the best path forward:
1. Consider [who owns the outcome](https://about.gitlab.com/handbook/finance/#how-spend-is-allocated-to-departments), as the spend will be allocated to their department. Get their approval on the proposed plan.
1. Have the owning party [open a finance issue](https://gitlab.com/gitlab-com/finance/issues/new) using the `vendor_contracts` template, ensure the justification above is included in the request.

## GitLab as a Product

### Single application

We believe that a single application for the DevOps lifecycle
based on convention over configuration offers a superior user experience. The
advantage can be quoted from the [Wikipedia page for convention over
configuration](https://en.wikipedia.org/wiki/Convention_over_configuration):
"decrease the number of decisions that developers need to make, gaining
simplicity, and not necessarily losing flexibility". In GitLab you only have to
specify unconventional aspects of your workflow. The happy path is
**frictionless from planning to monitoring**.

We're doubling down on our product for concurrent DevOps which brings the entire
lifecycle into one application and lets everyone contribute. We are leaning into
what our customers have told us they love: our single application strategy, our
pace of iteration, and our deep focus on users.

Consider opportunities to take advantage of this unique attribute in early
iterations. Integrating features with different parts of the application can
increase the adoption of early iterations. Other advantages:

- A minimal viable feature that is well integrated can be more useful than a
sophisticated feature that is not integrated.

Although not every feature needs to be integrated with other parts of the
application, you should consider if there are unique or powerful benefits for
integrating the feature more deeply in the second or third iteration.

### GitLab.com

GitLab.com runs GitLab Enterprise Edition.

To keep our code easy to maintain and to make sure everyone reaps the benefits
of all our efforts, we will not separate GitLab.com codebase from the Enterprise Edition codebase.

To avoid complexity, [GitLab.com tiers and GitLab self-managed tiers](/handbook/marketing/product-marketing/tiers/) strive to match 1:1.

- Free: Core features
- Bronze: Starter features
- Silver: Premium features
- Gold: Ultimate features
- Public projects are considered to have a Gold subscription level

Since we are not able to give admin access and do not yet have full feature
parity between self-managed instances and GitLab.com, we avoid saying that there is
a one to one match between subscription levels and tiers in marketing materials.
This has been a source of confusion in the past for customers.

#### GitLab.com subscription scope and tiers

GitLab.com subscriptions work on a namespace basis, which can mean:

- personal namespace, e.g. `JaneDoe/*`
- group namespace, e.g. `gitlab-org/`

This means that group-level features are only available on the group namespace.

Public projects get Gold for free.
Public groups _do not_ get Gold for free. Because:

- this would add significant additional complexity to the way we structure our features, licenses and groups. Because we don't want to discourage public groups, yet it wouldn't be fair to have a public group with only private projects and still get all the benefits. That would make buying a license for GitLab.com almost entirely moot.
- All value of group level features is aimed at organisations, i.e. managers and up (see our [stewardship](/stewardship)). The aim with giving all features away for free is to enable and encourage open source projects. The benefit of group-level features for open source projects is significantly diminished, therefore.
- Alternative solutions are hard to understand, and hard to maintain.

Admittedly, this is complex and can be confusing for product managers when implementing features.
Ideas to simplify this are welcome (but note that making personal namespaces equal to groups is not one of
them, as that introduces other issues).

### Gitlab.com Metrics

In addition to the two [long term financial success criteria](#long-term-financial-success-criteria-for-gitlabcom) listed above, you can find the full list of metrics used by Product and Finance and their definitions [here](/handbook/product/metrics/#gitlabcom).

### Paid Tiers

Keep in mind that the [CEO is the DRI for pricing and tiers](/handbook/ceo/pricing/#departments).
Please review the entirety of the [stewardship](/company/stewardship/) and [pricing](/handbook/ceo/pricing/) pages before making any determinations of which tier a given feature should go in.

#### What goes in what paid tier

Our [stewardship principles](/company/stewardship/) determine whether something belongs in a
paid tier. The [likely buyer](/handbook/ceo/pricing/#the-likely-type-of-buyer-determines-what-features-go-in-what-tier) determines which tier.

#### Determining the tier of a new feature

The framework for determining initial tiers for features is provided in
the [pricing page](/handbook/ceo/pricing/). Use that as a guide when making
determinations. Be sure to document your rationale for picking non-Core tiers
in MVC issues. When talking about why a certain change goes into a paid tier
instead of Core, mention the [stewardship page](/company/stewardship/#what-features-are-paid-only)
in the handbook directly and link to it.

When talking about why a certain change goes into Core and not a paid tier, mention our
[promises](https://about.gitlab.com/company/stewardship/#promises). Sometimes we choose
to put a feature in Core because it is easier to maintain in the long term.

Should you have any questions when making this decision do not hestitate to mention your
manager, the VP or Head of Product or the CEO in the issue for clarification.

#### Bringing features to lower tiers

If you are considering bringing a feature that exists in Premium to Core, for
example, follow this process:

- Create a confidential issue describing the plan and the reasons behind this change
- Get approval from either the VP or Head of Product, and the CEO to proceed
- Share this issue for feedback with the rest of the team on the GitLab company call
- Join the weekly Sales Team call, explain the proposal, and direct them to the issue to provide feedback. If attending the meeting is not feasible, ping the CRO on the issue and ask them to add it to an upcoming Sales Team meeting agenda.
- After feedback has been provided update your recommendation and assign to the CEO for final approval

Once a final determination has been made, reference the confidential issue in the public issue with a brief statement
summarizing the result.

#### Changing a price or introducing a new price

If you are considering launching a paid feature, or changing any other aspect of pricing, follow this process:

- Create a confidential issue in the [Product issue tracker](https://gitlab.com/gitlab-com/Product/issues) describing the plan and the reasons behind this change. Ensure the issue contains the margin analysis for the proposed price in addition to other relevant details related to finance or accounting, such as roll over credits
- Get approval from the VP Product
- Get approvals from the [CEO](/handbook/ceo/pricing/#departments), CFO, CRO, and VP of Field Operations
- Share the issue for feedback with the rest of the team on the GitLab company call
- Join the weekly Sales Team call, explain the proposal, and direct them to the issue to provide feedback. If attending the meeting is not feasible, ping the CRO on the issue and ask them to add it to an upcoming Sales Team meeting agenda
- If necessary, update proposal and reobtain [CEO](/handbook/ceo/pricing/#departments) approval.

Once a final determination has been made, consider making the confidential issue non-confidential. Ensure product marketing has a related website page for the feature and price. Ensure documentation is updated to accurately portray all facets of the feature. Consider a blog post to outline the feature.

#### Paid Tier requirements

All Starter, Premium, and Ultimate features must:

- Work easily for our customers that self-host GitLab. i.e. Their
  licenses need not be updated and the new feature is default-on for the
  instance.
- Work with GitLab.com Bronze / Silver / Gold subscriptions. This means there has to be
  some way of toggling or using the feature at a namespace level.
- Have documentation.
- Be featured on [products](/products) and [DevOps tools](/devops-tools) at launch.

#### Designing features for paid tiers

To make it possible to ship a feature for paid tiers, ideally the code is
completely separate. For example, the frontend and backend of the feature only exist in
the `gitlab-ee` project. However, this is not always possible.

In cases where it's preferable to have the backend code live in the `gitlab-ce`
repository, it's acceptable to only ship the frontend for the feature in EE.
In practice this makes the feature paid-only.

### Alpha, Beta, GA

Occasionally we need to test large, complex features before we are confident
that we'll be able to scale, support and maintain them as they are.
In this case we have the option to release them as Alpha or Beta versions.

In general, we should avoid releasing Alpha or Beta versions of features.
A minimally viable change should be _viable_ and therefore should not need a
pre-release. That said, if there is a situation where we have no valid alternative,
the definitions of each stage is below.

It's never acceptable to make changes that risk any damage to existing production
data accessed by our users.

#### Alpha

- not ready for production use
- unstable and can cause performance and stability issues
- the configuration and dependencies are likely to change
- features and functions may be removed
- data loss can occur (be that through bugs or updates)

#### Closed Beta

Similar to Beta, but only available to selected users.

- not ready for production use
- unstable and can cause performance and stability issues
- configuration and dependencies unlikely to change
- features and functions unlikely to change
- data loss less likely

#### Beta

- not ready for production use
- unstable and can cause performance and stability issues
- configuration and dependencies unlikely to change
- features and functions unlikely to change
- data loss not likely

#### Generally Available (GA)

Passed the [Production Readiness Review](https://gitlab.com/gitlab-com/infrastructure/blob/master/.gitlab/issue_templates/production_readiness.md) for GitLab.com, which means that it is:

- ready for production use at any scale
- fully documented and supported

### Discouraging, deprecating and removing features

Deprecating features (changes) follows a particular pattern.
Use the language `Discouraged (maintained)`, `Deprecated (not maintained)` or `Removed` to
specify the state of a feature that is going to be or is removed.

Features that are discouraged, deprecated or removed should be:

1. Labelled accordingly in the documentation
1. Labelled accordingly in the application

Features that are Deprecated or Removed should be removed from marketing pages.

#### Discouraged (maintained)

- planned to be removed at some point in the future, but will be deprecated publicly ahead of time
- maintained: bugs and regressions are still fixed
- still available with new installations, but documentation mentions
that use is discouraged (with alternatives if relevant)

#### Deprecated (no longer maintained)

- may be removed at any point in the future
- no longer maintained: bugs and regressions are no longer fixed
- still available with new installations, but documentation mentions
deprecated state

#### Removed

- no longer available in the latest version of the product

### Naming features

Naming new features or renaming existing features is notoriously hard and sensitive to many opinions.

#### Factors in picking a name

- It should clearly express what the feature is, in order to avoid the [AWS naming situation](https://www.expeditedssl.com/aws-in-plain-english).
- It should follow [usability heuristics](http://www.designprinciplesftw.com/collections/10-usability-heuristics-for-user-interface-design) when in doubt.
- It should be common in the industry.
- It should not overlap with any other existing concepts in GitLab.
- It should have as few words as possible (so people won't use a shortened name).
- If you remove words from the name, it is still unique (helps to give it as few words as possible).

#### Process

- It's highly recommended to start discussing this as early as possible.
- Seek a broad range of opinions and consider the arguments carefully.
- The PM responsible for the area involved should make the final decision and not delay the naming.
- Naming should definitely not be a blocker for a feature being released.
- Reaching consensus is not the goal and not a requirement for establishing a name.

#### Renaming

The bar for renaming existing features is extremely high, especially for long-time features with a lot of usage.
Some valid but not exclusive reasons are:

- New branding opportunities
- Reducing confusion as we introduce new adjacent features
- Reducing confusion as we re-factor existing features

### Permissions in GitLab

Use this section as guidance for using existing features and developing new ones.

1. Guests are not active contributors in private projects. They can only see, and leave comments and issues.
1. Reporters are read-only contributors: they can't write to the repository, but can on issues.
1. Developers are direct contributors, and have access to everything to go from idea to production,
   unless something has been explicitly restricted (e.g. through branch protection).
1. Maintainers are super-developers: they are able to push to master, deploy to production.
   This role is often held by maintainers and engineering managers.
1. Admin-only features can only be found in `/admin`. Outside of that, admins are the same as the highest possible permission (owner).
1. Owners are essentially group-admins. They can give access to groups and have
   destructive capabilities.

To keep the permissions system clear and consistent we should improve our roles to match common flows
instead of introducing more and more permission configurations on each resource.

For big instances with many users, having one role for creating projects, doing code review and managing teams may be insufficient.
So, in the long term, we want our permission system to explicitly cover the next roles:

1. An owner. A role with destructive and workflow enforcing permissions.
1. A manager. A role to keep a project running, add new team members etc.
1. A higher development role to do code review, approve contributions, and other development related tasks.

All the above can be achieved by iteratively improving existing roles and maybe [adding one more](https://gitlab.com/gitlab-org/gitlab-ce/issues/45414).

[Documentation on permissions](https://docs.gitlab.com/ee/user/permissions.html)

### Security Paradigm

You can now find our [security paradigm](/direction/secure/#security-paradigm) on the [Secure Vision](/direction/secure/) page.

Also see our [Secure Team engineering handbook](/handbook/engineering/ops/secure/).

### Statistics and performance data

Traditionally, applications only reveal valuable information about usage and
performance to administrators. However, most GitLab instances only have a handful of
admins and they might not sign in very often. This means interesting data is rarely
seen, even though it can help to motivate teams to learn from other teams,
identify issues or simply make people in the organisation aware of adoption.

To this end, performance data and usage statistics should be available to all users
by default. It's equally important that this can be optionally restricted to admins-only,
as laws in some countries require this, such as Germany.

Not all instance-data is available to all users in GitLab yet.
[This issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/41416) and the epic above should solve this.

### Internationalisation

GitLab is developed in English, but supports the contribution of other
languages.

GitLab will always default to English. We will not infer the language /
location / nationality of the user and change the language based on that.
You can't safely infer user preferences from their system settings either.
Technical users are used to this, usually writing software in English,
even though their language in the office is different.

### Performance

Fast applications are better applications. Everything from the core user experience,
to building integrations and using the API is better if every query is quick, and
every page loads fast. When you're building new features, performance has to be top of mind.

We must strive to make every single page fast. That means it's not acceptable for new
pages to add to performance debt. When they ship, they should be fast.

You must account for all cases, from someone with a single object, to thousands of objects.

Read the handbook page relating to [performance of GitLab.com](/handbook/engineering/performance), and note the Speed Index target shown there
(read it thoroughly if you need a detailed overview of performance). Then:

- Make sure that new pages and interactions meet the Speed Index target.
- Existing pages should never be significantly slowed down by the introduction of new features
or changes.
- Pages that load very slowly (even if only under certain conditions) should be sped up by
prioritizing work on their performance, or changes that would lead to improved page load speeds
(such as pagination, showing less data, etc).
- Any page that takes more than 4 seconds to load (speed index) should be considered too slow.
- Use the [availability & performance priority labels](/handbook/engineering/performance/#performance-labels)
to communicate and prioritize issues relating to performance.

Of course, you must prioritize improvements according to their impact (per the [availability & performance priority labels](/handbook/engineering/performance/#performance-labels)).
Pages that are visited often should be prioritized over pages that rarely have any visitors.
However, if page load time approaches 4 seconds or more, they are considered no longer
usable and should be fixed at the earliest opportunity.

#### Restriction of closed source Javascript

In addition, to meet the [ethical criteria of GNU](https://www.gnu.org/software/repo-criteria-evaluation.html),
all our javascript code on GitLab.com has to be free as in freedom.
Read more about this on [GNU's website](https://www.gnu.org/philosophy/javascript-trap.html).

### Actionable feedback

Deployments should never be fire and forget. GitLab will give you immediate
feedback on every deployment on any scale. This means that GitLab can tell you
whether performance has improved on the application level, but also whether
business metrics have changed.

Concretely, we can split up monitoring and feedback efforts within GitLab in
three distinct areas: execution (cycle analytics), business and system feedback.

#### Business feedback

With the power of monitoring and an integrated approach, we have the ability to
do amazing things within GitLab. GitLab will be able to automatically test
commits and versions through feature flags and A/B testing.

Business feedback exists on different levels:

* Short term: how does a certain change perform? Choose A/B based on data.
* Medium term: did a particular new feature change conversions, engagement
* Long term: how do larger efforts relate to changes in conversations, engagement, revenue

- [A/B Testing of branches](https://gitlab.com/gitlab-org/gitlab-ee/issues/117)

#### Application feedback

Your application should perform well after changes are made. GitLab will be able
to see whether a change is causing errors or performance issues on application
level. Think about:

* Response times of e.g. a backend API
* Error rates and occurrences of new bugs
* Changes in API calls

#### System feedback

We can now go beyond CI and CD. GitLab will able to tell you whether a change
improved performance or stability. Because it will have access to both
historical data on performance and code, it can show you the impact of any
particular change at any time.

System feedback happens over different time windows:

* Immediate: see whether changes influence availability and alert if they do
* Short-medium term: see whether changes influence system metrics and performance
* Medium-Long term: did a particular effort influence system status

- Implemented: [Performance Monitoring](https://docs.gitlab.com/ee/administration/monitoring/performance/introduction.html)
- [Status monitoring and feedback](https://gitlab.com/gitlab-org/gitlab-ce/issues/25555)
- [Feature monitoring](https://gitlab.com/gitlab-org/gitlab-ce/issues/24254)

#### Execution feedback & Cycle Analytics

GitLab is able to speed up cycle time for any project. To provide feedback on
cycle time GitLab will continue to expand cycle analytics so that it not only
shows you what is slow, it’ll help you speed up with concrete, clickable
suggestions.

- [Cycle Speed Suggestions](https://gitlab.com/gitlab-org/gitlab-ce/issues/25281)

### Why cycle time is important

The ability to monitor, visualize and improve upon cycle time (or: time to
value) is fundamental to GitLab's product. A shorter cycle time will allow you
to:

- respond to changing needs faster (i.e. skate to where the puck is going to be)
- ship smaller changes
- manage regressions, rollbacks, bugs better, because you're shipping smaller changes
- make more accurate predictions
- focus on improving customer experience, because you're able to respond to their needs faster

When we're adding new capabilities to GitLab, we tend to focus on things that
will reduce the cycle time for our customers. This is why we choose
[convention over configuration](/handbook/product/#convention-over-configuration)
and why we focus on automating the entire software development lifecycle.

All friction of setting up a new project and building the pipeline of tools you
need to ship any kind of software should disappear when using GitLab.

### Plays well with others

We understand that not everyone will use GitLab for everything all the time,
especially when first adopting GitLab. We want you to use more of GitLab because
you love that part of GitLab. GitLab plays well with others, even when you use
only one part of GitLab it should be a great experience.

GitLab ships with built-in integrations to many popular applications. We aspire to have the world's best integrations for Slack, JIRA, and Jenkins.

Many other applications [integrate with GitLab](/partners/integrate/), and we are open to adding new integrations to our [technology partners page](/partners/). New integrations with GitLab can vary in richness and complexity; from a simple webhook, and all the way to a [Project Service](https://docs.gitlab.com/ee/user/project/integrations/project_services.html).

GitLab [welcomes and supports new integrations](/integrations/) to be created to extend collaborations with other products.
GitLab plays well with others by providing APIs for nearly anything you can do within GitLab.
GitLab can be a [provider of authentication](https://docs.gitlab.com/ee/integration/oauth_provider.html) for external applications.
And of course GitLab is open source so people are very welcome to add anything that they are missing.
If you don't have time to contribute and are a customer we'll gladly work with you to design the API addition or integration you need.

### ML/AI at GitLab

Machine learning (ML) through neural networks is a really great tool to solve hard to define, dynamic problems.
Right now, GitLab doesn't use any machine learning technologies, but we expect to use them [in the near future](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=ML%2FAI)
for several types of problems.

#### Signal / noise separation

Signal detection is very hard in a noisy environment. GitLab plans to use
ML to warn users of any signals that stand out against the background noise in several features:

- security scans, notifying the user of stand-out warnings or changes
- error rates and log output, allowing you to rollback / automatically rollback a change if the network notices aberrant behavior

#### Recommendation engines

Automatically categorizing and labelling is risky. Modern models tend to overfit, e.g. resulting
in issues with too many labels. However, similar models can be used very well in combination
with human interaction in the form of recommendation engines.

- [suggest labels to add to an issue / MR (one click to add)](https://gitlab.com/tromika/gitlab-issues-label-classification)
- suggest a comment based on your behavior
- suggesting approvers for particular code

#### Smart behavior

Because of their great ability to recognize patterns, neural networks are an excellent
tool to help with scaling, and anticipating needs. In GitLab, we can imagine:

- auto scaling applications / CI based on past load performance
- prioritizing parallelized builds based on the content of a change

#### Code quality

Similar to [DeepScan](https://deepscan.io/home/).

#### Code navigation

Similar to [Sourcegraph](https://about.sourcegraph.com/).

----




## Changing the Product handbook

This is the Product handbook. If you (a Product team member, a GitLabber, or anyone
else) see any typos or small copywriting errors here, consider correcting them
with a merge request and merging it yourself (provided you have merge permissions
to this repository), and mentioning a [Product team member](/company/team/) afterward
as a courtesy so that we can thank you, since we have a [bias for action](/handbook/values/#results)
and trust your judgement. If you have a larger change (or don't have merge permissions),
create a merge request and mention any Product team member for further review so
that we can incorporate your change (if it makes sense) as soon as possible. Please
don't assign anybody to the merge request.

[features]: https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/features.md
[CAB]: /handbook/marketing/product-marketing/customer-reference-program/#gitlab-devops-customer-advisory-board

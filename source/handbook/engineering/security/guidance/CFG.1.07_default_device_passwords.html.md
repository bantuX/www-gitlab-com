---
layout: markdown_page
title: "CFG.1.07 - Default Device Passwords Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# CFG.1.07 - Default Device Passwords

## Control Statement

Vendor-supplied default passwords are changed according to GitLab standards prior to device installation on the GitLab network or immediately after software or operating system installation.

## Context

Changing the default password will strengthen the baseline configuration and reduce the ability for the system/device to become compromised.

## Scope

This control applies to all hosted systems (e.g. VM's and GCP compute services) as well as end user workstations (e.g. GitLabbers' MacBooks) and all third-party applications utilized by GitLab.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CFG.1.07_default_device_passwords.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CFG.1.07_default_device_passwords.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/CFG.1.07_default_device_passwords.md).

## Framework Mapping

* PCI
  * 2.1
  * 2.1.1

---
layout: markdown_page
title: "Customer Success Escalations Process"
---

# Technical Account Management Handbook
{:.no_toc}

## On this page
{:.no_toc}

- TOC
{:toc}

- Customer Success Escalations Process *(Current)*
- [Account Triage](/handbook/customer-success/tam/triage)
- [Account Engagement](/handbook/customer-success/tam/engagement)
- [Account Onboarding](/handbook/customer-success/tam/onboarding)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Gemstones](/handbook/customer-success/tam/gemstones)

---

## Objective

Define the process for managing account escalations and define framework for communications, activities and expectations for customer escalations.

## Scope

This addresses critical and high escalations for Enterprise customers. This can also apply to Commercial or SMB customers if there is a strategic partnership or relationship. This can also be leveraged for issues that affect a broad number of customers though the customer engagement model will differ. Any GitLab team member can escalate an account on behalf of the customer.

## Definitions of Severity Levels

| Severity Level | Description | Cadence | Levels of Involvement |
| -------------- | ----------- | ------- | --------------------- |
| Critical | Major issue(s) impacting significantly impacting a customer's ability to deploy and/or use a solution, risking loss of and/or use a solution, risking loss of customer and/or significant risk to the relationship and brand. | Daily | VP of Sales, Product, CRO, CEO, VP of Customer Success |
| High | Major issue(s) impacting significantly impacting a customer's ability to deploy and/or use a solution, risking current adoption, future growth on the account and damage to the relationship. | Multiple times per week  | VP of Sales, Product, CRO, CEO, VP of Customer Success |
| Medium | Issue(s) impacting customer's ability to deploy and/or use the product, risking current adoption and renewal. | Weekly to Bi-weekly | VP of Sales, VP of Customer Success |
| Low | Issue(s) impacting customer's ability to deploy and/or use the product, risking customer value realization, timeline, customer satisfaction and adoption levels. | Standard Communication | Director of Customer Success, Account Manager |

* Cadence refers to the cadence of internal and external meetings and communications to the customer.
* Level of involvement defines scope of internal communication and awareness. Others can be included based on the type of issues involved.

## Responsibilities

* Technical Support is responsible for managing Low and Medium level escalations, leveraging other teams (e.g., Technical Account Managers (TAMs), Engineering) as needed.
* TAMs are responsible for managing High and Critical level escalations, leveraging other teams (e.g., Support, Engineering) as needed.
  * If no TAM is assigned, the escalation will be managed by Technical Support.

## Escalation Tracker Template

* [Link](https://docs.google.com/document/d/1DFW9WDigDZTRQlArqvyaLl_GcYi5lwsxKKKtcjB49s0/edit#) to template (only acessibly by GitLab team members). Save the document for each individual customer and replace (CUSTOMER) with the customer name. 

* Executive Summary
  * This section does not need to be updated daily but should be updated when there are changes to the status of the overall engagement (e.g., top-level issue status, milestones, etc.).
  * Milestones should include key customer and GitLab milestones.

## Related Links

* [Technical Support Page](https://about.gitlab.com/support/)
* Product Escalation Request Process *(TBD)*
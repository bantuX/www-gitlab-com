---
layout: markdown_page
title: "Customer Success"
---
# Welcome to the Customer Success Handbook
{:.no_toc}

The Customer Success department is part of the [GitLab Sales](/handbook/sales) function who partners with our large and strategic customers to deliver value throughout their journey with GitLab.

The team can be reached in [Slack channel](https://gitlab.slack.com/archives/customer-success) (internal only).

## On this page
{:.no_toc}

- TOC
{:toc}

## Mission Statement
To deliver value to all customers by engaging in a consistent, repeatable, scalable way across defined segments so that customers see the value in their investment with GitLab, and we retain and drive growth in our within our enterprise customers.

* The mission of the Customer Success Department is to provide these customers with experience in order to:
    * Accelerate initial customer value
    * Maximize long-term, sustainable customer value
    * Improve overall customer satisfaction & referenceability
    * Maximize the total value of the customer to GitLab


## Vision Page
The ["Customer Success Vision Page"](/handbook/customer-success/vision) provides the long-term view of company wide success to which the customer success will be building in partnership with the value centers of the GitLab organization (i.e., Marketing, Sales, Product/Engineering, Support, Finance and IT).

## Department Initiatives
Our large and strategic customers are in need of an ongoing partnership that combines expert guidance with flexibility and adaptability to support their adoption and continuous improvement initiatives.  

These customers expect that partner to provide a streamlined, consistent and fully coordinated experience that encompasses the full span of their needs as well as the fully lifecycle of the relationship.
Need to focus on 3 main areas in order to grow in our existing accounts as well as land large and strategic:

1. Awareness
2. Adoption
3. Usage

### Initiative: Awareness
Opportunity to improve the overall awareness of GitLab in order to promote and evangelize our brand and solution in a meaningful way to provide big business impact to our customers so that they believe in our vision and strategy.

### Initiative: Adoption
Ensuring paying customers are successful in their onboarding in order to gain adoption and get the most out of our platform and remain happy, paying GitLabbers and brand advocates.

### Initiative: Usage
Collecting and making use of customer data and insights is key to customer success.  It’s important to do more with data and when necessary share back with customers, which in turn helps and encourages our customers to improve and drive adoption.

## Customer Success Groups
### Solutions Architects
* [Solutions Architects handbook](/handbook/customer-success/solutions-architects)

### Implementation Engineers
* [Implementation Engineering handbook](/handbook/customer-success/implementation-engineering)

### Technical Account Managers
* [Technical Account Manager handbook](/handbook/customer-success/tam)

### Account Managers
* [Account Managers handbook](/handbook/customer-success/account-management)

## Transition and Handoff Workflow

In an effort to capture the wider scope of customer interactions with GitLab, the flow outlined below will soon addresses the business processes involved through the entire service lifecycle and customer journey.  There is an active initiative to unify stages, phases, titles, names, etc led by Marketing and Sales.

## Responsibility Matrix and Transitions

The table below depicts the relationships between activities, responsible roles, relationship and opportunity stages in the customer lifecycle.  A more detailed in the [responsibility assignment matrix (RACI)](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix) format will be an critical tool for every GitLab customer.

|  **Sequence** | Activity                                    | Who's Responsible | Business Relationship Stage | Opportunity Stage | Output  |
|  ------: | :------ | :------ | :------ | :------ | :------ |
|  **1** | Create relationship lifecycle meta-record (e.g., SFDC) | Account Executive | Lead or Contact | Discovery | A data record containing information related to the prospect and having data fields attributes that will support the long term ability to "current state" of a customer at any given point in the lifecycle.  This cell links to an example customer "meta-record" |
|  **2** | Qualify the lead in terms of GitLab value to their organization | Account Executive | Lead or Contact | Discovery |  |
|  **3** | Engage solution architects  | Account Executive | Lead or Contact | Discovery |  |
|  **4** | Intro and engage solution architect in technical conversations | Account Executive | Lead or Contact | Discovery |  |
|  **5** | Update relevant SFDC fields with new information | Account Executive | Opportunity | Scoping |  |
|  **6** | Demo GitLab tech and articulate the value proposition | Solutions Architect | Opportunity | Scoping |  |
|  **7** | High-level technical discovery and fit assessment | Solutions Architect | Opportunity | Technical Evaluation | Updates to the customer technical profile that demonstrate the architectural fit is feasible and provides sufficient information for generating a SOW and solution design blueprint. |
|  **8** | Capture environment and technical specifics for each prospect | GitLab Group | Opportunity | Technical Evaluation | Enriched customer meta-record |
|  **9** | Create a solution design blueprint from requirements gathering, tech discovery and customer meta-record | Solution Architect | Opportunity | Technical Evaluation | Blueprint solution design diagram |
|  **10** | Handoff solution design blueprint to Implementation Engineer and TAM | Solutions Architect | Opportunity | Technical Evaluation | IE and TAM full review |
|  **11** | Validate the solution design as generated by Solutions Architect | IE and TAM | Opportunity | Proposal | Approval / signoff by IE and TAM |
|  **12** | Create and submit professional services implementation SOW to stakeholders | Implementation engineer | Opportunity | Proposal | Statement of Work (inclusive of blueprint solution design) |
|  **13** | Create and submit Proof of Value (PoV/PoC) plan to stakeholders  | Solution Architect | Opportunity | Technical Evaluation | PoV plan (inclusive of blueprint solution design) |
|  **14** | Agreed upon success criteria | GitLab Group | Opportunity | Negotiating | A document that clearly articulates what done looks like and when done is successful.  The success criteria are required for proof of value or a professional services implementation.  The document should be signed by an accountable individual from both parties and verbally discussed. |
|  **15** | Create and complete [Customer Success Plan](/handbook/customer-success/tam/#managing-the-customer-engagement) | Solutions Architect | Opportunity | Negotiating |
|  **16** | Close with a technical win, negotiated terms and signed contract | GitLab Group | Opportunity | Closed Won | Contract, MSA, SLA, OLA, et al the other legal bits |
|  **17** | [PoV/PoC project kick-off](/handbook/sales/POC/) | Solutions Architect | Opportunity | Negotiating | Kick-off and proof of value execution plan catered to the customer |
|  **18** | Official introduction and account management transition to TAM to customer stakeholders via [Welcome Call](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template/issues/24) | Account Executive | Opportunity | Technical Evaluation |  |
|  **19** | Professional Services Implementation kick-off | Technical Account Manager | Customer | Closed Won | Kick-off and implementation plan catered to the customer |
|  **20** | Transition customer meta-record and technical profile to IE or TAM | Solutions Architect | Customer | Closed Won |  |
|  **21** | Transition customer meta-record and business profile to TAM | Account Executive | Customer | Closed Won |  |
|  **22** | Full use-case, stakeholder, technology stack, and opportunity discovery | Technical Account Manager | Customer | Closed Won | Update fields in Salesforce, and Customer Collaboration projects |
|  **23** | Manage the ongoing customer meta-record rigorously | Technical Account Manager | Customer | Closed Won |  |

## Customer Success Data Management

Customer names, customer information security and NDA compliance are critical concerns for Customer Success, since we communicate with customers every day. All issues managing work as it relates to prospects and customers must be private and confidential. Work should be managed only in approved GitLab projects and never within private repositories. GitLab employee-only accessible projects for work management include:

* [us-public-sector](https://gitlab.com/gitlab-com/us-public-sector) parent project for all public sector work
* [account-management](https://gitlab.com/gitlab-com/account-management/) parent project for account-specific work and collaboration with the rest of sales
* [customer-success](https://gitlab.com/gitlab-com/customer-success) parent project for Customer Success employee, shared or triage work

## Other Resources

### Using Salesforce within Customer Success

Visit [this page](/handbook/customer-success/using-salesforce-within-customer-success/) for more info on using Salesforce within Customer Success

### Customer Success Metrics

We currently have a [Customer Success Metrics](https://gitlab.com/gitlab-com/customer-success/cs-metrics) Project within GitLab. This project is used to requesting new reports, dashboards and suggesting new metrics that we may need for the department.

This will sometimes be cross-referenced with a data & analytics project.

### Optimized Account Growth Program

In order to evangelize and grow the use of GitLab in existing accounts, the [Optimized Account Growth Program](/handbook/customer-success/optimized-account-growth-program) has been developed to facilitate collaborative sessions and workshops within customer environments. The program is intended to improve the partnership between GitLab and the customer, solving complex business problems while reducing cycle times and improving workflow, ultimately resulting in the growth in adoption and usage of GitLab in customer environments.

### Customer Success resource links outside handbook

* [Solutions Architects Onboarding Bootcamp](/handbook/customer-success/solutions-architects-onboarding-bootcamp/)
* [HealthCheck](https://docs.google.com/document/d/1aHA3W2FsHUApnz2XVtJoyhpcGYy6bgOHoRi4ArXnF0o/edit) (Internal Only)
* [Customer Reference Sheet](https://docs.google.com/a/gitlab.com/spreadsheets/d/1Off9pVkc2krT90TyOEevmr4ZtTEmutMj8dLgCnIbhRs/edit?usp=sharing)
* [Sales Collateral](https://drive.google.com/open?id=0B-ytP5bMib9TaUZQeDRzcE9idVk)
* [GitLab University](https://docs.gitlab.com/ee/university/)
* [Our Support Handbook](/handbook/support/)
* [GitLab Hosted](/gitlab-hosted/)
* [Customer Collaboration Project template](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template)
* [Workflow SA Demo Scenarios](https://docs.google.com/document/d/1kSVUNM4u6KI8M9FxoyiUbHEHAHIi34iiY25NhMxLucc/edit) (Internal Only)

### Other Sales Topics

* [Sales Handbook](/handbook/sales/)
* [Sales Operations](/handbook/business-ops/)
* [Sales Skills Best Practices](/handbook/sales-training/)
* [Sales Discovery Questions](/handbook/sales-qualification-questions/)
* [EE Product Qualification Questions](/handbook/EE-Product-Qualification-Questions/)
* [GitLab Positioning](/handbook/positioning-faq/)
* [FAQ from prospects](/handbook/sales-faq-from-prospects/)
* [Client Use Cases](/handbook/use-cases/)
* [Proof of Concept Guidelines](/handbook/sales/POC/)
* [Account Planning Template for Large/Strategic Accounts](https://docs.google.com/presentation/d/1yQ6W7I30I4gW5Vi-TURIz8ZxnmL88uksCl0u9oyRrew/edit?ts=58b89146#slide=id.g1c9fcf1d5b_0_24))
* [Sales Demo](/handbook/marketing/product-marketing/demo/)
* [Sales Development Group Handbook](/handbook/marketing/marketing-sales-development/sdr/)
* [Who to go to ask Questions or Give Feedback on a GitLab feature](/handbook/product/#who-to-talk-to-for-what)
* [CEO Preferences when speaking with prospects and customers](/handbook/people-operations/ceo-preferences/#sales-meetings)

### Customer Success & Market Segmentation
For definitions of the account size, refer to [market segmentation](/handbook/business-ops/#segmentation).

- Large: Sr. Account Executive closed deal and stays on account for growth and renewal. Solutions Architect is assigned pre-sales and stays on the account to help with adoption and growth.
- Mid-Market: Account Executive closes deal and transfers to Account Manager to handle renewals and growth. The Account Manager records the Previous Account Owner on the Opportunity Group in any deals while the account is in the New Customer period within the first 12 months including the first annual renewal.
- SMB: Web Portal with Sales Admin oversight.

---
layout: markdown_page
title: "SafeGuard"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc}

- TOC
{:toc}

----

The following benefits are provided by [SafeGuard](https://www.safeguardglobal.com/) and apply to team members who are contracted through SafeGuard. If there are any questions, these should be directed to People Operations at GitLab who will then contact the appropriate individual at SafeGuard.

## Ireland

- Currently SafeGuard do not provide private healthcare
- SafeGuard do provide a pension via Zurich, if individuals would like to join this scheme, a leaflet can be found by clicking on this [link](https://drive.google.com/file/d/1GRasMwjchtKSw4ZkPJNJsiCjAU-MOsNH/view?usp=sharing). Please note that at this time there are no employer contributions.


## Spain
**We are currently unable to hire any more employees or contractors in Spain.  See: https://about.gitlab.com/jobs/faq/#country-hiring-guidelines

- Currently SafeGuard does not provide private healthcare
- Accruals for 13th and 14th month salaries
- General risks and unemployment insurance
- Salary guarantee fund (FOGASA)
- Work accident insurance 

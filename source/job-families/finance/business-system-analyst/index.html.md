---
layout: job_family_page
title: "Business System Analyst"
---

## Business Systems Specialist - Product Transactions (Junior)

### Responsibilities

* Handle all incoming transactions and licensing requests from users and internal GitLab team members
* Identify and document new processes and methods of reducing incoming requests which help both internal and external users
* Triage / troubleshoot issues and find workarounds where possible
* Submit and contribute to bug reports and feature requests/improvements
* Work closely with the Fulfillment group to escalate and prioritize issues
* Work with the surrounding GitLab teams to help solve problems at scale
* Contribute to the appropriate FAQ’s, documentation, and handbook entries and work with GitLab’s technical writers to ensure consistency.
* Maintain good ticket performance and satisfaction
* Meet or exceed agreed SLA times consistently for requests

### Requirements

* 3-5 years experience in an administrative role, preferably senior
* Excellent written and spoken communication
* The ability to absorb abstract and complex problems and then communicate these problems clearly and concisely to others
* Patience, kindness and empathy
* Ability to work competently with a variety of different groups across GitLab
* A proactive self-starter who can think strategically whilst thriving in a reactive environment
* Highly organized and methodical with true attention to detail
* A passion for helping others and problem solving

### Nice to have

* Some experience with Rails applications
* Basic understanding of software development and programming
* Experience working in a support-desk style environment
* Experience with Zuora, Salesforce and similar CRM/billing and subscription tools
* A love of open source
* Experience with SaaS products
* Experience using GitLab/Git

## Business System Analyst

### Responsibilities

* Develop business requirements, write the business case, define business logic, create optimization and monitoring strategies and work with the implementation teams to communicate and develop solutions
* Work with cross-functional business teams to facilitate alignment around project issues and define best practice business processes
* Assist with Business Process Mapping and gap analysis, project scoping, planning, and scheduling
* Be a communication focal point providing coordination for IT/Business Systems projects
* Understand system capabilities and business requirements, and drive standard & scalable solutions. Anticipate risks and mitigating them before they become serious.
* Identify opportunities to increase efficiency and productivity within the context of the overall business strategy
* Document work using the GitLab’s standards, methods and tools
* Partner with ICs and Operations/Managers to establish timelines and ensure delivery of projects and requests

### Requirements

* 1-3 (or more) years of experience in software project management
* Multiple successful business system projects and experience with business analysis and business process improvement, preferably in a high-growth enterprise SaaS environment
* Good analytical skills, understanding of project lifecycles, and the ability to act in an entrepreneurial manner to ensure the software delivered matches the business communities' needs
* Strong written, verbal, and listening communication skills: an articulate and effective communicator able to describe complex problems and solutions in understandable terms; ability to frame communications to a diverse set of stakeholders.
* Experience with supporting multiple implementation work-streams simultaneously
* Strong collaboration skills while working with SME's, senior leaders, and business users/ stakeholders to drive business analysis/user requirements,
* Strong experience in negotiating scope and priorities and balancing for successful outcomes
* Basic understanding of database and integration approaches. Must be able to write basic - medium complexity SQL queries

### Additional Senior Requirements

* Proven track record of improving operational efficiency
* 10 years experince, demonstrating consistent advancement or development, with at least half in related roles
* Over three years in a high-growth technology company, working hands on with our tech stack at our stage in operational maturity
* Ability and desire to grow and coach less tenured team-members

### Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Director of Business Operations
* Next, candidates will be invited to schedule a second interview with our VP of Field Operations or Director of Sales Operations
* Candidates will then be invited to schedule a third interview with our Marketing Operations Lead
* Candidates will be then be invited to schedule a call with our FP&A Lead
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).

---
layout: job_family_page
title: "Accounts Payable Specialist"
---
GitLab is seeking an Accounts Payable Specialist who is high energy and can easily adapt to a fast paced growing department.  This is a full time remote position so we need someone who is an experienced proven self starter!


## Responsibilities

- Process full-cycle accounts payable including vendor and invoice management, approval and weekly disbursement activities
- Process expense reports, credit card account reconciliations, and payable side bank reconciliation
- Help ensure proper GL coding for all AP activity recorded in accordance with the GitLab chart of accounts
- Administrator for Expensify expense reporting tool including the review of all corporate card transactions to ensure proper coding and processing in NetSuite
- Assist in year-end 1099 filings
- Assist with implementation of accounting tools to help automate and streamline
- Assist team with ad hoc projects, as needed
- Assist with audit requests related to the accounts payable function
- Ensure internal controls are followed and maintained as related to the accounts payable function
- Fulfill all duties, including journal entries such as AP accruals/reclasses, along with reconciliations, and other assigned tasks related to accounts payable, in a timely manner to comply with the close calendar, - - checklists, and other due dates as assigned


## Requirements

- Must have experience with Netsuite
- Experience with Expensify is a plus
- 5+ years of related accounts payable experience is required
- Flexible to meet changing priorities and the ability to prioritize workload to achieve on time accurate results
- Knowledge of SOX is a plus
- Proficient with Excel and Google Sheets
- International experience is a plus
- Self-starter with the ability to work remotely and independently and interact with various teams when needed.
- You share our [values](/handbook/values), and work in accordance with those values.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Controller
- Candidates will then be invited to schedule a 45 minute interview with our CFO
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing).

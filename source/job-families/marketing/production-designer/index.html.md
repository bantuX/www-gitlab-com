---
layout: job_family_page
title: "Production Designer"
---

## Production Designer

At GitLab, Production Designers collaborate closely with Brand Designers, and other areas of marketing, to produce on-brand assets across various print, web, and digital mediums.

### Responsibilities

- Extend and translate established creative concepts into production-ready print, web, and digital assets.
- Work collaboratively with Brand Designers to create a consistent and cohesive brand experience.
- Work efficiently on multiple projects with consistency and strong attention to detail.
- Embrace an iterative design process and be receptive to feedback throughout the process.
- Contribute to the [Pajamas Design System](https://design.gitlab.com/).
- Able to work with, and help evolve, the GitLab brand.
- Work includes email templates, landing pages, social media, paid digital advertising, slide decks, digital and print whitepapers, one-pagers, etc., trade show booth design, and swag.

### Requirements

- Able to generate pixel-perfect production assets to meet required specifications for print, web, and digital mediums.
- Proven experience with print, web, and digital design.
- Degree in graphic design or a related field.
- Strong portfolio showcasing brand and marketing experience across print, web, and digital mediums.
- You have an eye for strong typography, illustration, icon design, and overall user experience.
- You know Sketch, Adobe Illustrator, Photoshop, InDesign, Google Slides, and Powerpoint like nobody’s business.
- Comfortable with Git, HTML, CSS, and Markdown to update [about.gitlab.com](https://about.gitlab.com/).
- You share our [values](/handbook/values), and work in accordance with those values.

## Levels

### Junior Production Designer

- Quickly and iteratively translate established creative concepts into production-ready print, web, and digital assets.
- Ability to work within GitLab's brand guidelines while producing creative and thoughtful visual experiences.
- Understanding of how typography, illustration, icons, etc. work together to create a meaningful brand experience.

#### Requirements

- 1-3 years experience in a design-related role.
- Ability to continuously meet deadlines and execute on projects.

### Intermediate Production Designer

- Work quickly and iteratively on creative projects with limited direction from Brand Designers.
- Proven experience with illustration and icon design as an extension of a brand.
- Ability to implement your work on [about.gitlab.com](https://about.gitlab.com/) through basic HTML, CSS, and Markdown.

#### Requirements

- 3-5 years experience in a design-related role.
- Self-manage workload to meet deadlines and prioritize accordingly.

### Senior Production Designer

- Work independently, drive multiple projects, prioritize accordingly, and iterate quickly.
- Ability to evolve and extend creative concepts with extreme attention to detail.
- Influence creative strategy and have a strong understanding of our audience(s).

#### Requirements

- 5+ years experience in a design-related role.
- Proven ability to lead creative projects from concept to completion.

### Manager, Production Design

- Own all aspects of production design; providing feedback and oversight of creative deliverables, keeping projects on brand and on time, and prioritizing work based on impact to the company.
- Set the production design workflow, directives, and goals.
- Proven understanding of our audience(s), creative strategy, and brand guidelines.
- Improve and enforce GitLab's brand guidelines.
- Influence GitLab's brand experience.

#### Requirements

- 5+ years experience in print, web, and digital design.
- Excellent communication, organizational, and leadership capabilities.
- Proven experience as a managing production designer or similar role, preferably within marketing.

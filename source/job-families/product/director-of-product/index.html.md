---
layout: job_family_page
title: "Director of Product"
---

As the Director of Product, you will be responsible for managing and building
the team that focuses on a subset of GitLab's [product categories](/handbook/product/categories/#devops-stages).

## Individual responsibility

- Make sure you have a great product team (recruit and hire, sense of progress, promote proactively, identify underperformance)
- Work on the vision with the Head of Product, VP of Product, and CEO; and communicate this vision internally and externally
- Distill the overall vision into a compelling roadmap
- Make sure the vision advances in every release and communicate this
- Communicate our vision though demo's, conference speaking, blogging, and interviews
- Work closely with Product Marketing, Sales, Engineering, etc.

## Team responsibility

- Ensure that the next milestone contains the most relevant items to customers, users, and us
- Work with customers, users, and other teams to make feature proposals enticing, actionable, and small
- Make sure the [DevOps tools](/devops-tools/) are up to date
- Keep [/direction](/direction) up to date as our high level roadmap
- Regularly join customer and partner visits that can lead to new features
- Ensure that we translate user demands to features that make them happy but keep the product UI clean and the codebase maintainable
- Make sure the release announcements are attractive and cover everything
- Be present on social media (hacker news, twitter, stack overflow, mailinglist), especially around releases

## Requirements

* 3-5 years experience in managing product managers
* 8-10 years of experience in product management
* Experience in DevOps
* Technical background or clear understanding of developer products; familiarity with Git, Continuous Integration, Containers, Kubernetes, and Project Management software a plus
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values), and work in accordance with those values
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

## Specialties

### Dev

The Director of Product, Dev leads the Dev parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#dev-sub-department)
(e.g manage, plan, and create) and reports to the Head of Product.

### CI/CD

The Director of Product, CI/CD leads the CI/CD parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#cicd-sub-department)
(e.g verify, package, and release) and reports to the Head of Product.

### Ops

The Director of Product, Ops leads the Ops parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#ops-sub-department)
(e.g. Monitor and Configure) and reports to the Head of Product.

### Secure

The Director of Product, Secure leads the Secure parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#secure-sub-department)
and reports to the Head of Product.

### Defend

The Director of Product, Defend leads the Defend parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#defend-sub-department)
and reports to the Head of Product.

### Growth

The Director of Product, Growth leads the Growth parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#growth-sub-department)
and reports to the Head of Product.

### Enablement

The Director of Product, Enablement leads the Enablement parts of the
[DevOps lifecycle](https://about.gitlab.com/handbook/product/categories/#enablement-sub-department)
and reports to the Head of Product.

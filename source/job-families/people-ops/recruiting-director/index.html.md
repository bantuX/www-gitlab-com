---
layout: job_family_page
title: "Recruiting Director"
---

## Senior Director 

## Responsibilities:

* Responsible for the overall strategy of the Recruiting team 
* Partners with the E-Group to determine strategic landscape for our geographical dispersion 
* Partners with the E-Group to align on overall plan for talent acquisition to ensure we maintain the high calibre of team members 
* Partner with Finance and Department Leaders to deliver data-driven workforce planning processes to ensure the right people are in the right roles with a clear view of talent supply and demand
* Lead selection/management of vendor relationships globally. Negotiate costs to stay within budget and maximize ROI
* Champion the diversity efforts within GitLab to drive a diverse talent pool for our open roles 
* Provide leadership to the Global Recruiting team by creating a culture of accountability with a focus on delivering measurable results 
* Continuous partnership with the CPO on the structure and capabilities required of the Recruiting team 
* Hands-on Leadership of executive-level searches 
* Drive an engaging team culture based on GItLab's core values
* Consistently evaluate and evolve team structure to support growing business needs
* Responsible for accurate and transparent hiring plans that are publically accessible in real time 

## About you:
* Proven experience in high growth environment 
* Successfully driven globally distributed hiring  
* Excellent people management skills, enabling the Recruiting team to reach their full potential 
* Led a high performing Recruiting team 


## Director

## Responsibilities:

* Provide strategic input on the direction of the recruiting team to the Senior Director
* Develops new methods to find passive talent and talent from low cost areas across the world.
* Partner with the leaders of the organization to ensure that they are planning for the right hires and are invested in the process of finding, evaluating, and onboarding their talent.
* Identify and Operationalize the technology needs of the recruiting team - identifying needs, determining solutions and integrating tools and systems
* Partner with internal and external teams to deep dive on trends, opportunities and build projects to continuously improve the effectiveness of finding the best talent for GitLab
* Actively research and source for high priority, executive positions across the organization.
* Evangelize a leading candidate experience from the initial recruiter interview to candidate onboarding
* Manage the recruitment team workload, evaluate and assign priorities to internal recruiting team emphasizing ownership and accountability
* Provide insight to optimize processes and technology
* Build out GitLab talent programs including diversity recruiting, college/campus recruiting, internships and apprenticeships.
* Utilize Greenhouse ATS to define recruiting metrics and using data to implement improvements for talent acquisition
* Cultivate strong partnerships with hiring teams and executives, and influence change throughout the entire hiring process
* Training for hiring managers and team members on GitLab hiring practices. Also influence those hire practices as they may need to be iterated on to drive the best, scalable results for GitLab.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)

## About you:

* Strong attention to detail and ability to work well with changing information
* Comfortable using Technology
* Effective and concise verbal and written communication skills with the ability to collaborate with cross functional team members
* A team player who acts with a sense of urgency and adapts to a fast-paced and ever-changing environment
* Resourceful and takes initiative to seek internal and external resources when problem solving
* Have implemented various HRIS/ATS systems or recruiting tools while delivering internal OKRs
* 5-10+ years recruiting experience with a minimum of 2 years managing a recruiting team.
* Experience hiring Global Talent in areas like Europe, India and China

## Hiring Process:
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates will be invited to schedule a 30 minute [screening call](handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters
* Next, candidates will be invited to schedule a 45 minute interview with our Chief People Officer
* After that, candidates will be invited to schedule a 30 minute interview with at least one of our People Operations Team members
* Candidate then may be invited to two different 45 minute interviews with two different hiring managers
* Finally, our CEO may choose to conduct a final interview.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

---
layout: markdown_page
title: "Category Vision - Design Management"
---

- TOC
{:toc}

## Design Management

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

We want to treat UX Designers as first-class users within GitLab and support their workflows as good as any product can. Design management will consider the design life cycle from generating ideas, design reviews, design systems, and more.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=design%20management)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=design%20managment)
- [Overall Vision](/direction/create/)

Please reach out to PM James Ramsay ([E-Mail](mailto:jramsay@gitlab.com)
[Twitter](https://twitter.com/jamesramsay)) if you'd like to provide feedback or ask
questions about what's coming.

## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

Design Management is targeted at [product designers](/handbook/marketing/product-marketing/roles-personas/#presley-product-designer) who collaborate with [product managers](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager) and [software developers](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) developers.

The **minimal** user journey will provide designers with the ability to upload mockups to an issue, and for point of interest discussions to happen on each image. Over time these mockups can be updated to resolve the discussions. As the mockups are changed, new versions will be created so that process can be captured and reviewed.

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**In progress (ETA 12.0): [Versioned designs and point of interest discussions](https://gitlab.com/groups/gitlab-org/-/epics/660)** - collaboration between designers, developers and product managers on issues is hard and unstructured. This first iteration will make it possible to upload discuss designs far more efficiently. Point of interest comments are the key feature, that allows much precise discussion rather than trying to verbally discuss multiple items of feedback on a single image in a single thread.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Within the Design Tools market, each product broadly solves one or more of these problems:

- Design and prototype
- Version control
- Team collaboration and communication
- UX Research and UI Testing

The most full featured Design Tools, that are attempting to solve all these problems are:

- [Invision Studio](https://www.invisionapp.com/studio)
- [Figma](https://figma.com)
- [UX Pin](https://www.uxpin.com/)

Given GitLab's unique strength as the single source of truth for planning and source code, we are well positioned to work from the merge request backwards into design workflows:

- Review and collaboration, bringing designers deeper into the tool that they already use to understand what needs to be designed and built, and integrating the design workflow into the planning and development workflow.
- Version control for source code is a core competency of GitLab, with great support for LFS and upcoming native support for large files in Git. Building on this to support versioning of designs and automation with CI is a natural fit.

See [Competitor analysis](https://docs.google.com/document/d/12o6h6Fm7bAjhW5AK1r-PNhvn0QrQwZncorYNia12e3Q/edit)

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

Before entering this market, a [competitor analysis](https://docs.google.com/document/d/12o6h6Fm7bAjhW5AK1r-PNhvn0QrQwZncorYNia12e3Q/edit#).

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

The total market potential is over US$ 4 billion and growing. With no clear winners in the design tool space, there is a significant opportunity for an application that can successfully engage developers and design teams in the DevOps lifecycle.

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

Analysts don't appear to be deeply engaged with this category yet. A recent inquiry with Gartner provided positive reinforcement for our current direction.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

No customer issues yet.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

No user issues yet, because no visible feature is yet available.

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/product/#dogfood-everything)
the product.-->

- [Versioned designs and point of interest discussions](https://gitlab.com/groups/gitlab-org/-/epics/660) - the MVC addresses the most frequent frustrations with the current workflow which involves uploading images in a markdown table.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Design process automation](https://gitlab.com/groups/gitlab-org/-/epics/991) - version control the source files in a git repo, and push to update the designs attached to issues.
- [Design reviews and approvals](https://gitlab.com/groups/gitlab-org/-/epics/990) - like merge request, designs need reviews and approvals before being approved to proceed.

---
layout: markdown_page
title: "Category Vision - Feature Flags"
---

- TOC
{:toc}

## Feature Flags

Feature flags unlock faster, more agile delivery workflows by providing
native support for feature flags directly into your development and
delivery process.

Feature Flags is built with an [Unleash](https://github.com/Unleash/unleash)-compatible
 API, ensuring interoperability with any other compatible tooling,
 and taking advantage of the various client libraries available for
 Unleash.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=feature%20flags&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1295) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Feature Flags is a very new feature. Our best first large customer
is ourselves, so working with our Delivery team to enable the
production use of our own feature flags platform is our first target.
The first and foremost requirement shared by that team is to implement
permissions [gitlab-ee#8293](https://gitlab.com/gitlab-org/gitlab-ee/issues/8239).

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [% rollout for Feature Flags](https://gitlab.com/gitlab-org/gitlab-ee/issues/8240)
- [Permissions for Feature Flags](https://gitlab.com/gitlab-org/gitlab-ee/issues/8239)
- [UserID-based access](https://gitlab.com/gitlab-org/gitlab-ee/issues/11459)
- [Cookie-based access](https://gitlab.com/gitlab-org/gitlab-ee/issues/11456)
- [API/CDN caching for feature flags](https://gitlab.com/gitlab-org/gitlab-ee/issues/9479)
- [More explicit logging for accesses](https://gitlab.com/gitlab-org/gitlab-ee/issues/9157)

## Competitive Landscape

Other feature flag products offer more comprehensive targeting and
configuration. The simplicity of our solution is actually a strength
compared to this in some cases, but there is some basic functionality
still to add. The most glaring gap for anyone wanting to use our
feature flags capability is surely that our product currently lacks
the ability to implement % rollout, a very common use case for feature
flags beyond toggling on/off. We plan to implement this feature via
via [gitlab-ee#8240](https://gitlab.com/gitlab-org/gitlab-ee/issues/8240).

There is a detailed LaunchDarkly comparison from when the project
was first being conceived [here](https://docs.google.com/spreadsheets/d/1p3QhVvdL7-RCD2pd8mm5a5q38D958a-vwPPWnBT4pmE/edit#gid=0).

## Analyst Landscape

Analysts are recognizing that this sort of capability is becoming
more a part of what's fundamentally needed for a continous delivery
platform, in order to minimize blast radius from changes. Often,
solutions in this space are complex and hard to get up and running
with, and they are not typically bundled or well integrated with CD
solutions. It's also unclear how to get started.

This backs up our desire to not overcomplicated the solution space
here, and highlights the need for guidance. [gitlab-ee#9450](https://gitlab.com/gitlab-org/gitlab-ee/issues/9450)
introduces new in-product documentation to help development and
operations teams learn how to successfully adopt feature flags.

## Top Customer Success/Sales Issue(s)

None yet, but feedback is welcome.

## Top Customer Issue(s)

None yet, but feedback is welcome.

## Top Internal Customer Issue(s)

Being able to control permissions [gitlab-ee#8239](https://gitlab.com/gitlab-org/gitlab-ee/issues/8239)
on a per-environment basis for feature flags is a key driver of adoption
for our own production usage.

### Delivery Team

- [framework#64](https://gitlab.com/gitlab-org/release/framework/issues/64) enables dashboard annotations for feature flags
- Feature Flags: [framework#216](https://gitlab.com/gitlab-org/release/framework/issues/216) and [framework#32](https://gitlab.com/gitlab-org/release/framework/issues/32)

## Top Vision Item(s)

Our top vision item is to help people get started with feature flags.
See the analyst section above for additional details.

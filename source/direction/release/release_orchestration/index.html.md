---
layout: markdown_page
title: "Category Vision - Release Orchestration"
---

- TOC
{:toc}

## Release Orchestration

Release Orchestration is the ability to coordinate complex releases, particularly
across projects, in an efficient way that leverages as-code behaviors as much as
possible, but also recognizes that there are manual steps and coordination points
involving human decisions throughout software delivery in the enterprise. More
specifically, this is managing the kinds of enterprise releases for which you'd
have a Release Manager in play, rather than having individual teams continually
deploying independent code to production. 

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=release%20orchestration&sort=milestone)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1298) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### How we plan to solve these challenges

These kinds of releases can still be automated to a large extent, especially
at the per-project level, but there is a human aspect in play that's very
important in terms of keeping the release coordinated. The release manager
role in these scenarios is typically responsible for:

- Choosing a release date and collecting the participants; these would
  typically correspond to teams who may or may not have something to
  release on an upcoming release date.
- Tracking the status of each of those pipelines as they move through
  the path to production: starting with scattered development environments,
  moving to an integration environment where everything is validated to work
  together, a stage environment where the deployment plan is tested, and
  finally to production itself.
- Release assurance capabilities such as deployment risk analytics,
  vulnerability scanning, impact analysis, post-deployment monitoring
  using ML or other advanced analytics to help provide a release risk
  score and increase release success likelihood.
- Generating combined release notes containing what will be in the release.

By providing more powerful tooling, we're able to make individual release
managers more effective in their role of orchestrating the releases that are
moving through the software development organization. This is in contrast to
our [Continuous Delivery](/direction/release/continuous_delivery/) category
vision, which is related but more about the automation that makes automatic,
continuous deployment to production possible.

For an example of how this is done in GitLab today, you can see how we managed
the 11.4 release at [release#462](https://gitlab.com/gitlab-org/release/tasks/issues/462)
and [release#460](https://gitlab.com/gitlab-org/release/tasks/issues/460). This
approach works, but is limited to manual checkboxes to orchestrate the process.
This can be significantly improved in several areas, making this process far
easier for ourselves and for our users.

### Enabling vs. Limiting Continuous Delivery

Release Orchestration is very much related to Continuous Delivery. To separate
them here at GitLab, our Release Orchestration category focuses on the human
management of releases, where it is required. This consists of two main points:

- The visualization and management of releases as an entity of their own,
  separate from individual CI/CD pipelines
- Facilitation of the human steps involved in software delivery, in as
  automated a way as possible

We believe Release Orchestration is not a framework for placing limitations
on CD teams, but instead a framework for enabling them when operating under
more controlled environments, such as large enterprises or those with other
regulatory requirements. For these kinds of organizations, even in cases
where all development teams are actually doing full CD, there will still be
a 'release' where at least feature flags are turned on, press releases happen,
and so on.

In order to ensure we strike this balance and don't veer off into the domain
of putting the brakes on CD, we will be building Release Orchestration
capabilities by growing our existing features rather than going completely
off on new tangents. This ensures automation remains the heart of what we do,
and is also the right way to learn the most about how to build the right
orthogonal solutions, when we eventually get there.

### Legacy vs. Modern Workflows

As a company, our priority is to solve problems for the future. We want
GitLab to be a solution that brings you to where you want to take your
engineering and software delivery processes, rather than propping up
inefficient processes with just enough automation to make them bearable.
That's why we're starting with solving release orchestration problems from
a modern, cloud-native perspective.

That said, we do keep legacy flows in mind as we build, and we're always
looking at how we can adapt these cloud-native workflows to also support
delivery of more traditional software. What we are being incredibly careful
not to do, though, is design software to support legacy problems and adapt
those to cloud-native workflows. This is the place that many of our
competitors in the space are in, and they struggle due to that challenge.

### Governance in the Release Pipeline

Security, compliance, control and governance of the release pipeline is handled
as part of [Release Governance](/about/direction/release_governance).

## What's Next & Why

Up next we'll be adding an environments dashboard [gitlab-ee#3713](https://gitlab.com/gitlab-org/gitlab-ee/issues/3713),
giving complete visibility over what's deployed to the environments for
your project.

## Maturity Plan

This category is currently at the "Minimal" maturity level, and
our next maturity target is Viable (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [Cross-project environment dashboard](https://gitlab.com/gitlab-org/gitlab-ee/issues/3713)
- [Executable runbooks for Releases MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/9427)

## Competitive Landscape

Release orchestration tools tend to have great features for managing releases,
as you'd expect; they are built from the ground up as a release management
workflow tool and are very capable in those areas. Our approach to release
orchestration will be a bit different, instead of being workflow-oriented we
are going to approach release orchestration from a publishing point of view.
What this means is instead of building complicated workflows for your releases,
we will focus more on the artifact of the release itself and embedding the
checks and steps into it.

An important view for the way we look at the world is [gitlab-ee#3713](https://gitlab.com/gitlab-org/gitlab-ee/issues/3713)
which introduces an environments-based view for managing what's deployed.

## Analyst Landscape

Analysts at this time are looking for more quality of life features that make
a difference in people's daily workflows - how does this make my day better?
By introducing features like [gitlab-ce#56024](https://gitlab.com/gitlab-org/gitlab-ce/issues/56024)
to automatically manage release notes as part of releases, we can demonstrate
how our solution is already capable of doing this.

## Top Customer Success/Sales Issue(s)

In terms of sales, a release orchestration dashboard that provides a single
view into upcoming releases will be the most impactful: [gitlab-ee#3277](https://gitlab.com/gitlab-org/gitlab-ee/issues/3277).
This will provide our sales teams with a single, clear view that can easily
tell the story about how we solve their release orchestration problems.

## Top Customer Issue(s)

This is prospective given the feature is new, but customers typically look
for a bit more polish in features like this than the current MVC version
provides. Implementing [gitlab-ce#65023](https://gitlab.com/gitlab-org/gitlab-ce/issues/56023),
which makes creation of the release package an inline part of the
`.gitlab-ci.yml`, will make this feature feel much more mature and
production-ready (even if it is already really usable.)

## Top Internal Customer Issue(s)

Alot of interest has been expressed in [gitlab-ee#9427](https://gitlab.com/gitlab-org/gitlab-ee/issues/9427),
the top vision item below, as a way to improve our own release process.

## Top Vision Item(s)

An exciting item for the vision is the ability to create releases as
runbooks via [gitlab-ee#9427](https://gitlab.com/gitlab-org/gitlab-ee/issues/9427).
This will allow non-technical users to create runnable release plans in GitLab,
which can have actions embedded in them which will perform automated parts
of the release.

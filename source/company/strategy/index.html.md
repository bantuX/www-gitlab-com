---
layout: markdown_page
title: "GitLab Strategy"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Why

We think that it is logical that our collaboration tools are a collaborative work themselves. More than [2,000 people](http://contributors.gitlab.com/) have contributed to GitLab to make that a reality. We believe in a world where **everyone can contribute**. Allowing everyone to make a proposal is the core of what a DVCS ([Distributed Version Control System](https://en.wikipedia.org/wiki/Distributed_version_control)) such as Git enables. No invite needed: if you can see it, you can contribute.

In summary, our vision is as follows:

We believe that all digital products should be open to contributions, from legal documents to movie scripts and from websites to chip designs. GitLab Inc. develops great open source software to enable people to collaborate in this way. GitLab is a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/#convention-over-configuration) that everyone should be able to afford and adapt. With GitLab, **everyone can contribute**.

## Mission

Change all creative work from read-only to read-write so that **everyone can contribute**.

When **everyone can contribute** consumers become contributors and we greatly increase the rate of human progress.

Our mission guides our path, during this path live our [values](/handbook/values/).

## How

Everyone can contribute to digital products with GitLab, to GitLab itself, and to our organization.

1. To ensure that **everyone can contribute with GitLab** we allow anyone to create a proposal, at any time, without setup, and with confidence.


    - Anyone: Every person in the world should be able to afford great DevOps software. GitLab.com has free private repos and CI runners and GitLab CE is [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). But open source is more than a license, that is why we are [a good steward of GitLab CE](/company/stewardship/) and keep both GitLab CE and EE open to inspection, modifications, enhancements, and suggestions.
    - Create: It is a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/#convention-over-configuration).
    - Proposal: with Git, if you can read it, you can fork it to create a proposal.
    - At any time: you can work concurrently to other people, without having to wait for permission or approval from others.
    - Without setup: you can make something without installing or configuring for hours with our web IDE, auto build.
    - With confidence: reduce the risk of a flawed proposal with review apps, integrated CI, automatic code quality, security scans, performance testing, and automatic monitoring.


1. To ensure that **everyone can contribute to GitLab the application** we actively welcome contributors. We do this by having quality code, tests, documentation, using popular frameworks, offering a comprehensive [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit), and a dedicated [GitLab Design System](https://design.gitlab.com/). We use GitLab at GitLab Inc., we dogfood it and make it a tool we continue to love. We celebrate contributions by recognizing a Most Valuable Person (MVP) every month. We allow everyone to anticipate, propose, discuss, and contribute features by having everything on a public issue tracker. We ship a new version every month so contributions and feedback are visible fast. To contribute to open source software people must be empowered to learn programming. That is why we sponsor initiatives such as Rails Girls.  
  
   There are a few significant, but often overlooked, nuances of the **everyone can contribute to GitLab the application** mantra:  
   * While collaboration is a core value of GitLab, over collaborating tends to involve team members unnecessarily, leading to consensus-based decision making, and ultimately slowing the pace of improvement in the GitLab application. Consider [doing it yourself](/handbook/values/#collaboration), creating a merge request, and facilitating a discussion on the solution.
   * For valuable features in line with our product philosophy, that do not yet exist within the application, don't worry about UX having a world class design before shipping. While we must be good stewards of maintaining a quality product, we also believe in rapid iteration to add polish and depth after an [MVC](https://about.gitlab.com/handbook/product/#the-minimally-viable-change) is created.
   * Prefer creating merge requests ahead of issues in order to suggest a tangible change to facilitate collaboration, driving conversation to the recommended implementation.  
   * Contributors should feel free to create what they need in GitLab. If quality engineering requires charting features, for example, which would normally be implemented out of another team, they should feel empowered to prioritize their own time to focus on this aspect of the application.
   * GitLab maintainers, developers, and Product Managers should be viewed as coaches for contributions, independent of source. While there are contributions that may not get merged as-is (such as copy/paste of EE code into the CE code base or features that disagree with product philosophy), the goal is to coach contributors to contribute in ways that are cohesive to the rest of the application.

   A group discussion reiterating the importance of everyone being able to contribute:
   <figure class="video_container">
   <iframe src="https://www.youtube.com/embed/l374J98iOmk?t=675" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
   </figure>



1. To ensure that **everyone can contribute to GitLab the company** we have open business processes that allow all team members to suggest improvements to our handbook. We hire remotely so everyone with an internet connection can come work for us and be judged on results, not presence in an office. We offer equal opportunity for every nationality. We are agnostic to location and create more equality of opportunity in the world. We engage on Hacker News, Twitter, and our blog post comments. And we strive to take decisions guided by [our values](/handbook/values).

## Goals

1. Ensure that **everyone can contribute** in the 3 ways outlined above.

2. Become most used software for the software development lifecycle and collaboration on all digital content by following [the sequence below](#sequence).

3. Complete our product vision of a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/#convention-over-configuration).

4. Offer a sense of progress [in a supportive environment with smart colleagues](http://pandodaily.com/2012/08/10/dear-startup-genius-choosing-co-founders-burning-out-employees-and-lean-vs-fat-startups/).

5. Stay independent so we can preserve our values. Since we took external investment we need a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event). To stay independent we want that to become a public company instead of being acquired.

## Sequence

We want to achieve our goals in the following order:

1. In CY2015 we [became the most popular on-premises software development lifecycle solution](https://about.gitlab.com/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market), and we want to continue that.

2. We want to become the most revenue generating on-premises software development lifecycle solution before going public.

3. Around going public we want to become the most popular SaaS solution for private repositories (a [complete product](/direction/#scope) that is [free forever](/gitlab-com/#why-gitlab-com-will-be-free-forever) is competitive since network effects are smaller for private repositories than for public ones).

4. After going public we want to become the most popular SaaS solution for public repositories. This market has a [strong network effect](https://en.wikipedia.org/wiki/Network_effect) since more people will participate if you host your public project on a site with more people. It is easier to overcome this network effect if many people already use GitLab.com for hosting private repositories. Having people on our SaaS helps drive awareness and familiarity with GitLab.

5. Our [BHAG](https://en.wikipedia.org/wiki/Big_Hairy_Audacious_Goal) is to become the most popular collaboration tool for knowledge workers in any industry. For this, we need to make the git workflow much more user friendly. The great thing is that sites like [Penflip](https://www.penflip.com/) are already building on GitLab to make it.

We want to **go public in CY2020**, specifically on Wednesday November 18, 2020 is five years after the first people got stock options with 4 years of vesting. To go public, we need more than $100 million in Annual Recurring Revenue (ARR). To achieve that we want to double [Incremental Annual Contract Value (IACV)](/handbook/finance/operating-metrics/#bookings-incremental-annual-contract-value-iacv) every year. We focus on an incremental number instead of growth of our Annual Recurring Revenue (ARR) because [ARR growth is misleading](https://blog.usejournal.com/the-vanity-of-arr-7246e525f4eb). So far we achieved the goal of doubling IACV in CY2013, CY2014, CY2015, CY2016, CY2017 and CY2018.

Why CY2020? It's aggressive, it's possible, and it's realistic. Having a goal gives us clarity on what we need to achieve. Our ambition is clear, and we want to be a growing and independent company. We are in an enormous market, and we're winning that market.

While we achieve our goals one by one, this doesn't mean we will focus on only one goal at a time. Simultaneously, we'll grow our userbase, get more [paid subscribers](/pricing/#self-managed), grow [GitLab.com](/pricing/#gitlab-com), realize our [scope](/direction/#scope), and make version control usable for more types of work.

During phase 2 there is a natural inclination to focus only on on-premises since we make all our money there. Having GitHub focus on SaaS instead of on-premises gave us a great opportunity to achieve phase 1. But GitHub was not wrong, they were early. When everyone was focused on video on demand Netflix focused on shipping DVDs by mail. Not because it was the future but because it was the biggest market. The biggest mistake they could have made was to stick with DVDs. Instead they leveraged the revenue generated with the DVDs to build the best video on demand service.

## Breadth over depth

We realize our competitors have started earlier and have more capital. Because we started later we need a more compelling product that covers the complete [scope](/direction/#scope) with a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/#convention-over-configuration) in a cloud native way. Because we have less capital, we need to build that as a community. Therefore it is important to share and ship our [vision for the product](/direction/#vision). The people that have the most knowledge have to prioritize **breadth over depth** since only they can add new functionality. Making the functionality more comprehensive requires less coordination than making the initial minimal feature. Shipping functionality that is incomplete to expand the scope sometimes goes against our instincts. However leading the way is needed to allow others to see our path and contribute. With others contributing, we'll iterate faster to [improve and polish functionality over time](http://mark.pundsack.com/2017/03/14/Polygonal-Product-Management/). So when in doubt, the rule of thumb is breadth over depth, so everyone can contribute.

If you want an analogy think of our product team as a plow way in front that tills the earth. It takes a while for the plants (complete features) to grow behind it. This tilled earth is ugly to look at but it surfaces the nutrients that the wider community needs to be inspired and to contribute.

If we can make a product that is strong with all features from planning to monitoring, and it works well, then we believe we can become the number one solution that companies standardize around. We need to offer the benefits that you can only have with an integrated product.

So breadth over depth is the strategy for GitLab the company. GitLab the project should have depth in every category it offers. It will take a few years to become [best in class in a certain space](/is-it-any-good/#gitlab-ci-is-a-leader-in-the-the-forrester-wave) because we depend on users contributing back, and we publish that journey on our [maturity page](/handbook/product/categories/maturity/). But that is the end goal, an application of unmatched breath and depth.

## Principles

1. Founder control: vote & board majority so we can keep making long term decisions.

2. Independence: since we took financing we need to have a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event); to maintain independence we want to become a public company rather than be acquired.

3. Low burn: spend seed money like it is the last we’ll raise, maintain 2 years of runway.

4. First time right: last to market so we get it right the first time, a fast follower with taste.

5. Values: make decisions based on [our values](/handbook/values), even if it is inconvenient.

6. Free SaaS: to make GitLab.com the most popular SaaS for private projects in CY2020, it should not have limits for projects or collaborators.

7. Reach: go for a broad reach, no focus on business verticals or certain programming languages.

8. Speed: ship every change in the next release to maximize responsiveness and learning.

9. Life balance: we want people to stay with us for a long time, so it is important to take time off, work on life balance, and being remote-only is a large part of the solution.

## Assumptions

1. [Open source user benefits](http://buytaert.net/acquia-retrospective-2015): significant advantages over proprietary software because of its faster innovation, higher quality, freedom from vendor lock-in, greater security, and lower total cost of ownership.

2. [Open Source stewardship](/company/stewardship/): community comes first, we [plays well with others](/handbook/product/#plays-well-with-others) and share the pie with other organizations commercializing GitLab.

3. [Innersourcing](/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/) is needed and will force companies to choose one solution top-down.

4. Git will dominate the version control market in CY2020.

5. A single application where [interdependence creates exceptional value](https://medium.com/@gerstenzang/developer-tools-why-it-s-hard-to-build-a-big-business-423436993f1c#.ie38a0cls) is superior to a collection of tools or a network of tools. Even so, good integrations are important for network effects and making it possible to integrate GitLab into an organization.

6. To be sustainable we need an open core model that includes a proprietary GitLab EE.

7. EE needs a low base price that is publicly available to compete for reach with CE, established competitors, and new entrants to the market.

8. The low base price for EE is supplemented by a large set of options aimed at larger organizations that get a lot of value from GitLab.

## Pricing

Most of GitLab functionality is and will be available for free in Core. Our paid tiers includes features that are [more relevant for managers, directors, and executives](/company/stewardship/#what-features-are-paid-only). [We promise](/company/stewardship/#promises) all major features in [our scope](/direction/#scope) are available in Core too. Instead of charging for specific parts of our scope (CI, Monitoring, etc.) we charge for smaller features that you are more likely to need if you use GitLab with a lot of users. There are a couple of reasons for this:

1. We want to be a good [steward of our open source product](/company/stewardship/).
1. Giving a great free product is part of our go to market, it helps create new users and customers.
1. Having our scope available to all users increases adoption of our scope and helps people see the benefit of an [single application](/handbook/product/single-application/).
1. Including all major features in Core helps reduce merge conflicts between CE and EE

Because we have a great free product we can't have one price.
Setting it high would make the difference from the free version too high.
Setting it low would make it hard to run a sustainable business.
There is no middle ground that would work out with one price.

That is why we have a [Starter, Premium, and Ultimate tiers](/handbook/product/#paid-tiers).
The price difference between each of them is half an order of magnitude (5x).

We charge for making people more effective and will charge per user, per application, or per instance.
We do include free minutes with our subscriptions and trials to make it easier for users to get started.
As we look towards more deployment-related functionality on .com it's tempting to offer compute and charge a percent on top of, for example, Google Cloud Platform (GCP).
We don't want to charge an ambiguous margin on top of another provider since this limits user choice and is not transparent.
So we will always let you BYOK (bring your own Kubernetes) and never lock you into our infrastructure to charge you an opaque premium on those costs.

## Challenges as we grow and scale

Losing the interest of the open source community would be detrimental to our success. For example, if someone wanted to make a contribution to CE and decided not to merge it because a similar feature already existed in EE, we would have lost out on an important contribution from the community.

We'll also need to adapt with a changing market. Netflix is a great example of this. Everyone knew that video on demand was the future. Netflix, however, started shipping DVDs over mail. They knew that it would get them a database of content that people would want to watch on demand. Timing is everything.

If a new, better version control technology dominates the market, we will need to adopt it and keep an open mind. Hopefully, we will be big enough at that point that people will consider us an integrated DevOps product. If not, we can always change our name, but we are currently investing to make git better for everyone.

For more thoughts on pricing please see our [pricing model](/handbook/ceo/pricing/).

## Dual flywheels

GitLab has two flywheel strategies that reinforce eachother.
A flywheel strategy is [defined as](https://medium.com/evergreen-business-weekly/flywheel-effect-why-positive-feedback-loops-are-a-meta-competitive-advantage-6d0ed55b67c5) one that has a positive feedback loops that build momentum, increasing the payoff of incremental effort.
Both are listed below as well as a table which lists the relevant indicator and department for every part of the flywheel.

### Open core flywheel

```mermaid
graph TD;
  MoreUsers-->MoreAcceptingMergeRequests;
  MoreAcceptingMergeRequests-->MoreContributions;
  MoreContributions-->MoreFeatures;
  MoreFeatures-->MoreUsers;
```

### Development spend flywheel

```mermaid
graph TD;
  MoreUsers-->MorePipeline;
  MorePipeline-->MoreRevenue;
  MoreRevenue-->MoreFeatures;
  MoreFeatures-->MoreUsers;
```

### KPIs and Responsible departments

| Part of flywheel | Key Performance Indicator (KPI) | Department |
|-------------- ---|---------------------------------|------------|
| MoreUsers | Stage Monthly Active Users | Developer marketing |
| MoreAcceptingMergeRequests | Accepting Merge Requests issue growth | Product Management |
| MoreContributions | Wider community contributions per release | Community relations |
| MoreFeatures | Merge Requests per release per engineer in product development | Engineering |
| MorePipeline | Pipe generated vs. plan | Marketing except community and developer |
| MoreRevenue | IACV vs. plan | Sales |

## Publicly viewable OKRs and KPIs

To make sure our goals are clearly defined and aligned throughout the organization, we make use of [Objectives and Key Results (OKRs)](/okrs) and [Key Performance Indicators (KPIs)](/handbook/ceo/kpis/) which are both publicly viewable.

## Why is this page public?

Our strategy is completely public because transparency is one of our [values](/handbook/values). We're not afraid of sharing our strategy because, as Peter Drucker said, "Strategy is a commodity, execution is an art."

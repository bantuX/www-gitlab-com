# frozen_string_literal: true

module Changelog
  class MergeRequest
    NO_CHANGELOG_LABEL = "no changelog"

    attr_reader :iid, :title, :labels

    def initialize(iid, title, labels)
      @iid = iid
      @title = title
      @labels = labels || []
    end

    def changelog_entry?
      return false if labels.include?(NO_CHANGELOG_LABEL)

      changes_handbook?
    end

    def date
      return DateTime.new(1970, 1, 1).to_date unless merged_at.is_a?(String)

      DateTime.parse(merged_at).to_date
    end

    def merged_at
      gitlab_merge_request.merged_at
    end

    def to_s
      "- [!#{iid}](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/#{iid}) #{title}"
    end

    private

    def changes
      return @changes if defined?(@changes)

      tries = 0
      begin
        @changes = Gitlab.merge_request_changes(Changelog::WWW_GITLAB_COM_PROJECT_ID, iid)&.to_h
      rescue Net::OpenTimeout => error
        # This can fail to connect occasionally, which really shouldn't be a reason to fail entirely
        # Give it 5 goes, just to get past any transient network fail
        # There may be more errors we should catch
        retry if (tries += 1) < 5
        raise error
      end
    end

    def changes_handbook?
      return false unless changes&.has_key?('changes')

      changes['changes'].any? do |change|
        change['new_path'].start_with?("source/handbook")
      end
    end

    def gitlab_merge_request
      @gitlab_merge_request ||= Gitlab.merge_request(Changelog::WWW_GITLAB_COM_PROJECT_ID, iid)
    end
  end
end

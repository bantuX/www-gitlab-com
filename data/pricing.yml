self_managed:
  intro: Host your own instance on-premises or in the public cloud
  prices:
    core_plan:
      title: Core
      edition_description: Helping developers build, deploy, and run their applications.
      monthly: $0
    starter_plan:
      title: Starter
      edition_description: Enabling teams to speed DevOps delivery with automation, prioritization, and workflow.
      monthly: $4
    premium_plan:
      title: Premium
      edition_description: Enabling IT to scale DevOps delivery with progressive deployment, advanced configuration, and consistent standards.
      monthly: $19
    ultimate_plan:
      title: Ultimate
      edition_description: Enabling businesses to transform IT by optimizing and accelerating delivery while managing priorities, security, risk, and compliance.
      monthly: $99
  questions:
    - question: Does GitLab offer a money-back guarantee?
      answer: |
        Yes, we offer a 45-day money-back guarantee for any GitLab self-hosted or GitLab.com plan. See full details on refunds in our [terms of service](https://about.gitlab.com/terms/)
    - question: What is GitLab High Availability?
      answer: |
        High availability is a system design that ensures a prearranged level of operational performance throughout a specific time-period. The most common way to measure HA is through the notion of uptime, which measures how long a service is up and running. <a href="/high-availability/">Learn more...</a>
    - question: Is there educational pricing?
      answer: |
        GitLab Ultimate and Gold are <a href="/solutions/education/">free for educational institutions</a>.
    - question: How is GitLab EE licensed?
      answer: |
        Every person with a GitLab account that is not blocked by the administrator should be counted in the subscription.
    - question: What is a user?
      answer: |
        User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
    - question: Can I get an evaluation license of EE?
      answer: |
        It is possible to obtain a free evaluation license of our enterprise edition for a 30 day period for up to 100 users.
    - question: What will happen to my evaluation license key at the end of the trial period?
      answer: |
        When you decide to purchase a subscription, you will be issued a new license key dependent on the type of subscription you sign up for. Should you not take out a subscription, your license key will expire at the end of your evaluation period.
    - question: Can I add more users to my subscription?
      answer: |
        Yes. You have a few options. You can add users to your subscription any time during the subscription period. You can log in to your account via the
        <a href="https://customers.gitlab.com">GitLab Customer Portal</a> and add more seats or by either contacting <a href="mailto:renewals@gitlab.com">renewals@gitlab.com</a> for a quote. In either case,
        the cost will be prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the additional licences per
        our true-up model.
    - question: The True-Up model seems complicated, can you illustrate?
      answer: |
        If you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also pay the full annual fee for the 200 users that you added during the year.
    - question: Do non-profits have to pay for the Enterprise Edition?
      answer: |
        Non-profits can use GitLab Community edition for free but need to purchase a
        subscription to the Enterprise Edition at our published rates.
    - question: Why does GitLab use licenses?
      answer: |
        The code for Enterprise Edition is open and can be inspected by anyone. This
        makes it easier to offer a trial of Enterprise Edition.
    - question: Can I inspect the GitLab EE license?
      answer: |
        Sure thing. It's publicly available at <https://gitlab.com/gitlab-org/gitlab-ee/blob/master/LICENSE>.
    - question: Can anyone download GitLab EE?
      answer: |
        Yes, without a license key it will function like CE.
    - question: How does the license key affect customers?
      answer: |
        Customers can now download and inspect the code for the latest GitLab EE version
        for free. However, they cannot use it without a valid subscription. They're also
        free to make modification to the GitLab EE code as long as they have a license.
    - question: Do blocked users count towards total user count?
      answer: |
        No, only _active_ users count towards total user count.
    - question: How does the license key work?
      answer: |
        The license key is a static file which, upon uploading, allows GitLab Enterprise
        Edition to run. During license upload we check that the active users on your
        GitLab Enterprise Edition instance doesn't exceed the new number of users.
        During the licensed period you may add as many users as you want. The license
        key will expire after one year for GitLab subscribers.
    - question: How do I upgrade GitLab EE to 7.11 or above?
      answer: |
        After you upgrade GitLab to EE version 7.11 and higher, GitLab EE will
        stop working without a license key. If you don't have a license key,
        please <a href="/sales/">contact sales</a> and request a key. Once you deploy the
        key, GitLab EE will start functioning again.
    - question: What happens when my subscription is about to expire or has expired?
      answer: |
        - Starting 30 days before the subscription end date, GitLab will display a
          notice to all administrators informing them of the impending expiration.
        - On the day the subscription expires, nothing changes.
        - 14 days after the subscription has ended, GitLab will display a notice to all
          users informing them of the expiration, and pushing code and creation of
          issues and merge requests will be disabled.
    - question: What happens when I renew my license after it expires?
      answer: |
        You will receive a new license that you will need to upload to your GitLab instance. This can be done by following <a href="https://docs.gitlab.com/ee/user/admin_area/license.html">these instructions</a>.
    - question: Do I need an additional license if I run more than one server (e.g., for backup, high availability, failover, testing, and staging)?
      answer: |
        No, if your GitLab Enterprise Edition servers cover the same users, you can use
        the same license file for all of them.
    - question: What happens if I decide to not renew my subscription?
      answer: |
        14 days after the end of your subscription, your key will no longer work and
        GitLab Enterprise Edition will not be functional anymore. You will be able to
        downgrade to GitLab Community Edition, which is free to use.
    - question: How does GitLab deal with bug fixes?
      answer: |
        Bugs that are reported to us always have a high priority, no matter which
        subscription you have. In most instances they will be fixed in the next release
        or the release after that, which means the turnaround is one month or two.
        Severe bugs will be fixed earlier in a patch release.
    - question: How does GitLab help companies ensure HIPAA compliance?
      include_file: includes/hipaa_faq.md
    - question: |
        How is a business day/hour defined with regard to support service levels?
      answer: |
        Normal business hours are from Sunday at 8:00pm PST (UTC-5) to Friday at
        4:00pm PST (UTC-5). Christmas Day and New Year's Day are holidays and not
        considered to be business days.
    - question: |
        Is GitHost still available?
      answer: |
        No, we are no longer accepting new customers for GitHost. More information is available in the <a href="/gitlab-hosted/">GitHost FAQ</a>
gitlab_com:
  intro: GitLab.com - SaaS offering hosted by GitLab
  prices:
    free_plan:
      title: Free
      edition_description: Helping developers build, deploy, and run their applications.
      support: Unlimited private projects and collaborators
      monthly: $0
      description:
        2,000 CI pipeline minutes per group per month on our shared runners
      features: Community Edition <a href="/products/#comparison">features</a>*
      link: https://gitlab.com/users/sign_in
      link_text: Sign Up
    bronze_plan:
      title: Bronze
      edition_description: Enabling teams to speed DevOps delivery with automation, prioritization, and workflow.
      paid: true
      monthly: $4
      yearly: $48
      description:
        2,000 CI pipeline minutes per group per month on our shared runners
      features: Enterprise Edition Starter <a href="/products/#comparison">features</a>*
      link: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff5a840412015aa3cde86f2ba6
      link_text: Buy Now
    silver_plan:
      title: Silver
      edition_description: Enabling IT to scale DevOps delivery with progressive deployment, advanced configuration, and consistent standards.
      paid: true
      monthly: $19
      yearly: $228
      description:
        10,000 CI pipeline minutes per group per month on our shared runners
      features: Enterprise Edition Premium <a href="/products/#comparison">features</a>*
      link: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fd5a840403015aa6d9ea2c46d6
      link_text: Buy Now
    gold_plan:
      title: Gold
      edition_description: Enabling businesses to transform IT by optimizing and accelerating delivery while managing priorities, security, risk, and compliance.
      paid: true
      monthly: $99
      yearly: $1188
      description:
        50,000 CI pipeline minutes per month on our shared runners and a 4-hour Support SLA.
      features: Enterprise Edition Premium <a href="/products/#comparison">features</a>*
      link: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fc5a83f01d015aa6db83c45aac
      link_text: Buy Now
  questions:
    - question: Does GitLab offer a money-back guarantee?
      answer: |
        Yes, we offer a 45-day money-back guarantee for any GitLab self-hosted or GitLab.com plan. See full details on refunds in our [terms of service](/terms/)
    - question: What are pipeline minutes?
      answer:
        Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners will not increase your pipeline minutes count and is unlimited.
    - question: What happens if I reach my minutes limit?
      answer:
        If you reach your limits, you can <a href="https://docs.gitlab.com/ee/user/admin_area/settings/continuous_integration.html#extra-shared-runners-pipeline-minutes-quota">purchase additional CI minutes</a>, or upgrade your account to Silver or Gold. Your own runners can still be used even if you reach your limits.
    - question: Does the minute limit apply to all runners?
      answer:
        No. We will only restrict your minutes for our shared runners. If you have a <a href="https://docs.gitlab.com/runner/">specific runner setup for your projects</a>, there is no limit to your build time on GitLab.com.
    - question: Do limits apply to public and private projects?
      answer:
        The minutes limit only applies to private projects. Public projects include projects set to "Internal" as they are visible to everyone on GitLab.com.
    - question: Is there a catch with the free forever plan?
      answer:
        There is no catch. Part of <a href="/company/strategy/#sequence">our strategy sequence</a> is to make GitLab.com the most popular SaaS solution for private and public repositories. To achieve this goal you get unlimited public and private projects, and there is no limit to the number of collaborators on a project.
    - question: Can I acquire a mix of licenses?
      answer:
        No, all users in the group need to be on the same plan.
    - question: Are GitLab Pages included in the free plan?
      answer:
        Absolutely, GitLab Pages will remain free for everyone.
    - question: How do I subscribe?
      answer:
        Head over to <a href="https://customers.gitlab.com">https://customers.gitlab.com</a>, choose the plan that is right for you. After purchase, we’ll take care of upgrading your account to the plan you’ve chosen.
    - question: Can I import my projects from another provider?
      answer:
        Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket. <a href="https://docs.gitlab.com/ee/workflow/importing/README.html">See our documentation</a> for all your import options.
    - question: I already have an account, how do I upgrade?
      answer:
        Head over to <a href="https://customers.gitlab.com">https://customers.gitlab.com</a>, choose the plan that is right for you.
    - question: What about your availability and security?
      answer:
        GitLab.com is monitored 24/7. Our servers are hosted on <a href="/handbook/engineering/infrastructure/production-architecture/">Google Cloud Platform (GCP), Amazon Web Services (AWS), Digital Ocean, and Azure</a>, we use configuration management, and we patch our servers at least once a week. Our <a href="https://gitlab.com/gitlab-com/runbooks">runbooks are public</a> as is <a href="https://gitlab.com/gitlab-com/infrastructure/issues">our operational issue tracker</a>. GitLab offers Two-Factor Authentication (2FA) via a mobile application or a U2F device, rate limiting, audit logs, and passwords are one-way encrypted. Answers to other common security questions are available on our security page.
    - question: Can I export my data?
      answer:
        You can <a href="https://docs.gitlab.com/ee/user/project/settings/import_export.html">export most of your data</a> at any time. Your data belongs to you. You are never stuck on GitLab.com, you can always export and import your project to a self hosted version of GitLab.
    - question: Do plans increase the minutes limit depending on the number of users in that group?
      answer:
        No. The limit will be applied to a group, no matter the number of users in that group.
    - question: What counts towards the disk space?
      answer:
        The project and wiki repository, Git LFS files, attachments, build artifacts, and images in the container registry.
    - question: Where can I find detailed information on GitLab.com's settings, such as SSH host keys and its shared Runners?
      answer:
        Where possible, GitLab.com uses the [standard package defaults](https://docs.gitlab.com/omnibus/package-information/defaults.html).  A list of all customized settings like the SSH host keys, Runners and Pages settings is available on the [GitLab.com settings](https://docs.gitlab.com/ee/user/gitlab_com/) page.
    - question: Is GitLab.com functioning OK?
      answer:
        For more information see our status page at [status.gitlab.com](https://status.gitlab.com/) and follow [@gitlabstatus](https://twitter.com/gitlabstatus) on twitter.
    - question: How much space can I have for my repo on GitLab.com?
      answer:
        Size limitations, as well as more information on how to
        manage repository size, is available [in our documentation](https://docs.gitlab.com/ee/user/admin_area/settings/account_and_limit_settings.html)
    - question: Can I buy additional storage space for myself or my organization?
      answer: >
        Not yet, but we are <a href="https://gitlab.com/gitlab-org/gitlab-ee/issues/5634" target="_blank">working on it</a>, you will soon be able to track your storage usage across all features and buy additional storage space for GitLab.com.
    - question: |
        Is GitHost still available?
      answer: |
        No, we are no longer accepting new customers for GitHost. More information is available in the <a href="/gitlab-hosted/">GitHost FAQ</a>
    - question: Do you have special pricing for public / open source projects?
      answer: |
        Yes! As part of GitLab's commitment to open source, Gold project features are available for free to public projects on GitLab.com. For organizations interested in free Gold features for groups, we also offer [free Gold and Ultimate](https://about.gitlab.com/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/) to educational institutions and open source projects.
    - question: Where is GitLab.com hosted?
      answer: |
        Currently we are hosted on the Google Cloud Platform in the USA
